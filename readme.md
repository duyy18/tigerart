<p align="center"><img src="https://github.com/normansyarif/siginjai-art/blob/master/public/images/home/siginjai-readme.png"></p>

## Siginjai ART

A Command Center System developed for PT. Telkom Indonesia Jambi by Information Systems students of Jambi University. The features in this system are:

- **Area** (shows the detailed information about each STO owned by PT Telkom Indonesia Jambi).
- **Report Performance** (shows all the reports, such as revenue consumer, revenue BGES, etc).
- **Togetherness** (a gallery that shows all the photos about activities in PT. Telkom Indonesia Jambi).
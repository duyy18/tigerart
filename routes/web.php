<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'PagesController@welcomePage')->name("welcome");
Route::get('/', function(){
	return redirect('/progress');
})->name("welcome");

Route::get('/tiger', 'PagesController@welcomePage');

Route::get('video-player/{id}', 'PagesController@videoPlayer')->name('videoPlayer');

Route::group(['middleware' => 'authGuard'], function () {
	/* ===== Tim magang telkom tahun 2019 =====
		Ahmad
	  ========================================= */
// Progress
Route::post('ajax/progress/do_filter','Progress\ProgressController@do_filter')
	->name('do_filter_progress');
Route::post('ajax/progress/fill_no_sc/{id?}','Progress\ProgressController@fill_blank_no_sc')
	->name('ajax_fill_blank_no_sc');
Route::post('ajax/progress/do_filter_kendala','Progress\ProgressController@do_filter_kendala')
	->name('do_filter_progress_kendala');
Route::get('progress','Progress\ProgressController@home')
	->name('progress_new');
Route::get('edit_data_no_sc','Progress\ProgressController@edit_data_no_sc')
	->name('edit_data_no_sc');
Route::get('progress/edit/{id?}','Progress\ProgressController@edit')
	->name('edit_progress');
Route::get('progress/validate_force_close/{sc?}','Progress\ProgressController@validate_close_prog')
	->name('fill_validate_force_close');
Route::get('progress/edit_hasil_survei/{id?}','Progress\ProgressController@edit_closing')
	->name('edit_progress_closing');
Route::get('progress/riwayat/{id?}','Progress\ProgressController@riwayat')
	->name('riwayat_progress');
Route::get('data_masuk_progress','Progress\ProgressController@data_masuk')
	->name('data_masuk');
Route::get('admin_edit_data/{id?}','Progress\ProgressController@admin_edit_data')
	->name('admin_edit_data');
Route::post('ajax/admin_decision_edit/{id?}','Progress\ProgressController@admin_decision_edit')
	->name('admin_decision_edit');
Route::get('ganti_password','Progress\ProgressController@ganti_password')
	->name('ganti_password');
Route::get('fill_password/{id?}','Progress\ProgressController@fill_password')
	->name('fill_password');
// Baru
Route::get('progress/do_progress/{id?}/{lv?}','Progress\ProgressController@doProgress')
	->name('receive_sc');
Route::get('progress/cs_validate/{sc?}/{decision?}','Progress\ProgressController@doValidateProgress')->name('do_validate_progress');
Route::get('progress/action_decision/{sc?}','Progress\ProgressController@action_decision')
	->name('action_decision');
Route::get('progress/fill/{id?}','Progress\ProgressController@fill')
	->name('fill_progress');
Route::get('progress/fill_no_sc/{id?}','Progress\ProgressController@fill_no_sc')
	->name('fill_no_sc');
Route::get('progress/detail/{id?}','Progress\ProgressController@detail')
	->name('detail_progress');
Route::get('progress/upload','Progress\ProgressController@upload')
	->name('upload_progress');
Route::get('progress/histori','Progress\ProgressController@histori')
	->name('histori_progress');
Route::get('progress/monitoring','Progress\ProgressController@monitor')
	->name('monitoring_progress');
Route::get('progress/data_close', 'Progress\ProgressController@data_close')
	->name('data_close_progress');
Route::get('progress/apresiasi','Progress\ProgressController@apresiasi')
	->name('apresiasi_progress');
Route::post('progress/tiang_upload/{sc?}','Progress\ProgressController@upload_tiang')
	->name('upload_tiang');
Route::post('ajax/progress/update/{jenis?}/{sc?}','Progress\ProgressController@updateProses')
	->name('update_progress');
Route::post('ajax/progress/upload','Progress\ProgressController@uploadFile')
	->name('upload_progress_ajax');
Route::post('ajax/progress/upfoto/{sc?}','Progress\ProgressController@uploadFoto')
	->name('upload_foto_progress');
Route::delete('ajax/progress/delfoto/{jenis?}/{sc?}','Progress\ProgressController@hapusFoto')
	->name('del_foto_progress');
Route::post('ajax/progress/upkml/{sc?}','Progress\ProgressController@uploadKML')
	->name('upload_kml');
Route::delete('ajax/progress/delkml/{sc?}','Progress\ProgressController@hapusKML')
	->name('hapus_kml');
Route::get('ajax/progress/monitor/{sektor?}/{jenis?}',
	'Progress\ProgressController@get_data_monitoring')
	->name('get_data_monitoring');
Route::post(
	'ajax/progress/force_close/validasi/{sc?}/{val?}',
	'Progress\ProgressController@validasi_force_close_pending')
	->name('validasi_force_close_pending');
Route::post('ajax/action_change_password/{id?}','Progress\ProgressController@action_change_password')
	->name('action_change_password');
Route::get('ajax/progress/force_close','Progress\ProgressController@get_data_force_close')
	->name('get_data_closing');
Route::post('ajax/progress/survei_post','Progress\ProgressController@post_survei_progress')
	->name('post_survei_progress');
Route::get('ajax/progress/urungkan/{sc?}','Progress\ProgressController@urungkan_progress')
	->name('urungkan_progress');
Route::post('ajax/progress/validasi_hasil_survei_pt23/{sc?}','Progress\ProgressController@validasi_hasil_survei_pt23')
	->name('validasi_hasil_survei_pt23');
Route::post('ajax/progress/decision_unspec/{sc?}','Progress\ProgressController@decision_unspec_post')
	->name('process_decision_unspec');
Route::get('misc/decrypt/{hash?}','Progress\ProgressController@decrypt');
Route::get('misc/encrypt/{hash?}','Progress\ProgressController@encrypt');
});

// AREA
Route::get('area', 'AreaController@showAreaHome')->name('areaShowHome');
Route::get('area/edit', 'AreaController@edit')->name('areaEdit');
Route::post('area/update', 'AreaController@update')->name('areaUpdate');
Route::get('area/sto/add', 'AreaController@addSTO')->name('addSTO');
Route::post('area/sto/store', 'AreaController@storeSTO')->name('storeSTO');
Route::get('area/sto/{id}', 'AreaController@showSTO')->name('showSTO');
Route::get('area/sto/{id}/edit', 'AreaController@editSTO')->name('editSTO');
Route::post('area/sto/{id}/update', 'AreaController@updateSTO')->name('updateSTO');
Route::get('area/witel', 'AreaController@showWitel')->name('showWitel');
Route::get('area/unavailable', 'AreaController@underConstruction')->name('underConstruction');
// ./End of Area


// REPORT PERFORMANCE
Route::get('report/revenue-consumer', 'RevenueController@showPage')->name('showRevenue');
Route::get('report/revenue-consumer/import', 'RevenueController@showRevImport')->name('showRevImport');
Route::get('report/nal', 'NalController@showPage')->name('showNal');
Route::get('report/nal/import', 'NalController@showImportPage')->name('showNalImport');
Route::get('report/bges', 'BgesController@showPage')->name('showBges');
// ./End of Report Performance


// EXCELLENT SERVICE
Route::get('togetherness/excellent-service/search', 'ExcellentServiceController@search')->name('es-search');
Route::get('togetherness/excellent-service/new', 'ExcellentServiceController@create')->name('add-es-route');
Route::post('togetherness/excellent-service/post', 'ExcellentServiceController@store')->name('es-post');
Route::put('togetherness/excellent-service/update/{id}', 'ExcellentServiceController@update')->name('es-put');
Route::delete('togetherness/excellent-service/destroy/{id}', 'ExcellentServiceController@destroy')->name('es-destroy');
Route::get('togetherness/excellent-service/{unit_id}', 'ExcellentServiceController@index')->name('togetherEs');
Route::get('togetherness/excellent-service/edit/{id}', 'ExcellentServiceController@edit')->name('edit-es-route');

Route::get('togetherness/es/ajax/description/{act_id}', 'ExcellentServiceController@ajaxDescription');
Route::get('togetherness/es/ajax/photos/{act_id}', 'ExcellentServiceController@ajaxPhotos');
Route::get('togetherness/es/ajax/videos/{act_id}', 'ExcellentServiceController@ajaxVideos');

Route::get('togetherness/es-delete-photo/{meeting_id}/{photo}', 'ExcellentServiceController@deletePhoto')->name('EsDeletePhoto');
Route::get('togetherness/es-delete-video/{meeting_id}/{video}', 'ExcellentServiceController@deleteVideo')->name('EsDeleteVideo');
// ./End of Excellent Service


// SOLVING THE PROBLEMS
Route::get('togetherness/solving-problems/search', 'SolvingProblemsController@search')->name('sp-search');
Route::get('togetherness/solving-problems/new', 'SolvingProblemsController@create')->name('add-sp-route');
Route::post('togetherness/solving-problems/post', 'SolvingProblemsController@store')->name('sp-post');
Route::put('togetherness/solving-problems/update/{id}', 'SolvingProblemsController@update')->name('sp-put');
Route::delete('togetherness/solving-problems/destroy/{id}', 'SolvingProblemsController@destroy')->name('sp-destroy');
Route::get('togetherness/solving-problems/{unit_id}', 'SolvingProblemsController@index')->name('togetherSp');
Route::get('togetherness/solving-problems/edit/{id}', 'SolvingProblemsController@edit')->name('edit-sp-route');

Route::get('togetherness/sp/ajax/attendance/{meeting_id}', 'SolvingProblemsController@ajaxAttendance')->name('ajaxAttendance');
Route::get('togetherness/sp/ajax/description/{meeting_id}', 'SolvingProblemsController@ajaxDescription')->name('ajaxDescription');
Route::get('togetherness/sp/ajax/photos/{meeting_id}', 'SolvingProblemsController@ajaxPhotos')->name('ajaxPhotos');

Route::get('attendance/submission/{meeting_id}', 'SolvingProblemsController@attendanceForm')->name('attendanceForm');
Route::post('attendance/post', 'SolvingProblemsController@attendancePost')->name('attendancePost');
Route::post('attendance/export', 'SolvingProblemsController@attendanceExport')->name('attendanceExport');
Route::get('attendance/submission-success', 'SolvingProblemsController@attendanceSuccess')->name('attendanceSuccess');
Route::get('attendance/setting', 'SolvingProblemsController@changeAttendanceURL')->name('attendanceSetting');
Route::post('attendance/setting/post', 'SolvingProblemsController@postAttendanceURL')->name('attendanceSettingPost');

Route::get('togetherness/delete-photo/{meeting_id}/{photo}', 'SolvingProblemsController@deletePhoto')->name('SpDeletePhoto');
// ./End of Solving the problems


// SOLID SPEED SMART
Route::get('togetherness/3s/search', 'SolidSpeedSmartController@search')->name('3s-search');
Route::get('togetherness/3s/new', 'SolidSpeedSmartController@create')->name('add-3s-route');
Route::post('togetherness/3s/post', 'SolidSpeedSmartController@store')->name('3s-post');
Route::put('togetherness/3s/update/{id}', 'SolidSpeedSmartController@update')->name('3s-put');
Route::delete('togetherness/3s/destroy/{id}', 'SolidSpeedSmartController@destroy')->name('3s-destroy');
Route::get('togetherness/3s/{unit_id}', 'SolidSpeedSmartController@index')->name('together3S');
Route::get('togetherness/3s/edit/{id}', 'SolidSpeedSmartController@edit')->name('edit-3s-route');

Route::get('togetherness/3s/ajax/description/{act_id}', 'SolidSpeedSmartController@ajaxDescription');
Route::get('togetherness/3s/ajax/photos/{act_id}', 'SolidSpeedSmartController@ajaxPhotos');
Route::get('togetherness/3s/ajax/videos/{act_id}', 'SolidSpeedSmartController@ajaxVideos');

Route::get('togetherness/3s-delete-photo/{meeting_id}/{photo}', 'SolidSpeedSmartController@deletePhoto')->name('SssDeletePhoto');
Route::get('togetherness/3s-delete-video/{meeting_id}/{video}', 'SolidSpeedSmartController@deleteVideo')->name('SssDeleteVideo');
// ./End of Solid Speed Smart

// Import files
Route::post('report/nal/post', 'NalController@post')->name('postNal');
Route::post('report/rev/post', 'RevenueController@post')->name('postRev');

// Download files
Route::get('file/{filename}', 'PagesController@download')->name('download');

// Bot
Route::get('bot', 'NotificationController@showPage')->name('bot');
Route::post('broadcast-messsage', 'NotificationController@sendMessage')->name('sendBot');
Route::get('recipients', 'NotificationController@recipients')->name('recipients');
Route::get('recipients/add', 'NotificationController@create')->name('createRecipient');
Route::post('recipients/store', 'NotificationController@store')->name('storeRecipient');
Route::get('recipients/{id}/edit', 'NotificationController@edit')->name('editRecipient');
Route::post('recipients/{id}/update', 'NotificationController@update')->name('updateRecipient');
Route::post('recipients/{id}/destroy', 'NotificationController@destroy')->name('deleteRecipient');

// Recipient Group
Route::get('group', 'NotificationController@groupIndex')->name('groupIndex');
Route::get('group/create', 'NotificationController@createGroup')->name('createGroup');
Route::post('group/store', 'NotificationController@storeGroup')->name('storeGroup');
Route::get('group/{id}/edit', 'NotificationController@editGroup')->name('editGroup');
Route::post('group/{id}/update', 'NotificationController@updateGroup')->name('updateGroup');
Route::post('group/{id}/destroy', 'NotificationController@destroyGroup')->name('deleteGroup');

Route::get('bot-find-chat-id', 'NotificationController@findChatID')->name('findChatID');
Route::get('bot-json', 'NotificationController@botIframe')->name('botIframe');

Route::auth();

Route::get('/home', 'HomeController@index')->name('home');

@extends('layouts.report_layout')

@section('addRoute', route('showRevImport'))

@section('title')
Finance Performance 
@endsection

@section('content')

<style type="text/css">
.map-table {
  border-radius: 5px;
}

.map-table th {
  padding-right: 3px;
}

.map-legend {
  float: left;
  margin-right: 15px;
}
</style>

<div class="container">

  <div class="card main-content" style="margin-bottom: 50px">
    <div class="card-title main-content-title">
      <strong>Revenue Consumer</strong>
    </div>

    <div class="box-container col-xs-12">
      <div class="box border-blue">
        <div class="box-title bg-blue">Target</div>
        <div class="image-container">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-8">
                <p class="text-center">
                  <strong>Target Revenue {{ $periode['year'] }} (Rp. Milyar)</strong>
                </p>

                <div class="chart">
                  <canvas id="salesChart" style="height: 180px; width: 703px;" width="703" height="180"></canvas>
                </div>
              </div>
              <div class="col-md-4">
                <div class="progress-group">
                  <span class="progress-text">1st Quarter</span>
                  <span class="progress-number">Rp. {{ $target['quarter']['1'] }} Milyar</span>

                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 21.5%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">2nd Quarter</span>
                  <span class="progress-number">Rp. {{ $target['quarter']['2'] }} Milyar</span>

                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 22.9%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">3rd Quarter</span>
                  <span class="progress-number">Rp. {{ $target['quarter']['3'] }} Milyar</span>

                  <div class="progress sm">
                    <div class="progress-bar progress-bar-green" style="width: 25.2%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">4th Quarter</span>
                  <span class="progress-number">Rp. {{ $target['quarter']['4'] }} Milyar</span>

                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 30.4%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text"><b>TOTAL</b></span>
                  <span class="progress-number"><b>Rp. {{ $target['total'] }} Milyar</b></span>
                </div>
                <!-- /.progress-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>

    <div class="box-container col-xs-12">
      <div class="box border-red">
        <div class="box-title bg-red">YTD {{ $periode['month'] }} {{ $periode['year'] }}</div>
        <div class="image-container">
          <div class="col-md-12">
            <div class="row">

              <div class="col-md-4">
                <div class="box">
                  <div class="box-header with-border">
                    <h4 class="box-title-report">Achievement Revenue</h4>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="info-box">
                              <span class="info-box-icon bg-red">T</span>
                              <div class="info-box-content">
                                <span class="info-box-text">TARGET</span>
                                <span class="info-box-number">{{ $ytd['target'] }}</span>
                              </div>
                              <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                          </div>
                          <!-- /.col -->
                          <div class="col-md-12">
                            <div class="info-box">
                              <span class="info-box-icon bg-green">R</i></span>

                              <div class="info-box-content">
                                <span class="info-box-text">REALISASI</span>
                                <span class="info-box-number">{{ $ytd['realisasi'] }}</span>
                              </div>
                              <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                          </div>
                          <!-- /.col -->

                          <div class="col-md-12">
                            <div class="info-box">
                              <span class="info-box-icon bg-aqua">A</span>

                              <div class="info-box-content">
                                <span class="info-box-text">ACHIEVEMENT</span>
                                <span class="info-box-number" id="ach-number" style="color: #3c8dbc"></span>
                              </div>
                              <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                          </div>
                          <!-- /.col -->
                        </div>

                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                  </div>
                  <!-- ./box-body -->
                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->

              <div class="col-md-4">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title-report">Ranking TR1</h3>

                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">

                        <div class="info-box bg-green">
                          <span class="info-box-icon">1</span>

                          <div class="info-box-content">
                            <span class="info-box-text box-text-1 rank-label">{{ $ytd['top']['tr']['1'][0] }}</span>
                            <span class="info-box-text box-text-1 rank-number">{{ $ytd['top']['tr']['1'][1] }}</span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>

                        <div class="info-box bg-purple">
                          <span class="info-box-icon">2</span>

                          <div class="info-box-content">
                            <span class="info-box-text box-text-1 rank-label">{{ $ytd['top']['tr']['2'][0] }}</span>
                            <span class="info-box-text box-text-1 rank-number">{{ $ytd['top']['tr']['2'][1] }}</span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>

                        <div class="info-box bg-light-blue">
                          <span class="info-box-icon">{{ $ytd['rank']['tr'] }}</span>

                          <div class="info-box-content">
                            <span class="info-box-text box-text-1 rank-label">WITEL JAMBI</span>
                            <span class="info-box-text box-text-1 rank-number">{{ $ytd['ach'] }}</span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>

                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                  </div>
                  <!-- ./box-body -->
                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->

              <div class="col-md-4">
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title-report">Ranking Nasional</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">

                        <div class="info-box bg-red-2">
                          <span class="info-box-icon">1</span>

                          <div class="info-box-content">
                            <span class="info-box-text box-text-1 rank-label">{{ $ytd['top']['nas']['1'][0] }}</span>
                            <span class="info-box-text box-text-1 rank-number">{{ $ytd['top']['nas']['1'][1] }}</span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>

                        <div class="info-box bg-orange-2">
                          <span class="info-box-icon">2</span>

                          <div class="info-box-content">
                            <span class="info-box-text box-text-1 rank-label">{{ $ytd['top']['nas']['2'][0] }}</span>
                            <span class="info-box-text box-text-1 rank-number">{{ $ytd['top']['nas']['2'][1] }}</span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>

                        <div class="info-box bg-coklat">
                          <span class="info-box-icon">{{ $ytd['rank']['nas'] }}</span>

                          <div class="info-box-content">
                            <span class="info-box-text box-text-1 rank-label">WITEL JAMBI</span>
                            <span class="info-box-text box-text-1 rank-number">{{ $ytd['ach'] }}</span>
                          </div>
                          <!-- /.info-box-content -->
                        </div>

                      </div>
                      <!-- /.col -->
                    </div>
                    <!-- /.row -->
                  </div>
                  <!-- ./box-body -->
                </div>
                <!-- /.box -->
              </div>
              <!-- /.col -->

            </div>
            <!-- /.row -->
          </div>
          <div class="clear"></div>
        </div>
      </div>
    </div>

    <div class="box-container col-xs-12">
      <div class="box border-green">
        <div class="box-title bg-green">Growth MoM {{ $periode['month'] }} {{{ $periode['year'] }}}</div>
        <div class="image-container">
          <div class="col-md-6">

            <div class="box">
              <div class="box-header with-border">
                <h4 class="box-title-report">Growth Revenue MoM</h4>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">

                    <div class="row">
                      <div class="col-md-6">

                        <div class="row">
                          <div class="col-md-12">
                            <div class="chart">
                              <canvas id="barChart" style="height: 300px; width: 510px;" width="410" height="300"></canvas>
                            </div>

                            <div class="col-md-12" style="margin-top: 15px;">

                              <div class="progress-group">
                                <span class="progress-text">{{ $periode['last_month'] }} {{ $periode['year'] }}</span>
                                <span class="progress-number">{{ $growthMoM['lastMonth'] }}</span>
                              </div>
                              <!-- /.progress-group -->
                              <div class="progress-group">
                                <span class="progress-text">{{ $periode['month'] }} {{ $periode['year'] }}</span>
                                <span class="progress-number">{{ $growthMoM['curMonth'] }}</span>
                              </div>
                              <!-- /.progress-group -->
                              <div class="progress-group growthMoM_container">
                                <span class="progress-text"><b>Growth MoM</b></span>
                                <span class="progress-number" id="growthMoM" style="font-weight: bold"></span>
                              </div>
                              <!-- /.progress-group -->
                            </div>

                          </div>
                        </div>

                      </div>
                      <!-- /.col -->

                      <div class="col-md-6">
                        <h4>Ranking TR1</h4>

                        <table class="table table-striped">
                          <tbody><tr>
                            <th style="width: 10px">Rank</th>
                            <th>Witel</th>
                            <th style="width: 40px">Growth</th>
                          </tr>
                          <tr>
                            <td>1.</td>
                            <td>{{ $growthMoM['top']['tr']['1'][0] }}</td>
                            <td><span class="badge bg-red">{{ $growthMoM['top']['tr']['1'][1] }}</span></td>
                          </tr>
                        <tr>
                          <td>2.</td>
                          <td>{{ $growthMoM['top']['tr']['2'][0] }}</td>
                          <td><span class="badge bg-yellow">{{ $growthMoM['top']['tr']['2'][1] }}</span></td>
                        </tr>
                        <tr style="color: #009688">
                          <td style="font-weight: bold">{{ $growthMoM['rank']['tr'] }}.</td>
                          <td style="font-weight: bold">WITAL JAMBI</td>
                          <td style="font-weight: bold"><span class="badge bg-green-3">{{ $growthMoM['growth'] }}</span></td>
                        </tr>
                      </tbody>
                    </table>


                    <h4>Ranking Nasional</h4>

                    <table class="table table-striped">
                      <tbody><tr>
                        <th style="width: 10px">Rank</th>
                        <th>Witel</th>
                        <th style="width: 40px">Growth</th>
                      </tr>
                      <tr>
                        <td>1.</td>
                        <td>{{ $growthMoM['top']['nas']['1'][0] }}</td>
                        <td><span class="badge bg-red">{{ $growthMoM['top']['nas']['1'][1] }}</span></td>
                      </tr>
                      <tr>
                        <td>2.</td>
                        <td>{{ $growthMoM['top']['nas']['2'][0] }}</td>
                        <td><span class="badge bg-yellow">{{ $growthMoM['top']['nas']['2'][1] }}</span></td>
                      </tr>
                      <tr style="color: #009688">
                        <td style="font-weight: bold">{{ $growthMoM['rank']['nas'] }}.</td>
                        <td style="font-weight: bold">WITEL JAMBI</td>
                        <td style="font-weight: bold"><span class="badge bg-green-3">{{ $growthMoM['growth'] }}</span></td>
                      </tr>
                    </tbody>
                  </table>
                </div>

              </div>

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- ./box-body -->
      </div>
      <!-- /.box -->

    </div>

    <div class="col-md-6"></div> <!-- Another item -->

    <div class="clear"></div>
  </div>
</div>
</div>

<div class="box-container col-xs-12">
  <div class="box border-blue">
    <div class="box-title bg-blue">Growth YoY {{ $periode['month'] }} {{ $periode['year'] }}</div>
    <div class="image-container">
      <div class="col-md-6">
        <div class="box">
          <div class="box-header with-border">
            <h4 class="box-title-report">Growth Revenue YoY {{ $periode['month'] }} {{ $periode['year'] }}</h4>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">

                <div class="row">
                  <div class="col-md-6">

                    <div class="row">
                      <div class="col-md-12">
                        <div class="chart">
                          <canvas id="barChart2" style="height: 300px; width: 510px;" width="410" height="300"></canvas>
                        </div>

                        <div class="col-md-12" style="margin-top: 15px;">

                          <div class="progress-group">
                            <span class="progress-text">YTD {{ $periode['month'] }} {{ ((int)($periode['year'])) - 1 }}</span>
                            <span class="progress-number">{{ $growthYoY['lastMonth'] }}</span>
                          </div>
                          <!-- /.progress-group -->
                          <div class="progress-group">
                            <span class="progress-text">YTD {{ $periode['month'] }} {{ $periode['year'] }}</span>
                            <span class="progress-number">{{ $growthYoY['curMonth'] }}</span>
                          </div>
                          <!-- /.progress-group -->
                          <div class="progress-group growthYoY_container">
                            <span class="progress-text"><b>Growth YoY</b></span>
                            <span class="progress-number" id="growthYoY" style="font-weight: bold">{{ $growthYoY['growth'] }}</span>
                          </div>
                          <!-- /.progress-group -->
                        </div>

                      </div>
                    </div>

                  </div>
                  <!-- /.col -->

                  <div class="col-md-6">
                    <h4>Ranking TR1</h4>

                    <table class="table table-striped">
                      <tbody><tr>
                        <th style="width: 10px">Rank</th>
                        <th>Witel</th>
                        <th style="width: 40px">Growth</th>
                      </tr>
                      <tr>
                        <td>1.</td>
                        <td>{{ $growthYoY['top']['tr']['1'][0] }}</td>
                        <td><span class="badge bg-red">{{ $growthYoY['top']['tr']['1'][1] }}</span></td>
                      </tr>
                    <tr>
                      <td>2.</td>
                      <td>{{ $growthYoY['top']['tr']['2'][0] }}</td>
                      <td><span class="badge bg-yellow">{{ $growthYoY['top']['tr']['2'][1] }}</span></td>
                    </tr>
                    <tr style="color: #009688">
                      <td style="font-weight: bold">{{ $growthYoY['rank']['tr'] }}.</td>
                      <td style="font-weight: bold">WITEL JAMBI</td>
                      <td style="font-weight: bold"><span class="badge bg-green-3">{{ $growthYoY['growth'] }}</span></td>
                    </tr>
                  </tbody>
                </table>


                <h4>Ranking Nasional</h4>

                <table class="table table-striped">
                  <tbody><tr>
                    <th style="width: 10px">Rank</th>
                    <th>Witel</th>
                    <th style="width: 40px">Growth</th>
                  </tr>
                  <tr>
                    <td>1.</td>
                    <td>{{ $growthYoY['top']['nas']['1'][0] }}</td>
                    <td><span class="badge bg-red">{{ $growthYoY['top']['nas']['1'][1] }}</span></td>
                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>{{ $growthYoY['top']['nas']['2'][0] }}</td>
                    <td><span class="badge bg-yellow">{{ $growthYoY['top']['nas']['2'][1] }}</span></td>
                  </tr>
                  <tr style="color: #009688">
                    <td style="font-weight: bold">{{ $growthYoY['rank']['nas'] }}.</td>
                    <td style="font-weight: bold">WITEL JAMBI</td>
                    <td style="font-weight: bold"><span class="badge bg-green-3">{{ $growthYoY['growth'] }}</span></td>
                  </tr>
                </tbody>
              </table>
            </div>

          </div>

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- ./box-body -->
  </div>
  <!-- /.box -->
</div>

<div class="col-md-6"></div> <!-- Another item -->

<div class="clear"></div>
</div>
</div>
</div>

<div class="box-container col-xs-12">
  <div class="box border-red">
    <div class="box-title bg-red">Revenue per Product {{ $periode['year'] }}</div>
    <div class="image-container">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3" style="padding-top: 50px">
            <!-- Chart here -->
            <div id="donut-chart" style="height: 180px;"></div>

            <div style="width: 67%; margin: 20px auto">
              <div class="progress-group">
                <span class="progress-text"><span style="color: red" class="glyphicon glyphicon-stop"></span> Indihome</span>
                <span class="progress-number">{{ $revenuePerProduct['indihome'][0] }}</span>
              </div>
              <!-- /.progress-group -->
              <div class="progress-group">
                <span class="progress-text"><span style="color: grey" class="glyphicon glyphicon-stop"></span> Non Indihome</span>
                <span class="progress-number">{{ $revenuePerProduct['nonindihome'][0] }}</span>
              </div>
              <!-- /.progress-group -->
              <div class="progress-group">
                <span style="margin-left: 17px" class="progress-text"><b>TOTAL</b></span>
                <span class="progress-number"><b>{{ $revenuePerProduct['total'] }}</b></span>
              </div>
            </div>
          </div>
          <div class="col-md-9">
            <!-- Table here -->
            <div class="col-md-12">
              <table class="table table-striped table-responsive" style="padding-left: 3%; padding-right: 3%;">
                <tbody>

                  <tr>
                    <th>Bulan</th>
                    <th>Jan</th>
                    <th>Feb</th>
                    <th>Mar</th>
                    <th>Apr</th>
                    <th>Mei</th>
                    <th>Jun</th>
                    <th>Jul</th>
                    <th>Agu</th>
                    <th>Sep</th>
                    <th>Okt</th>
                    <th>Nov</th>
                    <th>Des</th>
                  </tr>

                  <tr style="color: #f00">
                    <td style="font-weight: bold;">Indihome</td>

                    <!-- Indihome -->
                    @for($i = 0; $i < 12; $i++)
                    @if($trendRevenue[$i][0] == "0")
                    <td>-</td>
                    @else
                    <td>{{ $trendRevenue[$i][0] }}</td>
                    @endif
                    @endfor

                  </tr>

                  <tr style="color: rgb(97, 97, 97);">
                    <td style="font-weight: bold;">Non Indihome</td>

                    <!-- Nonindihome -->
                    @for($i = 0; $i < 12; $i++)
                    @if($trendRevenue[$i][0] == "0")
                    <td>-</td>
                    @else
                    <td>{{ $trendRevenue[$i][1] }}</td>
                    @endif
                    @endfor

                  </tr>

                  <tr>
                    <td style="font-weight: bold;">TOTAL</td>

                    <!-- Total -->
                    @for($i = 0; $i < 12; $i++)
                    @if($trendRevenue[$i][2] == "0")
                    <td>-</td>
                    @else
                    <td>{{ $trendRevenue[$i][2] }}</td>
                    @endif
                    @endfor
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-md-12">
              <canvas id="barChart3" style="height: 80px;" height="80"></canvas>
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>

<div class="box-container col-xs-12">
  <div class="box border-blue">
    <div class="box-title bg-blue">Revenue per Layanan {{ $periode['year'] }}</div>
    <div class="image-container">
      <div class="col-md-12">
        <table id="example2" class="table table-bordered table-hover">
          <tr>
            <th>Bulan</th>
            <td>Jan</td>
            <td>Feb</td>
            <td>Mar</td>
            <td>Apr</td>
            <td>Mei</td>
            <td>Jun</td>
            <td>Jul</td>
            <td>Agt</td>
            <td>Sep</td>
            <td>Okt</td>
            <td>Nov</td>
            <td>Des</td>
          </tr>
          <tr style="color: rgb(97, 97, 97);">
            <th>Legacy</th>

            @for($i = 0; $i < 12; $i++)
            @if($serviceRevenue[$i][2] == "0")
            <td>-</td>
            @else
            <td>{{ $serviceRevenue[$i][2] }}</td>
            @endif
            @endfor

          </tr>
          <tr style="color: #f00">
            <th>Connectivity</th>

            @for($i = 0; $i < 12; $i++)
            @if($serviceRevenue[$i][0] == "0")
            <td>-</td>
            @else
            <td>{{ $serviceRevenue[$i][0] }}</td>
            @endif
            @endfor
          </tr>
          <tr style="color: rgba(60,141,188,1)">
            <th>Digital Service</th>

            @for($i = 0; $i < 12; $i++)
            @if($serviceRevenue[$i][1] == "0")
            <td>-</td>
            @else
            <td>{{ $serviceRevenue[$i][1] }}</td>
            @endif
            @endfor

          </tr>

        </table>
      </div>
      <div class="col-md-12">
        <canvas id="barChart5" style="height: 69px" height="69"></canvas>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>

<div class="box-container col-xs-12">
  <div class="box border-green">
    <div class="box-title bg-green">Revenue Contribution</div>
    <div class="image-container">
      <div class="col-md-12" style="padding-bottom: 30px">
        <h4>Revenue Contribution Per ISA</h4>
        <div class="col-md-4" id="donut-chart2" style="height: 300px"></div>

        <div class="col-md-3" style="padding-top: 100px">

          <div class="progress-group">
            <span class="progress-text"><span style="color: #2196F3" class="glyphicon glyphicon-stop"></span> Kandatel Muara Bungo</span>
            <span class="progress-number">{{ $revContribution[0][1] }}</span>
          </div>

          <div class="progress-group">
            <span class="progress-text"><span style="color: #f00" class="glyphicon glyphicon-stop"></span> Jambi Inner</span>
            <span class="progress-number">{{ $revContribution[1][1] }}</span>
          </div>

          <div class="progress-group">
            <span class="progress-text"><span style="color: #aaa" class="glyphicon glyphicon-stop"></span> Jambi Outer</span>
            <span class="progress-number">{{ $revContribution[2][1] }}</span>
          </div>

        </div>
      </div>
      <div class="col-md-12">
        <h4>Revenue Contribution Per STO</h4>
        <div id="dvMap" style="height: 600px; border-radius: 10px"></div>
        <div class="legends">
          <p class="map-legend">Cont : Revenue Contribution</p>
          <p class="map-legend">T : Target Revenue</p>
          <p class="map-legend">R : Real Revenue</p>
          <p class="map-legend">Ach : Achievement</p>
          <p class="map-legend">Rank : Rank Achievement</p>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>

<div class="clear"></div>
</div>


@endsection


@section('script')

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHzEtuVlr36M5MJwT7EtHna7cLFTzDTWs&v=3&amp;sensor=false"></script>

<script type="text/javascript" src="{{ asset('js/markerwithlabel.js') }}"></script>

<script type="text/javascript">

  $(document).ready(function() {
    var markers = {!! json_encode($revSTO) !!};

    var mapOptions = {
      center: new google.maps.LatLng('-1.75754', '102.69160'),
      zoom: 9,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var infoWindow = new google.maps.InfoWindow();
    var latlngbounds = new google.maps.LatLngBounds();
    var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
    var i = 0;
    var interval = setInterval(function () {
      var data = markers[i];
      var myLatlng = new google.maps.LatLng(data.lat, data.lng);
      var icon = "";
      var marker = new MarkerWithLabel({
        position: myLatlng,
        map: map,
        id: data.sto,
        zIndex: 40,
        draggable: true,
        animation: google.maps.Animation.DROP,
        labelContent: '<div class="map-table" style="background:white; color: #000; padding: 5px">'
        + '<p style="font-weight:bold">' + data.sto + '</p>'
        + '<table>'
        +   '<tr>'
        +       '<th>Cont</th>'
        +       '<td>' + data.cont + '</td>'
        +   '</tr>'  
        +   '<tr>'
        +       '<th>T</th>'
        +       '<td>' + data.t + '</td>'
        +   '</tr>'  
        +   '<tr>'
        +       '<th>R</th>'
        +       '<td>' + data.r + '</td>'
        +   '</tr>'  
        +   '<tr>'
        +       '<th>Ach</th>'
        +       '<td class="map-ach">' + data.ach + '</td>'
        +   '</tr>'  
        +   '<tr>'
        +       '<th>Rank</th>'
        +       '<td>' + data.rank + '</td>'
        +   '</tr>'  
        +   '<tr>'
        +       '<th>Dev</th>'
        +       '<td class="map-dev">' + data.dev + '</td>'
        +   '</tr>'  
        +   '<tr>'
        +       '<th>GMoM</th>'
        +       '<td class="map-gmom">' + data.gmom + '</td>'
        +   '</tr>'  
        +   '<tr>'
        +       '<th>GYoY</th>'
        +       '<td class="map-gyoy">' + data.gyoy + '</td>'
        +   '</tr>'     
        + '</table></div>',
        labelAnchor: new google.maps.Point(22, -5),
        labelClass: "labels",
        labelStyle: {opacity: 0.9},
        icon: new google.maps.MarkerImage(icon),
        category: data.category
      });

      if(data.lat == null || data.lat == "null" || data.lat == "") {
        marker.setMap(null);
      }

      (function (marker, data) {
        google.maps.event.addListener(marker, "click", function (e) {
          var latitude = e.latLng.lat();
          var longitude = e.latLng.lng();
          console.log( latitude + ', ' + longitude );
        });
      })(marker, data);
      latlngbounds.extend(marker.position);

      i++;
      if (i == markers.length) {
        clearInterval(interval);
        var bounds = new google.maps.LatLngBounds();
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
      }
    }, 80);

    validate();
  });

  function validate() {

              // Achievement target - #ach-number
              var achNumber = $('#ach-number');
              var value = parseFloat("{{ $ytd['ach'] }}");

              $(achNumber).html(value + '<small>%</small>');

              if(value < 100) {
                $(achNumber).css("color", "red");
              }else{
                $(achNumber).css("color", "#3c8dbc");
              }


              // Growth MoM
              var momElement = $('#growthMoM');
              var valueMonth = parseFloat("{{ $growthMoM['growth'] }}");

              $(momElement).html(valueMonth + "%");

              if(valueMonth < 0) {
                $(".growthMoM_container").css("color", "red");
              }else{
                $(".growthMoM_container").css("color", "#3c8dbc");
              }


              // Growth YoY
              var yoyElement = $('#growthYoY');
              var valueYear = parseFloat("{{ $growthYoY['growth'] }}");

              $(yoyElement).html(valueYear + "%");

              if(value < 0) {
                $(".growthYoY_container").css("color", "red");
              }else{
                $(".growthYoY_container").css("color", "#3c8dbc");
              }

            }



            $(function () {

              'use strict';
              var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
              var salesChart       = new Chart(salesChartCanvas);

              var salesChartData = {
                labels  : ['Q1', 'Q2', 'Q3', 'Q4'],
                datasets: [
                {
                  label               : 'Target Revenue',
                  fillColor           : 'rgba(60,141,188,0.9)',
                  strokeColor         : 'rgba(60,141,188,0.8)',
                  pointColor          : '#3b8bba',
                  pointStrokeColor    : 'rgba(60,141,188,1)',
                  pointHighlightFill  : '#fff',
                  pointHighlightStroke: 'rgba(60,141,188,1)',
                  data                : ["{{ $target['quarter']['1'] }}", "{{ $target['quarter']['2'] }}", "{{ $target['quarter']['3'] }}", "{{ $target['quarter']['4'] }}"]
                }
                ]
              };

              var salesChartOptions = {
    // Boolean - If we should show the scale at all
    showScale               : true,
    // Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines      : false,
    // String - Colour of the grid lines
    scaleGridLineColor      : 'rgba(0,0,0,.05)',
    // Number - Width of the grid lines
    scaleGridLineWidth      : 1,
    // Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    // Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines  : true,
    // Boolean - Whether the line is curved between points
    bezierCurve             : true,
    // Number - Tension of the bezier curve between points
    bezierCurveTension      : 0.3,
    // Boolean - Whether to show a dot for each point
    pointDot                : true,
    // Number - Radius of each point dot in pixels
    pointDotRadius          : 4,
    // Number - Pixel width of point dot stroke
    pointDotStrokeWidth     : 1,
    // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,
    // Boolean - Whether to show a stroke for datasets
    datasetStroke           : true,
    // Number - Pixel width of dataset stroke
    datasetStrokeWidth      : 2,
    // Boolean - Whether to fill the dataset with a color
    datasetFill             : true,
    // String - A legend template
    legendTemplate          : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio     : true,
    // Boolean - whether to make the chart responsive to window resizing
    responsive              : true
  };
  // Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);

  // ---------------------------
  // - END MONTHLY SALES CHART -
  // ---------------------------


  //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ["{{ $periode['last_month'] }} {{ $periode['year'] }}", "{{ $periode['month'] }} {{ $periode['year'] }}"],
      datasets: [

      {
        label               : 'Growth MoM',
        fillColor           : 'rgba(60,141,188,0.9)',
        strokeColor         : 'rgba(60,141,188,0.8)',
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : ["{{ $growthMoM['lastMonth'] }}", "{{ $growthMoM['curMonth'] }}"]
      }
      ]
    }
    barChartData.datasets[0].fillColor   = '#00a65a'
    barChartData.datasets[0].strokeColor = '#00a65a'
    barChartData.datasets[0].pointColor  = '#00a65a'
    var barChartOptions                  = {
      scaleOverride : true,
      scaleSteps : 7,
      scaleStepWidth : 2,
      scaleStartValue : 0,
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)

    // barChart2

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas                   = $('#barChart2').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ["{{ $periode['month'] }} {{ ((int)$periode['year']) - 1 }}", "{{ $periode['month'] }} {{ $periode['year'] }}"],
      datasets: [

      {
        label               : 'Growth YoY',
        fillColor           : 'rgba(60,141,188,0.9)',
        strokeColor         : 'rgba(60,141,188,0.8)',
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : ["{{ $growthYoY['lastMonth'] }}", "{{ $growthYoY['curMonth'] }}"]
      }
      ]
    }
    barChartData.datasets[0].fillColor   = '#2196F3'
    barChartData.datasets[0].strokeColor = '#2196F3'
    barChartData.datasets[0].pointColor  = '#2196F3'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)

    // ---barChart 3

    var barChartCanvas                   = $('#barChart3').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'],
      datasets: [

      {
        label               : 'Trend Revenue Indihome',
        fillColor           : '#f00',
        strokeColor         : '#f00',
        pointColor          : '#f00',
        pointStrokeColor    : '#f00',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : ["{{ $trendRevenue[0][0] }}",
        "{{ $trendRevenue[1][0] }}",
        "{{ $trendRevenue[2][0] }}",
        "{{ $trendRevenue[3][0] }}",
        "{{ $trendRevenue[4][0] }}",
        "{{ $trendRevenue[5][0] }}",
        "{{ $trendRevenue[6][0] }}",
        "{{ $trendRevenue[7][0] }}",
        "{{ $trendRevenue[8][0] }}",
        "{{ $trendRevenue[9][0] }}",
        "{{ $trendRevenue[10][0] }}",
        "{{ $trendRevenue[11][0] }}"]
      },
      {
        label               : 'Trend Revenue Non Indihome',
        fillColor           : 'rgba(97,97,97,1)',
        strokeColor         : 'rgba(97,97,97,1)',
        pointColor          : 'rgba(97,97,97,1)',
        pointStrokeColor    : 'rgba(97,97,97,1)',
        pointHighlightFill  : 'rgba(97,97,97,1)',
        pointHighlightStroke: 'rgba(97,97,97,1)',
        data                : ["{{ $trendRevenue[0][1] }}",
        "{{ $trendRevenue[1][1] }}",
        "{{ $trendRevenue[2][1] }}",
        "{{ $trendRevenue[3][1] }}",
        "{{ $trendRevenue[4][1] }}",
        "{{ $trendRevenue[5][1] }}",
        "{{ $trendRevenue[6][1] }}",
        "{{ $trendRevenue[7][1] }}",
        "{{ $trendRevenue[8][1] }}",
        "{{ $trendRevenue[9][1] }}",
        "{{ $trendRevenue[10][1] }}",
        "{{ $trendRevenue[11][1] }}"]
      }
      ]
    }
    var barChartOptions                  = {
      scaleOverride : true,
      scaleSteps : 8,
      scaleStepWidth : 2,
      scaleStartValue : 0,
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)

    // ----barchart 5

    var barChartCanvas                   = $('#barChart5').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'],
      datasets: [
      {
        label               : 'Legacy',
        fillColor           : 'rgb(97, 97, 97)',
        strokeColor         : 'rgb(97, 97, 97)',
        pointColor          : 'rgb(97, 97, 97)',
        pointStrokeColor    : 'rgb(97, 97, 97)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : ["{{ $serviceRevenue[0][2] }}",
        "{{ $serviceRevenue[1][2] }}",
        "{{ $serviceRevenue[2][2] }}",
        "{{ $serviceRevenue[3][2] }}",
        "{{ $serviceRevenue[4][2] }}",
        "{{ $serviceRevenue[5][2] }}",
        "{{ $serviceRevenue[6][2] }}",
        "{{ $serviceRevenue[7][2] }}",
        "{{ $serviceRevenue[8][2] }}",
        "{{ $serviceRevenue[9][2] }}",
        "{{ $serviceRevenue[10][2] }}",
        "{{ $serviceRevenue[11][2] }}"
        ]
      },
      {
        label               : 'Connectivity',
        fillColor           : '#f00',
        strokeColor         : '#f00',
        pointColor          : '#f00',
        pointStrokeColor    : '#f00',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : ["{{ $serviceRevenue[0][0] }}",
        "{{ $serviceRevenue[1][0] }}",
        "{{ $serviceRevenue[2][0] }}",
        "{{ $serviceRevenue[3][0] }}",
        "{{ $serviceRevenue[4][0] }}",
        "{{ $serviceRevenue[5][0] }}",
        "{{ $serviceRevenue[6][0] }}",
        "{{ $serviceRevenue[7][0] }}",
        "{{ $serviceRevenue[8][0] }}",
        "{{ $serviceRevenue[9][0] }}",
        "{{ $serviceRevenue[10][0] }}",
        "{{ $serviceRevenue[11][0] }}"
        ]
      },
      {
        label               : 'Digital Service',
        fillColor           : 'rgba(60,141,188,0.9)',
        strokeColor         : 'rgba(60,141,188,0.8)',
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : ["{{ $serviceRevenue[0][1] }}",
        "{{ $serviceRevenue[1][1] }}",
        "{{ $serviceRevenue[2][1] }}",
        "{{ $serviceRevenue[3][1] }}",
        "{{ $serviceRevenue[4][1] }}",
        "{{ $serviceRevenue[5][1] }}",
        "{{ $serviceRevenue[6][1] }}",
        "{{ $serviceRevenue[7][1] }}",
        "{{ $serviceRevenue[8][1] }}",
        "{{ $serviceRevenue[9][1] }}",
        "{{ $serviceRevenue[10][1] }}",
        "{{ $serviceRevenue[11][1] }}"
        ]
      }
      ]
    }
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions)

    // pie chart

    var donutData = [
    { label: 'IndiHome', data: "{{ $revenuePerProduct['indihome'][0] }}", color: '#f00', percentage: "{{ $revenuePerProduct['indihome'][1] }}" },
    { label: 'Non IndiHome', data: "{{ $revenuePerProduct['nonindihome'][0] }}", color: 'rgba(97,97,97,1)', percentage: "{{ $revenuePerProduct['nonindihome'][1] }}" }
    ]
    $.plot('#donut-chart', donutData, {
      series: {
        pie: {
          show       : true,
          radius     : 1,
          innerRadius: 0.4,
          label      : {
            show     : true,
            radius   : 2 / 3,
            formatter: labelFormatter,
            threshold: 0.1
          }

        }
      },
      legend: {
        show: false
      }
    })

    function labelFormatter(label, series) {
     console.log(series)
     return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
     + label
     + '<br>'
     + series.data[0][1] + " (" + series.percentage + ")" + '</div>'

   }

  // pie chart2

  var donutData = [      
  { label: "Jambi Outer", data: "{{ $revContribution[2][0] }}", color: '#aaa', percentage: "{{ $revContribution[2][1] }}" },
  { label: "Jambi Inner", data: "{{ $revContribution[1][0] }}", color: '#f00', percentage: "{{ $revContribution[1][1] }}" },
  { label: "Kandatel Muara Bungo", data: "{{ $revContribution[0][0] }}", color: '#2196F3', percentage: "{{ $revContribution[0][1] }}" }
  ]
  $.plot('#donut-chart2', donutData, {
    series: {
      pie: {
        show       : true,
        radius     : 1,
        innerRadius: 0.4,
        label      : {
          show     : true,
          radius   : 2 / 3,
          formatter: labelFormatter,
          threshold: 0.1
        }

      }
    },
    legend: {
      show: false
    }
  })

  function labelFormatter(label, series) {
   console.log(series)
   return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
   + label
   + '<br>'
   + series.data[0][1] + " (" + series.percentage + ")" + '</div>'

 }

 window.onload = function () {



 }


});



</script>

@endsection
@extends('layouts.report_layout')

@section('content')

<div class="container">
	<div class="col-md-8 col-md-offset-2">
		<div class="card">
			<h4 class="title">Telegram Bot</h4>
			<form class="col-md-10 col-md-offset-1" method="post" enctype="multipart/form-data" action="{{ route('sendBot') }}" style="margin-top: 20px; padding-bottom: 40px">

				@if(Session::get('user_ids'))
				<label>Sending Logs</label>
				<div id="logs" style="background-color: #e0f6ff; padding: 8px 15px; height: 200px; margin-bottom: 20px; border-radius: 10px; overflow: auto">
					<p style="color: #787878">Don't close this window before the message is sent to all the recipients.</p>
				</div>
				@endif

				

				<label>Message</label>
				{{ csrf_field() }}
				<textarea name="body" id="body" class="form-control" rows="8" placeholder="Write your message here..."></textarea>
				<label style="margin-top: 20px">Recipients</label>
				<select name="group" class="form-control">
					<option value="all">All</option>

					@if($group)
						@foreach($group as $g)
						<option value="{{ $g->id }}">{{ $g->name }}</option>
						@endforeach
					@endif

				</select>
				<p><a href="{{ route('recipients') }}">Manage recipients</a></p>
				<button style="margin-top: 20px" type="submit" class="btn btn-primary">Send</button>
				<button style="margin-top: 20px" id="tts-box" class="btn btn-success">Read</button>

				<div class="clear"></div>
			</form>
			<div class="clear"></div>
		</div>
		<div style="height: 30px"></div>
	</div>
</div>
@endsection

@section('script')

<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>

<script src="http://code.responsivevoice.org/responsivevoice.js"></script>

<script type="text/javascript">

	function sendToMultipleUsers(user_id, message) {

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});

		for(var i = 0; i < user_id.length; i++) {

			$('#logs').append('<p>Sending to user id ' + user_id[i] + "...</p>");

			jQuery.ajax({
				url: "https://api.telegram.org/bot649401424:AAGKo6FrXwi6O00YkXsMp16s9ZvLFCaueLs/sendMessage",
				cache: false,
				method: 'post',
				data: {
					chat_id: user_id[i],
					text: message
				},
				success: function(result){
					$('#logs').append('<p style="color: #199a00">Sent to ' + result.result.chat.first_name + ".</p>");
				},
				error:function(data){
					$('#logs').append('<p style="color: #f00">Failed when trying to send the message.</p>');
				}
			});

		}

	}

	$(document).ready(function() {
		
		@if(Session::get('user_ids'))
		$('#body').html({!! json_encode(Session::get('message')) !!});
		var user_id = {!! json_encode(Session::get('user_ids')) !!};
		var msg = {!! json_encode(Session::get('message')) !!};
		sendToMultipleUsers(user_id, msg);
		@endif

	});

	$('#tts-box').click(function(e) {
		e.preventDefault();
		var text = $('#body').val();
		responsiveVoice.speak(text, "Indonesian Female");
	});

</script>

@endsection
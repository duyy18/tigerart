@extends('layouts.report_layout')

@section('content')

<div class="container">
	<div class="col-md-8 col-md-offset-2">
		<div class="card">
			<h4 class="title">Upload file NAL (.csv)</h4>
			<form method="post" enctype="multipart/form-data" action="{{ route('postNal') }}" class="navbar-form navbar-left" style="margin-left: 25%; margin-top: 60px">
				@include('includes.message')
				<label>File NAL</label>
				<div class="clear"></div>
				<div class="form-group">
					{!! csrf_field() !!}
					<input type="file" class="form-control" name="csvfile" placeholder="Your file...">
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>		

			<div class="clear"></div>

			<p style="text-align: center; margin-top: 30px"><a href="{{ route('download', 'nal.csv') }}">Download file</a>  (Right click > save link as...)</p>
		</div>
	</div>
</div>
@endsection

@section('script')

<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>

<script type="text/javascript">

</script>

@endsection
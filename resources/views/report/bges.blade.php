@extends('layouts.report_layout')

@section('content')


<div class="container">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1 style="font-size: 2em; margin-bottom: 15px; margin-top: 10px">
			BGES Jambi
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<!-- <li><a href="#">Layout</a></li> -->
			<li class="active">BGES Jambi</li>
		</ol>
	</section>


	<div class="row">

		<div class="col-md-4">
			<section id="monthly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					YTD Juli 2018
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">BGES Achivement Revenue Ytd Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-red">T</span>

											<div class="info-box-content">
												<span class="info-box-text">TARGET</span>
												<span class="info-box-number">41,2</span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->

									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-green">R</span>

											<div class="info-box-content">
												<span class="info-box-text">REALISASI</span>
												<span class="info-box-number">40,2</span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->

									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-aqua">A</span>

											<div class="info-box-content">
												<span class="info-box-text">ACHIEVEMENT</span>
												<span class="info-box-number" style="color: red">98,77<small>%</small></span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->
								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

			<!-- new -->
		</section>


		<div class="col-md-4">
			<section id="yearly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					Growth MoM
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">BGES Growth Revenue MoM Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">

										<div class="row">
											<div class="col-md-12">
												<div class="chart">
													<canvas id="barChartBges" style="height: 235px; width: 510px;" width="410" height="300"></canvas>
												</div>

												<div class="col-md-12" style="margin-top: 15px;">

													<div class="progress-group">
														<span class="progress-text">Juni 2018</span>
														<span class="progress-number">6,1</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text">Juli 2018</span>
														<span class="progress-number">6,0</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text"><b>Growth MoM</b></span>
														<span class="progress-number" style="color: red"><b>-1,44%</b></span>
													</div>
													<!-- /.progress-group -->
												</div>

											</div>
										</div>

									</div>
									<!-- /.col -->


								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

		</section>


		<div class="col-md-4">
			<section id="yearly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					Growth YoY
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">BGES Growth Revenue YoY Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">

										<div class="row">
											<div class="col-md-12">
												<div class="chart">
													<canvas id="barChartBges2" style="height: 235px; width: 510px;" width="410" height="300"></canvas>
												</div>

												<div class="col-md-12" style="margin-top: 15px;">

													<div class="progress-group">
														<span class="progress-text">Ytd Juli 2017</span>
														<span class="progress-number">37,6</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text">Ytd Juli 2018</span>
														<span class="progress-number">40,7</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text"><b>Growth YoY</b></span>
														<span class="progress-number" style="color: blue"><b>8,22%</b></span>
													</div>
													<!-- /.progress-group -->
												</div>

											</div>
										</div>

									</div>
									<!-- /.col -->


								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

			<!-- new -->

		</div>

	</section>


	<!-- no2 -->
	<div class="row">

		<div class="col-md-4">
			<section id="monthly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					YTD Juli 2018
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">DES Achivement Revenue Ytd Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-red">T</span>

											<div class="info-box-content">
												<span class="info-box-text">TARGET</span>
												<span class="info-box-number">25,5</span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->

									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-green">R</span>

											<div class="info-box-content">
												<span class="info-box-text">REALISASI</span>
												<span class="info-box-number">5,3</span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->

									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-aqua">A</span>

											<div class="info-box-content">
												<span class="info-box-text">ACHIEVEMENT</span>
												<span class="info-box-number" style="color: red">71,11<small>%</small></span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->
								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

			<!-- new -->
		</section>


		<div class="col-md-4">
			<section id="yearly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					Growth MoM
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">DES Growth Revenue MoM Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">

										<div class="row">
											<div class="col-md-12">
												<div class="chart">
													<canvas id="barChartDes" style="height: 235px; width: 510px;" width="410" height="300"></canvas>
												</div>

												<div class="col-md-12" style="margin-top: 15px;">

													<div class="progress-group">
														<span class="progress-text">Juni 2018</span>
														<span class="progress-number">3,6</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text">Juli 2018</span>
														<span class="progress-number">3,5</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text"><b>Growth MoM</b></span>
														<span class="progress-number" style="color: red"><b>-4,01%</b></span>
													</div>
													<!-- /.progress-group -->
												</div>

											</div>
										</div>

									</div>
									<!-- /.col -->


								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

		</section>


		<div class="col-md-4">
			<section id="yearly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					Growth YoY
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">DES Growth Revenue YoY Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">

										<div class="row">
											<div class="col-md-12">
												<div class="chart">
													<canvas id="barChartDes2" style="height: 235px; width: 510px;" width="410" height="300"></canvas>
												</div>

												<div class="col-md-12" style="margin-top: 15px;">

													<div class="progress-group">
														<span class="progress-text">Ytd Juli 2017</span>
														<span class="progress-number">22,8</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text">Ytd Juli 2018</span>
														<span class="progress-number">26,0</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text"><b>Growth YoY</b></span>
														<span class="progress-number" style="color: blue"><b>14,18%</b></span>
													</div>
													<!-- /.progress-group -->
												</div>

											</div>
										</div>

									</div>
									<!-- /.col -->


								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

			<!-- new -->

		</div>

	</section>

	<div class="row">

		<div class="col-md-4">
			<section id="monthly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					YTD Juli 2018
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">DGS Achivement Revenue Ytd Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-red">T</span>

											<div class="info-box-content">
												<span class="info-box-text">TARGET</span>
												<span class="info-box-number">8,3</span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->

									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-green">R</span>

											<div class="info-box-content">
												<span class="info-box-text">REALISASI</span>
												<span class="info-box-number">9,4</span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->

									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-aqua">A</span>

											<div class="info-box-content">
												<span class="info-box-text">ACHIEVEMENT</span>
												<span class="info-box-number" style="color: red">113,35<small>%</small></span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->
								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

			<!-- new -->
		</section>


		<div class="col-md-4">
			<section id="yearly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					Growth MoM
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">DGS Growth Revenue MoM Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">

										<div class="row">
											<div class="col-md-12">
												<div class="chart">
													<canvas id="barChartDgs" style="height: 235px; width: 510px;" width="410" height="300"></canvas>
												</div>

												<div class="col-md-12" style="margin-top: 15px;">

													<div class="progress-group">
														<span class="progress-text">Juni 2018</span>
														<span class="progress-number">1,7</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text">Juli 2018</span>
														<span class="progress-number">1,8</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text"><b>Growth MoM</b></span>
														<span class="progress-number" style="color: blue"><b>5,85%</b></span>
													</div>
													<!-- /.progress-group -->
												</div>

											</div>
										</div>

									</div>
									<!-- /.col -->


								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

		</section>


		<div class="col-md-4">
			<section id="yearly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					Growth YoY
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">DGS Growth Revenue YoY Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">

										<div class="row">
											<div class="col-md-12">
												<div class="chart">
													<canvas id="barChartDgs2" style="height: 235px; width: 510px;" width="410" height="300"></canvas>
												</div>

												<div class="col-md-12" style="margin-top: 15px;">

													<div class="progress-group">
														<span class="progress-text">Ytd Juli 2017</span>
														<span class="progress-number">8,7</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text">Ytd Juli 2018</span>
														<span class="progress-number">9,4</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text"><b>Growth YoY</b></span>
														<span class="progress-number" style="color: blue"><b>7,74%</b></span>
													</div>
													<!-- /.progress-group -->
												</div>

											</div>
										</div>

									</div>
									<!-- /.col -->


								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
		</section>
	</div>
	<!-- /.col -->
	<div class="row">

		<div class="col-md-4">
			<section id="monthly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					YTD Juli 2018
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">DBS Achivement Revenue Ytd Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-red">T</span>

											<div class="info-box-content">
												<span class="info-box-text">TARGET</span>
												<span class="info-box-number">7,4</span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->

									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-green">R</span>

											<div class="info-box-content">
												<span class="info-box-text">REALISASI</span>
												<span class="info-box-number">5,3</span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->

									<div class="col-md-12">
										<div class="info-box">
											<span class="info-box-icon bg-aqua">A</span>

											<div class="info-box-content">
												<span class="info-box-text">ACHIEVEMENT</span>
												<span class="info-box-number" style="color: red">71,11<small>%</small></span>
											</div>
											<!-- /.info-box-content -->
										</div>
										<!-- /.info-box -->
									</div>
									<!-- /.col -->
								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

			<!-- new -->
		</section>


		<div class="col-md-4">
			<section id="yearly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					Growth MoM
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">DBS Growth Revenue MoM Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">

										<div class="row">
											<div class="col-md-12">
												<div class="chart">
													<canvas id="barChartDbs" style="height: 235px; width: 510px;" width="410" height="300"></canvas>
												</div>

												<div class="col-md-12" style="margin-top: 15px;">

													<div class="progress-group">
														<span class="progress-text">Juni 2018</span>
														<span class="progress-number">0,8</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text">Juli 2018</span>
														<span class="progress-number">0,7</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text"><b>Growth MoM</b></span>
														<span class="progress-number" style="color: red"><b>-5,67%</b></span>
													</div>
													<!-- /.progress-group -->
												</div>

											</div>
										</div>

									</div>
									<!-- /.col -->


								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

		</section>


		<div class="col-md-4">
			<section id="yearly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					Growth YoY
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">DBS Growth Revenue YoY Juli 2018</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-12">

										<div class="row">
											<div class="col-md-12">
												<div class="chart">
													<canvas id="barChartDbs2" style="height: 235px; width: 510px;" width="410" height="300"></canvas>
												</div>

												<div class="col-md-12" style="margin-top: 15px;">

													<div class="progress-group">
														<span class="progress-text">Ytd Juli 2017</span>
														<span class="progress-number">6,1</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text">Ytd Juli 2018</span>
														<span class="progress-number">5,3</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text"><b>Growth YoY</b></span>
														<span class="progress-number" style="color: red"><b>-13,34%</b></span>
													</div>
													<!-- /.progress-group -->
												</div>

											</div>
										</div>

									</div>
									<!-- /.col -->


								</div>

							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- ./box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->

		</div>

	</section>
</div>

</section>



</div>





</div>
<!-- /.container -->



@endsection



@section('script')


<script type="text/javascript">
	$(function () {

  'use strict';

    //-------------
    //- BAR CHART - DBS
    //-------------
    var barChartCanvas                   = $('#barChartBges').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Juni 2018', 'Juli 2018'],
      datasets: [
        
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(191, 191, 191,0.9)',
          strokeColor         : 'rgba(191, 191, 191,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(191, 191, 191,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(191, 191, 191,1)',
          data                : [6.1, 6.0]
        }
      ]
    }
    barChartData.datasets[0].fillColor   = 'rgba(191, 191, 191,1)'
    barChartData.datasets[0].strokeColor = 'rgba(191, 191, 191,1)'
    barChartData.datasets[0].pointColor  = 'rgba(191, 191, 191,1)'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions);


    //-------------
    //- BAR CHART - DBS
    //-------------
    var barChartCanvas                   = $('#barChartBges2').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Juli 2017', 'Juli 2018'],
      datasets: [
        
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [37.6, 40.7]
        }
      ]
    }
    barChartData.datasets[0].fillColor   = 'rgba(191, 191, 191,1)'
    barChartData.datasets[0].strokeColor = 'rgba(191, 191, 191,1)'
    barChartData.datasets[0].pointColor  = 'rgba(191, 191, 191,1)'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions);


    //-------------
    //- BAR CHART - DES
    //-------------
    var barChartCanvas                   = $('#barChartDes').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Juni 2018', 'Juli 2018'],
      datasets: [
        
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [3.6, 3.5]
        }
      ]
    }
    barChartData.datasets[0].fillColor   = 'rgba(255, 0, 0,1)'
    barChartData.datasets[0].strokeColor = 'rgba(255, 0, 0,1)'
    barChartData.datasets[0].pointColor  = 'rgba(255, 0, 0,1)'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions);


    //-------------
    //- BAR CHART - DES
    //-------------
    var barChartCanvas                   = $('#barChartDes2').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Juli 2017', 'Juli 2018'],
      datasets: [
        
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [22.8, 26.0]
        }
      ]
    }
    barChartData.datasets[0].fillColor   = 'rgba(255, 0, 0,1)'
    barChartData.datasets[0].strokeColor = 'rgba(255, 0, 0,1)'
    barChartData.datasets[0].pointColor  = 'rgba(255, 0, 0,1)'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions);


    //-------------
    //- BAR CHART - DGS
    //-------------
    var barChartCanvas                   = $('#barChartDgs').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Juni 2018', 'Juli 2018'],
      datasets: [
        
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [1.7, 1.8]
        }
      ]
    }
    barChartData.datasets[0].fillColor   = 'rgba(56, 87, 35,1)'
    barChartData.datasets[0].strokeColor = 'rgba(56, 87, 35,1)'
    barChartData.datasets[0].pointColor  = 'rgba(56, 87, 35,1)'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions);

    //-------------
    //- BAR CHART - DGS
    //-------------
    var barChartCanvas                   = $('#barChartDgs2').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Juni 2018', 'Juli 2018'],
      datasets: [
        
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [8.7, 9.4]
        }
      ]
    }
    barChartData.datasets[0].fillColor   = 'rgba(56, 87, 35,1)'
    barChartData.datasets[0].strokeColor = 'rgba(56, 87, 35,1)'
    barChartData.datasets[0].pointColor  = 'rgba(56, 87, 35,1)'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions);



     //- BAR CHART - DBS
    //-------------
    var barChartCanvas                   = $('#barChartDbs').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Juni 2018', 'Juli 2018'],
      datasets: [
        
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [0.8, 0.7]
        }
      ]
    }
    barChartData.datasets[0].fillColor   = 'rgba(0, 112, 192, 1)'
    barChartData.datasets[0].strokeColor = 'rgba(0, 112, 192, 1)'
    barChartData.datasets[0].pointColor  = 'rgba(0, 112, 192, 1)'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions);

    //-------------
    //- BAR CHART - DBS
    //-------------
    var barChartCanvas                   = $('#barChartDbs2').get(0).getContext('2d')
    var barChart                         = new Chart(barChartCanvas)
    var barChartData                     = {
      labels  : ['Juni 2018', 'Juli 2018'],
      datasets: [
        
        {
          label               : 'Digital Goods',
          fillColor           : 'rgba(60,141,188,0.9)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [6.1, 5.3]
        }
      ]
    }
    barChartData.datasets[0].fillColor   = 'rgba(0, 112, 192, 1)'
    barChartData.datasets[0].strokeColor = 'rgba(0, 112, 192, 1)'
    barChartData.datasets[0].pointColor  = 'rgba(0, 112, 192, 1)'
    var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
    }

    barChartOptions.datasetFill = false
    barChart.Bar(barChartData, barChartOptions);



});



</script>


@endsection
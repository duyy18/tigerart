@extends('layouts.report_layout')

@section('content')

<div class="container">
	<div class="col-md-8 col-md-offset-2">
		<div class="card">
			<h4 class="title">How to find user's chat ID</h4>
			<div style="margin-top: 30px; margin-bottom: 30px" class="col-md-10 col-md-offset-1">
				<div class="alert alert-info">
					This explanation is a little bit technical. Please read it carefully.
				</div>
				<div>
					<p>For the bot to be able to send you a message, it needs to know your chat ID. Think of it as your "phone number" or "address" so the bot knows where to send the message to. Here's how you can find your chat ID:</p>
					<ol>
						<li>
							You need to start a conversation with the bot by sending any message to it. The message, along with any other information related to your account will be kept on the server for about 24 hours before it gets deleted.
							<div class="clear"></div>
							<img style="width: 50%; margin-top: 20px; margin-bottom: 20px" src="{{ url('/images/bot/chat.png') }}">
						</li>
						<li>Visit <a target="__blank" href="{{ route('botIframe') }}">this link</a> and you will see a bunch of messages that were sent to the bot for the past 24 hours.</li>
						<li>Press <b>Ctrl+F</b> (for Windows) or <b>Command+F</b> (for macOS) on your keyboard to find user's name. If you can't find it, try to resend the message to the bot (see point 1).</li>
						<li>
							On the left, you can see a field with the key 'id', <b><em>and that's your chat ID!</em></b>
							<img style="width: 100%; margin-top: 20px; margin-bottom: 20px" src="{{ url('/images/bot/bot.png') }}">
						</li>
						<li>
							Now that you've found your chat ID, you can save it to SiginjaiART database along with your name and the group you belong to.
							<div class="clear"></div>
							<img style="width: 60%; margin-top: 20px; margin-bottom: 20px" src="{{ url('/images/bot/add.png') }}">
						</li>
						<li>And that's it! Now you can compose a message, and the bot will send the message to you.<br />You can see the list of people who have been registered to the SiginjaiART database by visiting <a href="{{ route('recipients') }}">this link</a>.</li>
					</ol>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div style="height: 30px"></div>
	</div>
</div>
@endsection

@section('script')

<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>

<script type="text/javascript">


</script>

@endsection
<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>


<script type="text/javascript">

	
	function sendToMultipleUsers(user_id, message) {

		$.ajaxSetup({
         headers: {
           'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
     });

		for(var i = 0; i < user_id.length; i++) {

			console.log('Sending to ' + user_id[i] + "...");
         
         jQuery.ajax({
           url: "https://api.telegram.org/bot649401424:AAGKo6FrXwi6O00YkXsMp16s9ZvLFCaueLs/sendMessage",
           cache: false,
           method: 'post',
           data: {
             chat_id: user_id[i],
             text: message
          },
          success: function(result){
             console.log("Message is sent to " + result.result.chat.first_name + ".");
          },
          error:function(data){
           console.log("Sending message failed" + ".");
        }
     });

      }
   }

   $(document).ready(function() {
    var user_id = {!! json_encode($user_ids) !!};
    var msg = "{{ $message }}"
    sendToMultipleUsers(user_id, msg);
 });


</script>


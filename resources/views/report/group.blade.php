@extends('layouts.report_layout')

@section('content')

<div class="container">
	<div class="col-md-10 col-md-offset-1">
		<div class="card">
			<h4 class="title" style="margin-bottom: 40px">Recipient Groups</h4>

			<div class="col-md-10 col-md-offset-1 table-responsive">

				@include('includes.message')

				<a style="margin-bottom: 20px" class="pull-right" href="{{ route('createGroup') }}"><button class="btn btn-success">Add group</button></a>

				@if($group)
				<table class="table table-condensed">
					<tr>
						<th class="col-md-8">Group Name</th>
						<th class="col-md-4">Action</th>
					</tr>
					@foreach($group as $g)
					<tr>
						<td>{{ $g->name }}</td>
						<td>
							<a href="{{ route('editGroup', $g->id) }}"><button style="width: 30%" class="pu-table-button green">Edit</button></a>
							<a href="javascript:void(0);" onclick="if(confirm('Are you sure?')) {$(this).find('form').submit()};" >
								<button style="width: 30%" class="pu-table-button pink">Delete</button>
								<form method="post" action="{{ route('deleteGroup', $g->id) }}">
									{{ csrf_field() }}
								</form>
							</a>
						</td>
					</tr>
					@endforeach
				</table>
				
				@else
				<p>Nothing to show.</p>
				@endif

				<div class="clear"></div>
				<p style="padding-top: 5px; padding-bottom: 20px"><a href="{{ route('recipients') }}">Manage the recipients</a></p>

			</div>

			<div class="clear"></div>

			

		</div>
		<div style="height: 30px"></div>
	</div>
</div>
@endsection

@section('script')

<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>


@endsection
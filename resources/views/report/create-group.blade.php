@extends('layouts.report_layout')

@section('content')

<div class="container">
	<div class="col-md-8 col-md-offset-2">
		<div class="card">
			<h4 class="title">Add a Group</h4>
			<form class="col-md-10 col-md-offset-1" method="post" enctype="multipart/form-data" action="{{ route('storeGroup') }}" style="margin-top: 20px; padding-bottom: 40px">

				{{ csrf_field() }}

				<label>Group Name</label>
				<input type="text" name="name" class="form-control" placeholder="Group Name">

				<button style="margin-top: 20px" type="submit" class="btn btn-primary">Save</button>

				<div class="clear"></div>
			</form>
			<div class="clear"></div>
		</div>
		<div style="height: 30px"></div>
	</div>
</div>
@endsection

@section('script')

<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>

<script type="text/javascript">


</script>

@endsection
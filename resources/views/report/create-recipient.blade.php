@extends('layouts.report_layout')

@section('content')

<div class="container">
	<div class="col-md-8 col-md-offset-2">
		<div class="card">
			<h4 class="title">Add a User</h4>
			<form class="col-md-10 col-md-offset-1" method="post" enctype="multipart/form-data" action="{{ route('storeRecipient') }}" style="margin-top: 20px; padding-bottom: 40px">

				{{ csrf_field() }}

				<label>Chat ID</label>
				<input type="text" name="chat_id" class="form-control" placeholder="Chat ID">

				<label style="margin-top: 20px">Name</label>
				<input type="text" name="name" class="form-control" placeholder="Name">

				<label style="margin-top: 20px">Group</label>
				<select name="group" class="form-control">

					@if($group)
						@foreach($group as $g)
						<option value="{{ $g->id }}">{{ $g->name }}</option>
						@endforeach
					@endif
					
				</select>

				<button style="margin-top: 20px" type="submit" class="btn btn-primary">Save</button>

				<div class="clear"></div>

				<p style="margin-top: 20px; margin-bottom: 20px"><a target="__blank" href="{{ route('findChatID') }}">How to find user's chat ID</a></p>
			</form>
			<div class="clear"></div>
		</div>
		<div style="height: 30px"></div>
	</div>
</div>
@endsection

@section('script')

<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>

<script type="text/javascript">


</script>

@endsection
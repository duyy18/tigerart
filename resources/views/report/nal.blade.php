@extends('layouts.report_layout')

@section('addRoute', route('showNalImport'))

@section('title')
LIS & NAL IndiHome
@endsection

@section('content')

<div class="container">

	<div class="col-md-4">
		<section id="target" class="content-header">
			<h1 style="margin-bottom: 10px">
				Milestone LIS Indihome
			</h1>

			<div class="row">
				<div class="col-md-12">
					<div class="box">

						<div class="box-body">
							<img style="width: 100%" src="{{ asset('images/roadmap.jpg') }}">
						</div>
					</div>
				</div>
			</div>

		</section>
	</div>

	<div class="col-md-8" style="padding-left: 0">

		<section id="target" class="content-header">
			<h1 style="margin-bottom: 10px">
				Target
			</h1>

			<div class="row">

				<div class="col-md-6">
					<div class="box" style="min-height: 370px">
						<div class="box-header with-border">
							<h3 class="box-title bt-adm">{{ $target['title'] }}</h3>


							<!-- /.box-header -->
							<div class="box-body">
								<div class="row">
									<div class="col-md-12">

										<div class="progress-group">
											<span class="progress-text">1st Quarter</span>
											<span class="progress-number">{{ $target['q1'] }}</span>

											<div class="progress sm">
												<div class="progress-bar progress-bar-aqua" style="width: 21.5%"></div>
											</div>
										</div>
										<!-- /.progress-group -->
										<div class="progress-group">
											<span class="progress-text">2nd Quarter</span>
											<span class="progress-number">{{ $target['q2'] }}</span>

											<div class="progress sm">
												<div class="progress-bar progress-bar-red" style="width: 22.9%"></div>
											</div>
										</div>
										<!-- /.progress-group -->
										<div class="progress-group">
											<span class="progress-text">3rd Quarter</span>
											<span class="progress-number">{{ $target['q3'] }}</span>

											<div class="progress sm">
												<div class="progress-bar progress-bar-green" style="width: 25.2%"></div>
											</div>
										</div>
										<!-- /.progress-group -->
										<div class="progress-group">
											<span class="progress-text">4th Quarter</span>
											<span class="progress-number">{{ $target['q4'] }}</span>

											<div class="progress sm">
												<div class="progress-bar progress-bar-yellow" style="width: 30.4%"></div>
											</div>
										</div>
										<!-- /.progress-group -->
										<div class="progress-group">
											<span class="progress-text"><b>TOTAL</b></span>
											<span class="progress-number"><b>{{ $target['total'] }}</b></span>
										</div>
										<!-- /.progress-group -->
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- ./box-body -->

						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>

				<div class="col-md-6">
					<div class="box" style="min-height: 370px">
						<div class="box-header with-border">
							<h3 class="box-title bt-adm">{{ $ytd['title'] }}</h3>

						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">

									<div class="row">
										<div class="col-md-12">
											<div class="info-box">
												<span class="info-box-icon bg-red">T</span>

												<div class="info-box-content">
													<span class="info-box-text">TARGET</span>
													<span class="info-box-number">{{ $ytd['target'] }}</span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->

										<div class="col-md-12">
											<div class="info-box">
												<span class="info-box-icon bg-green">R</span>

												<div class="info-box-content">
													<span class="info-box-text">REALISASI</span>
													<span class="info-box-number">{{ $ytd['realisasi'] }}</span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->

										<div class="col-md-12">
											<div class="info-box">
												<span class="info-box-icon bg-aqua">A</span>

												<div class="info-box-content">
													<span class="info-box-text">ACHIEVEMENT</span>
													<span class="info-box-number" id="ytd-ach">{{ $ytd['ach'] }}</span>
												</div>
												<!-- /.info-box-content -->
											</div>
											<!-- /.info-box -->
										</div>
										<!-- /.col -->
									</div>

								</div>
								<!-- /.col -->
							</div>
							<!-- /.row -->
						</div>
						<!-- ./box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->


				<div class="col-md-6">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title bt-adm">Ranking TR1</h3>

						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">

									<div class="info-box bg-green">
										<span class="info-box-icon">1</span>

										<div class="info-box-content">
											<span class="info-box-text box-text-1 rank-label">{{ $ytd['top_1_tr1_name'] }}</span>
											<span class="info-box-text box-text-1 rank-number">{{ $ytd['top_1_tr1_score'] }}</span>
										</div>
										<!-- /.info-box-content -->
									</div>

									<div class="info-box bg-purple">
										<span class="info-box-icon">2</span>

										<div class="info-box-content">
											<span class="info-box-text box-text-1 rank-label">{{ $ytd['top_2_tr1_name'] }}</span>
											<span class="info-box-text box-text-1 rank-number">{{ $ytd['top_2_tr1_score'] }}</span>
										</div>
										<!-- /.info-box-content -->
									</div>

									<div class="info-box bg-light-blue">
										<span class="info-box-icon">{{ $ytd['rank_tr1'] }}</span>

										<div class="info-box-content">
											<span class="info-box-text box-text-1 rank-label">WITEL JAMBI</span>
											<span class="info-box-text box-text-1 rank-number">{{ $ytd['ach'] }}</span>
										</div>
										<!-- /.info-box-content -->
									</div>

								</div>
								<!-- /.col -->
							</div>
							<!-- /.row -->
						</div>
						<!-- ./box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->


				<div class="col-md-6">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title bt-adm">Ranking Nasional</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">

									<div class="info-box bg-red-2">
										<span class="info-box-icon">1</span>

										<div class="info-box-content">
											<span class="info-box-text box-text-1 rank-label">{{ $ytd['top_1_nas_name'] }}</span>
											<span class="info-box-text box-text-1 rank-number">{{ $ytd['top_1_nas_score'] }}</span>
										</div>
										<!-- /.info-box-content -->
									</div>

									<div class="info-box bg-orange-2">
										<span class="info-box-icon">2</span>

										<div class="info-box-content">
											<span class="info-box-text box-text-1 rank-label">{{ $ytd['top_2_nas_name'] }}</span>
											<span class="info-box-text box-text-1 rank-number">{{ $ytd['top_2_nas_score'] }}</span>
										</div>
										<!-- /.info-box-content -->
									</div>

									<div class="info-box bg-coklat">
										<span class="info-box-icon">{{ $ytd['rank_nas'] }}</span>

										<div class="info-box-content">
											<span class="info-box-text box-text-1 rank-label">WITEL JAMBI</span>
											<span class="info-box-text box-text-1 rank-number">{{ $ytd['ach'] }}</span>
										</div>
										<!-- /.info-box-content -->
									</div>

								</div>
								<!-- /.col -->
							</div>
							<!-- /.row -->
						</div>
						<!-- ./box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->


			</div>
		</section>

	</div>



	<!-- SAMPAI DISINI -->

	<div class="row">

		<div class="col-md-6">
			<section id="monthly-growth" class="content-header">
				<h1 style="margin-bottom: 10px">
					Growth MoM Juli 2018
				</h1>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title bt-adm">{{ $mom['title'] }}</h3>


					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">

								<div class="row">
									<div class="col-md-6">

										<div class="row">
											<div class="col-md-12">
												<div class="chart">
													<canvas id="barChart" style="height: 300px; width: 510px;" width="410" height="300"></canvas>
												</div>

												<div class="col-md-12" style="margin-top: 15px;">

													<div class="progress-group">
														<span class="progress-text">{{ date('F Y', strtotime('-1 month')) }}</span>
														<span class="progress-number">{{ $mom['lastMonth'] }}</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text">{{ date('F Y') }}</span>
														<span class="progress-number">{{ $mom['curMonth'] }}</span>
													</div>
													<!-- /.progress-group -->
													<div class="progress-group">
														<span class="progress-text"><b>Growth MoM</b></span>
														<span class="progress-number" id="mom-growth">{{ $mom['gMoM'] }}</span>
													</div>
													<!-- /.progress-group -->
												</div>

											</div>
										</div>

									</div>
									<!-- /.col -->

									<div class="col-md-6">
										<h4>Ranking TR1</h4>

										<table class="table table-striped">
											<tbody><tr>
												<th style="width: 10px">Rank</th>
												<th>Witel</th>
												<th style="width: 40px">Growth</th>
											</tr>
											<tr>
												<td>1.</td>
												<td>{{ $mom['top_1_tr1_name'] }}</td>
												<td><span class="badge bg-red">{{ $mom['top_1_tr_score'] }}</span></td>
											</tr>
											<tr>
												<td>2.</td>
												<td>{{ $mom['top_2_tr1_name'] }}</td>
												<td><span class="badge bg-yellow">{{ $mom['top_2_tr1_score'] }}</span></td>
											</tr>
											<tr style="color: #009688">
												<td style="font-weight: bold">{{ $mom['rank_tr1'] }}.</td>
												<td style="font-weight: bold">WITEL JAMBI</td>
												<td style="font-weight: bold"><span class="badge bg-green-3">{{ $mom['gMoM'] }}</span></td>
											</tr>
										</tbody>
									</table>


									<h4>Ranking Nasional</h4>

									<table class="table table-striped">
										<tbody><tr>
											<th style="width: 10px">Rank</th>
											<th>Witel</th>
											<th style="width: 40px">Growth</th>
										</tr>
										<tr>
											<td>1.</td>
											<td>{{ $mom['top_1_nas_name'] }}</td>
											<td><span class="badge bg-red">{{ $mom['top_1_nas_score'] }}</span></td>
										</tr>
										<tr>
											<td>2.</td>
											<td>{{ $mom['top_2_nas_name'] }}</td>
											<td><span class="badge bg-yellow">{{ $mom['top_2_nas_score'] }}</span></td>
										</tr>
										<tr style="color: #009688">
											<td style="font-weight: bold">{{ $mom['rank_nas'] }}.</td>
											<td style="font-weight: bold">WITEL JAMBI</td>
											<td style="font-weight: bold"><span class="badge bg-green-3">{{ $mom['gMoM'] }}</span></td>
										</tr>
									</tbody>
								</table>
							</div>

						</div>

					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- ./box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->

	<!-- new -->
</section>


<div class="col-md-6">
	<section id="yearly-growth" class="content-header">
		<h1 style="margin-bottom: 10px">
			NAL Month to Date
		</h1>
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title bt-adm">{{ $nalToDate['title'] }}</h3>


			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">

						<div class="row">
							<div class="col-md-6">
								<div class="info-box">
									<span class="info-box-icon bg-red">T</span>

									<div class="info-box-content">
										<span class="info-box-text">Target</span>
										<span class="info-box-number">{{ $nalToDate['target'] }}</span>
									</div>
									<!-- /.info-box-content -->
								</div>
								<!-- /.info-box -->
							</div>
							<!-- /.col -->

							<div class="col-md-6">
								<div class="info-box">
									<span class="info-box-icon bg-green">R</span>

									<div class="info-box-content">
										<span class="info-box-text">Real Mtd</span>
										<span class="info-box-number">{{ $nalToDate['real'] }}</span>
									</div>
									<!-- /.info-box-content -->
								</div>
								<!-- /.info-box -->
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="info-box">
									<span class="info-box-icon bg-blue">A</span>

									<div class="info-box-content">
										<span class="info-box-text">Achievement</span>
										<span class="info-box-number" id="nal-to-date-ach">{{ $nalToDate['ach'] }}</span>
									</div>
									<!-- /.info-box-content -->
								</div>
								<!-- /.info-box -->
							</div>
							<!-- /.col -->
							<div class="col-md-6">
								<div class="info-box">
									<span class="info-box-icon bg-aqua">D</span>

									<div class="info-box-content">
										<span class="info-box-text">Dev</span>
										<span class="info-box-number" id="nal-to-date-dev">{{ $nalToDate['dev'] }}</span>
									</div>
									<!-- /.info-box-content -->
								</div>
								<!-- /.info-box -->
							</div>
							<!-- /.col -->
						</div>

						<div style="font-size: 1.5em">Target hari ini adalah {{ $nalToDate['target2'] }}</div>

					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- ./box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->

	<!-- new -->

</div>

</section>

<div class="row">

	<div class="col-md-6">
		<section id="monthly-growth" class="content-header">
			<h1 style="margin-bottom: 10px">
				Sales per Package
			</h1>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title bt-adm">{{ $sppy['title'] }}</h3>


				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">


							<div class="row">
								<div class="col-md-6">
									<div id="donut-chart" style="height: 188px;"></div>
								</div>


								<div class="col-md-6" style="margin-top: 15px;">

									<div class="progress-group">
										<span class="progress-text"><span style="color: red" class="glyphicon glyphicon-stop"></span> Paket 3P</span>
										<span class="progress-number">{{ $sppy['paket_3p'] }}</span>
									</div>
									<!-- /.progress-group -->
									<div class="progress-group">
										<span class="progress-text"><span style="color: grey" class="glyphicon glyphicon-stop"></span> Paket 2P</span>
										<span class="progress-number">{{ $sppy['paket_2p'] }}</span>
									</div>
									<!-- /.progress-group -->
									<div class="progress-group">
										<span style="margin-left: 17px" class="progress-text"><b>TOTAL</b></span>
										<span class="progress-number"><b>{{ $sppy['total'] }}</b></span>
									</div>
								</div>

								<!-- /.progress-group -->
							</div>
							<!-- row -->
						</div>
						<!-- col -->
					</div>
					<!-- row -->
				</div>
				<!-- ./box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->

		<!-- new -->
	</section>


	<div class="col-md-6">
		<section id="yearly-growth" class="content-header">
			<h1 style="margin-bottom: 10px">
				Sales per Package
			</h1>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title bt-adm">{{ $sppm['title'] }}</h3>


				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">


							<div class="row">
								<div class="col-md-6">
									<div id="donut-chart-2" style="height: 188px;"></div>
								</div>


								<div class="col-md-6" style="margin-top: 15px;">

									<div class="progress-group">
										<span class="progress-text"><span style="color: red" class="glyphicon glyphicon-stop"></span> Paket 3P</span>
										<span class="progress-number">{{ $sppm['paket_3p'] }}</span>
									</div>
									<!-- /.progress-group -->
									<div class="progress-group">
										<span class="progress-text"><span style="color: grey" class="glyphicon glyphicon-stop"></span> Paket 2P</span>
										<span class="progress-number">{{ $sppm['paket_2p'] }}</span>
									</div>
									<!-- /.progress-group -->
									<div class="progress-group">
										<span style="margin-left: 17px" class="progress-text"><b>TOTAL</b></span>
										<span class="progress-number"><b>{{ $sppm['total'] }}</b></span>
									</div>
								</div>

								<!-- /.progress-group -->
							</div>
							<!-- row -->
						</div>
						<!-- col -->
					</div>
					<!-- row -->
				</div>
				<!-- ./box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->

		<!-- new -->

	</div>

</section>

<div class="row">

	<div class="col-md-6">
		<section id="monthly-growth" class="content-header">
			<h1 style="margin-bottom: 10px">
				Activation Type
			</h1>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title bt-adm">{{ $aty['title'] }}</h3>


				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">


							<div class="row">
								<div class="col-md-6">
									<div id="donut-chart-3" style="height: 188px;"></div>
								</div>


								<div class="col-md-6" style="margin-top: 15px;">

									<div class="progress-group">
										<span class="progress-text"><span style="color: red" class="glyphicon glyphicon-stop"></span> New Sales</span>
										<span class="progress-number">{{ $aty['new_sales'] }}</span>
									</div>

									<div class="progress-group">
										<span class="progress-text"><span style="color: #00c0ef !important" class="glyphicon glyphicon-stop"></span> Upselling</span>
										<span class="progress-number">{{ $aty['upselling'] }}</span>
									</div>
									<!-- /.progress-group -->
									<div class="progress-group">
										<span style="margin-left: 17px" class="progress-text"><b>TOTAL</b></span>
										<span class="progress-number"><b>{{ $aty['total'] }}</b></span>
									</div>
								</div>

								<!-- /.progress-group -->
							</div>
							<!-- row -->
						</div>
						<!-- col -->
					</div>
					<!-- row -->
				</div>
				<!-- ./box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->

		<!-- new -->
	</section>


	<div class="col-md-6">
		<section id="yearly-growth" class="content-header">
			<h1 style="margin-bottom: 10px">
				Activation Type
			</h1>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title bt-adm">{{ $atm['title'] }}</h3>


				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">


							<div class="row">
								<div class="col-md-6">
									<div id="donut-chart-4" style="height: 188px;"></div>
								</div>


								<div class="col-md-6" style="margin-top: 15px;">

									<div class="progress-group">
										<span class="progress-text"><span style="color: red" class="glyphicon glyphicon-stop"></span> New Sales</span>
										<span class="progress-number">{{ $atm['new_sales'] }}</span>
									</div>

									<div class="progress-group">
										<span class="progress-text"><span style="color: #00c0ef !important" class="glyphicon glyphicon-stop"></span> Upselling</span>
										<span class="progress-number">{{ $atm['upselling'] }}</span>
									</div>
									<!-- /.progress-group -->
									<div class="progress-group">
										<span style="margin-left: 17px" class="progress-text"><b>TOTAL</b></span>
										<span class="progress-number"><b>{{ $atm['total'] }}</b></span>
									</div>
								</div>
							</div>

							<!-- /.progress-group -->
						</div>
						<!-- row -->
					</div>
					<!-- col -->
				</div>
				<!-- row -->
			</div>
			<!-- ./box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->

	<!-- new -->

</section>


<div class="row">

	<div class="col-md-6">
		<section id="monthly-growth" class="content-header">
			<h1 style="margin-bottom: 10px">
				NAL Contribution
			</h1>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title bt-adm">{{ $nalContY['title'] }}</h3>


				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">


							<div class="row">
								<div class="col-md-6">
									<div id="donut-chart-5" style="height: 188px;"></div>
								</div>


								<div class="col-md-6" style="margin-top: 15px;">

									<div class="progress-group">
										<span class="progress-text"><span style="color: red" class="glyphicon glyphicon-stop"></span> Kandatel Muara Bungo</span>
										<span class="progress-number">{{ $nalContY['bungo'] }}</span>
									</div>

									<div class="progress-group">
										<span class="progress-text"><span style="color: #00c0ef !important" class="glyphicon glyphicon-stop"></span> Jambi Inner</span>
										<span class="progress-number">{{ $nalContY['inner'] }}</span>
									</div>
									<!-- /.progress-group -->
									<div class="progress-group">
										<span class="progress-text"><span style="color: grey" class="glyphicon glyphicon-stop"></span> Jambi Outer</span>
										<span class="progress-number">{{ $nalContY['outer'] }}</span>
									</div>
								</div>

								<!-- /.progress-group -->
							</div>
							<!-- row -->
						</div>
						<!-- col -->
					</div>
					<!-- row -->
				</div>
				<!-- ./box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->

		<!-- new -->
	</section>


	<div class="col-md-6">
		<section id="yearly-growth" class="content-header">
			<h1 style="margin-bottom: 10px">
				NAL Contribution
			</h1>
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title bt-adm">{{ $nalContM['title'] }}</h3>


				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">


							<div class="row">
								<div class="col-md-6">
									<div id="donut-chart-6" style="height: 188px;"></div>
								</div>


								<div class="col-md-6" style="margin-top: 15px;">

									<div class="progress-group">
										<span class="progress-text"><span style="color: red" class="glyphicon glyphicon-stop"></span> Kandatel Muara Bungo</span>
										<span class="progress-number">{{ $nalContM['bungo'] }}</span>
									</div>

									<div class="progress-group">
										<span class="progress-text"><span style="color: #00c0ef !important" class="glyphicon glyphicon-stop"></span> Jambi Inner</span>
										<span class="progress-number">{{ $nalContM['inner'] }}</span>
									</div>
									<!-- /.progress-group -->
									<div class="progress-group">
										<span class="progress-text"><span style="color: grey" class="glyphicon glyphicon-stop"></span> Jambi Outer</span>
										<span class="progress-number">{{ $nalContM['outer'] }}</span>
									</div>
								</div>

								<!-- /.progress-group -->
							</div>
							<!-- row -->
						</div>
						<!-- col -->
					</div>
					<!-- row -->
				</div>
				<!-- ./box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->

		<!-- new -->

	</div>

</section>

<div class="row">
	<div class="col-md-12">
		<section id="yearly-growth" class="content-header">



			<div class="box">
				<div class="box-header with-border">

				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<div class="row table-responsive" style="padding-left: 10px; padding-right: 10px">

						<table class="table table-striped">
							<tbody>

								<tr>
									<th>NAL</th>
									<th>Bangko</th>
									<th>Jambi Centrum</th>
									<th>Kota Baru</th>
									<th>Kuala Tungkal</th>
									<th>Mendalo</th>
									<th>Muara Bulian</th>
									<th>Muara Bungo</th>
									<th>Muara Tebo</th>
									<th>Pangkalan Bulian</th>
									<th>Pasir Putih</th>
									<th>Rimbo Bujang</th>
									<th>Sarolangun</th>
									<th>Sungai Penuh</th>
									<th>Telanai Pura</th>
								</tr>

								<tr>
									<td style="font-weight: bold;">Line in Service Indihome</td>
									
									@foreach($lisIndihome as $li)
									<td>{{ $li }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">Line in Service Non Indihome</td>
									
									@foreach($lisNonIndihome as $lni)
									<td>{{ $lni }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">NAL Contribution</td>
									
									@foreach($nalCont as $nalc)
									<td>{{ $nalc }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">Target NAL</td>
									
									@foreach($targetNal as $tnal)
									<td>{{ $tnal }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">Realisasi NAL</td>
									
									@foreach($realisasiNal as $rnal)
									<td>{{ $rnal }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">Achievement</td>
									
									@foreach($ach as $a)
									<td>{{ $a }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">Rank Achievement</td>
									
									@foreach($rankAch as $ra)
									<td>{{ $ra }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">Dev</td>
									
									@foreach($devYtd as $d)
									<td>{{ $d }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">{{ $gMom['title'] }}</td>
									
									@foreach($gMom['data'] as $gm)
									<td>{{ $gm }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">{{ $targetM['title'] }}</td>
									
									@foreach($targetM['data'] as $tm)
									<td>{{ $tm }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">{{ $realisasiM['title'] }}</td>
									
									@foreach($realisasiM['data'] as $rm)
									<td>{{ $rm }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">Dev</td>
									
									@foreach($devM as $dm)
									<td>{{ $dm }}</td>
									@endforeach

								</tr>

								<tr>
									<td style="font-weight: bold;">{{ $targetB['title'] }}</td>
									
									@foreach($targetB['data'] as $tb)
									<td>{{ $tb }}</td>
									@endforeach

								</tr>

							</tbody>
						</table>

					</section>
				</div>
			</div>


		</div>
		<!-- /.container -->
	</div>
	@endsection


	@section('script')

	<script type="text/javascript">

		var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
		var barChart                         = new Chart(barChartCanvas)
		var barChartData                     = {
			labels  : ['{{ date('M Y', strtotime("-1 month")) }}', '{{ date('M Y') }}'],
			datasets: [

			{
				label               : 'Growth MoM',
				fillColor           : 'rgba(60,141,188,0.9)',
				strokeColor         : 'rgba(60,141,188,0.8)',
				pointColor          : '#3b8bba',
				pointStrokeColor    : 'rgba(60,141,188,1)',
				pointHighlightFill  : '#fff',
				pointHighlightStroke: 'rgba(60,141,188,1)',
				data                : [{{ $mom['lastMonth'] }}, {{ $mom['curMonth'] }}]
			}
			]
		}
		barChartData.datasets[0].fillColor   = '#e53935'
		barChartData.datasets[0].strokeColor = '#e53935'
		barChartData.datasets[0].pointColor  = '#e53935'
		var barChartOptions                  = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero        : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : true,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - If there is a stroke on each bar
      barShowStroke           : true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth          : 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing         : 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing       : 1,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to make the chart responsive
      responsive              : true,
      maintainAspectRatio     : true
  }

  barChartOptions.datasetFill = false
  barChart.Bar(barChartData, barChartOptions)




  var donutData = [
  { label: 'Paket 3P', data: parseInt({{ $sppy['paket_3p'] }}), color: '#ef5350', percentage: parseFloat('{{ $sppy['paket_3p_percent'] }}') },
  { label: 'Paket 2P', data: parseInt({{ $sppy['paket_2p'] }}), color: '#546e7a', percentage: parseFloat('{{ $sppy['paket_2p_percent'] }}') }
  ]
  $.plot('#donut-chart', donutData, {
  	series: {
  		pie: {
  			show       : true,
  			radius     : 1,
  			innerRadius: 0.4,
  			label      : {
  				show     : true,
  				radius   : 2 / 3,
  				formatter: labelFormatter,
  				threshold: 0.1
  			}

  		}
  	},
  	legend: {
  		show: false
  	}
  })

  var donutData = [
  { label: 'Paket 3P', data: parseInt({{ $sppm['paket_3p'] }}), color: '#ef5350', percentage: parseFloat('{{ $sppm['paket_3p_percent'] }}') },
  { label: 'Paket 2P', data: parseInt({{ $sppm['paket_2p'] }}), color: '#546e7a', percentage: parseFloat('{{ $sppm['paket_2p_percent'] }}') }
  ]
  $.plot('#donut-chart-2', donutData, {
  	series: {
  		pie: {
  			show       : true,
  			radius     : 1,
  			innerRadius: 0.4,
  			label      : {
  				show     : true,
  				radius   : 2 / 3,
  				formatter: labelFormatter,
  				threshold: 0.1
  			}

  		}
  	},
  	legend: {
  		show: false
  	}
  })

  var donutData = [
  { label: 'New Sales', data: parseInt({{ $aty['new_sales'] }}), color: '#ef5350', percentage: parseFloat('{{ $aty['new_sales_percent'] }}') },
  { label: 'Upselling', data: parseInt({{ $aty['upselling'] }}), color: '#1db3e4', percentage: parseFloat('{{ $aty['upselling_percent'] }}') }
  ]
  $.plot('#donut-chart-3', donutData, {
  	series: {
  		pie: {
  			show       : true,
  			radius     : 1,
  			innerRadius: 0.4,
  			label      : {
  				show     : true,
  				radius   : 2 / 3,
  				formatter: labelFormatter,
  				threshold: 0.1
  			}

  		}
  	},
  	legend: {
  		show: false
  	}
  })

  var donutData = [
  { label: 'New Sales', data: parseInt({{ $atm['new_sales'] }}), color: '#ef5350', percentage: parseFloat('{{ $atm['new_sales_percent'] }}') },
  { label: 'Upselling', data: parseInt({{ $atm['upselling'] }}), color: '#1db3e4', percentage: parseFloat('{{ $atm['upselling_percent'] }}') }
  ]
  $.plot('#donut-chart-4', donutData, {
  	series: {
  		pie: {
  			show       : true,
  			radius     : 1,
  			innerRadius: 0.4,
  			label      : {
  				show     : true,
  				radius   : 2 / 3,
  				formatter: labelFormatter,
  				threshold: 0.1
  			}

  		}
  	},
  	legend: {
  		show: false
  	}
  })

  var donutData = [
  { label: 'Kandatel Muara Bungo', data: parseInt({{ $nalContY['bungo'] }}), color: '#ef5350', percentage: parseFloat('{{ $nalContY['bungo_percent'] }}') },
  { label: 'Jambi Inner', data: parseInt({{ $nalContY['inner'] }}), color: '#1db3e4', percentage: parseFloat('{{ $nalContY['inner_percent'] }}') },
  { label: 'Jambi Outer', data: parseInt({{ $nalContY['outer'] }}), color: '#546e7a', percentage: parseFloat('{{ $nalContY['outer_percent'] }}') }
  ]
  $.plot('#donut-chart-5', donutData, {
  	series: {
  		pie: {
  			show       : true,
  			radius     : 1,
  			innerRadius: 0.4,
  			label      : {
  				show     : true,
  				radius   : 2 / 3,
  				formatter: labelFormatter,
  				threshold: 0.1
  			}

  		}
  	},
  	legend: {
  		show: false
  	}
  })

  var donutData = [
  { label: 'Kandatel Muara Bungo', data: parseInt({{ $nalContM['bungo'] }}), color: '#ef5350', percentage: parseFloat('{{ $nalContM['bungo_percent'] }}') },
  { label: 'Jambi Inner', data: parseInt({{ $nalContM['inner'] }}), color: '#1db3e4', percentage: parseFloat('{{ $nalContM['inner_percent'] }}') },
  { label: 'Jambi Outer', data: parseInt({{ $nalContM['outer'] }}), color: '#546e7a', percentage: parseFloat('{{ $nalContM['outer_percent'] }}') }
  ]
  $.plot('#donut-chart-6', donutData, {
  	series: {
  		pie: {
  			show       : true,
  			radius     : 1,
  			innerRadius: 0.4,
  			label      : {
  				show     : true,
  				radius   : 2 / 3,
  				formatter: labelFormatter,
  				threshold: 0.1
  			}

  		}
  	},
  	legend: {
  		show: false
  	}
  })

  function labelFormatter(label, series) {
  	console.log(series)
  	return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
  	+ label
  	+ '<br>'
  	+ series.data[0][1] + " (" + series.percentage + "%)" + '</div>'

  }

  // check ytd achievement
  if(parseFloat($('#ytd-ach').html()) < 100) {
  	$('#ytd-ach').css('color', 'red');
  }else{
  	$('#ytd-ach').css('color', 'green');
  }

  //check mom growth
  if(parseFloat($('#mom-growth').html()) < 0) {
  	$('#mom-growth').css('color', 'red');
  }else{
  	$('#mom-growth').css('color', 'green');
  }

  //check ntd ach
  if(parseFloat($('#nal-to-date-ach').html()) < 100) {
  	$('#nal-to-date-ach').css('color', 'red');
  }else{
  	$('#nal-to-date-dev').css('color', 'green');
  }

  //check ntd dev
  if(parseFloat($('#nal-to-date-dev').html()) < 0) {
  	$('#nal-to-date-dev').css('color', 'red');
  }else{
  	$('#nal-to-date-dev').css('color', 'green');
  }

</script>

@endsection
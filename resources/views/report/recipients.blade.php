@extends('layouts.report_layout')

@section('content')

<div class="container">
	<div class="col-md-10 col-md-offset-1">
		<div class="card">
			<h4 class="title" style="margin-bottom: 40px">Telegram Bot Recipients</h4>

			<div class="col-md-10 col-md-offset-1 table-responsive">

				@include('includes.message')

				<a style="margin-bottom: 20px" class="pull-right" href="{{ route('createRecipient') }}"><button class="btn btn-success">Add user</button></a>

				@if($rec)
				<table class="table table-condensed">
					<tr>
						<th>Chat ID</th>
						<th>Name</th>
						<th>Group</th>
						<th>Action</th>
					</tr>
					@foreach($rec as $r)
					<tr>
						<td>{{ $r->chat_id }}</td>
						<td>{{ $r->name }}</td>
						<td>{{ $r->group->name }}</td>
						<td>
							<a href="{{ route('editRecipient', $r->id) }}"><button style="width: 30%" class="pu-table-button green">Edit</button></a>
							<a href="javascript:void(0);" onclick="if(confirm('Are you sure?')) {$(this).find('form').submit()};" >
								<button style="width: 30%" class="pu-table-button pink">Delete</button>
								<form method="post" action="{{ route('deleteRecipient', $r->id) }}">
									{{ csrf_field() }}
								</form>
							</a>
						</td>
					</tr>
					@endforeach
				</table>

				{{ $rec->links() }}
				
				@else
				<p>Nothing to show.</p>
				@endif

				<div class="clear"></div>
				<p style="padding-top: 20px"><a href="{{ route('groupIndex') }}">Manage the recipient groups</a></p>

			</div>

			<div class="clear"></div>

			

		</div>
		<div style="height: 30px"></div>
	</div>
</div>
@endsection

@section('script')

<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>


@endsection
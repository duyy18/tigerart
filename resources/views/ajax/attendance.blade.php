<h3 class="pu-title">Attendance</h3>
<div class="pu-body">
	<div class="col-md-9" id="attendance-content">

		@if(count($attendance) > 0)

		<table class="table pu-table">

			<tr>
				<th class="table-label">No.</th>
				<th class="table-label">Name</th>
				<th class="table-label">NIK</th>
				<th class="table-label">Unit</th>
				<th class="table-label">Time</th>
			</tr>

			@foreach($attendance as $key=>$item)

			<tr>
				<td>{{ $key+1 }}</td>
				<td>{{ $item->name }}</td>
				<td>{{ $item->nik }}</td>
				<td>{{ $item->unit->name }}</td>
				<td>{{ date('d-m-Y H:i', strtotime($item->created_at)) }}</td>
			</tr>

			@endforeach

		</table>

		@else

		<p style="text-align: center; margin-top: 20px;">Nothing to show.</p>

		@endif
		
	</div>
	<div class="col-md-3">
		<button class="pu-table-button blue" id="qr-code-toggle">Show QRCode</button>
		<form method="post" action="{{ route('attendanceExport') }}">
			{{ csrf_field() }}
			<input type="hidden" name="filename" value="{{ date('d-m-Y', strtotime($meeting->time)) . ' - ' . $meeting->title }}">
			<textarea style="display: none" id="hidden-table" name="table"></textarea>
			<button type="submit" class="pu-table-button green">Export as CSV</button>
		</form>
		
		@if(Auth::user()->role == 1)
		<a href="{{ route('attendanceSetting') }}"><button class="pu-table-button pink" id="qr-code-toggle">Configure the URL</button></a>
		@endif
		
	</div>
</div>

<div style="display: none" id="attendance-qr">
	<img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(460)->generate('http://' . $url . '/attendance/submission/' . $meeting_id)) !!} ">
</div>

<script type="text/javascript">
	
	$('#qr-code-toggle').click(function() {
		if($(this).html() == 'Show QRCode') {
			$(this).html('Show List');
			$('#attendance-content').html($('#attendance-qr').html());
		}else{
			$(this).html('Show QRCode');
			sendAjax("{{ route('ajaxAttendance', $meeting_id) }}")
		}
	});

	$('#hidden-table').html($('#attendance-content').html());

</script>
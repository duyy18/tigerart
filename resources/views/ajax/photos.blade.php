<h3 class="pu-title">Photos</h3>
<div class="pu-body">

	@if(!empty($sp->photos))
	
	@foreach (unserialize($sp->photos) as $photo)
	
	<div class="simple_img_gallery col-md-3 media-container">
		<a href="{{ asset('images/togetherness/' . $cat . '/' . $photo) }}"><img class="img-thumbnail" src="{{ asset('images/togetherness/' . $cat . '/thumbs/' . $photo) }}"></a>
	</div>

	@endforeach

	@else

	<div style="text-align: center; margin-top: 50px">
		<p>Nothing to show.</p>
	</div>

	@endif
	
	<div class="clear"></div>
</div>

<script type="text/javascript">
	$(function() {
		$('.simple_img_gallery').createSimpleImgGallery();
	});
</script>
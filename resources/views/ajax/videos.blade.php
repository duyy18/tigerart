<h3 class="pu-title">Videos</h3>
<div class="pu-body">

	@if(!empty($sp->videos))
	
	@foreach (unserialize($sp->videos) as $video)
	
	<div class="col-md-3 media-container video-box">
		<p class="hidden video_id">{{ $video }}</p>
		<a target="__blank" href="{{ route('videoPlayer', $video) }}">
			<img style="cursor: pointer;" class="img-thumbnail" src="https://img.youtube.com/vi/{{ $video }}/hqdefault.jpg">
		</a>
	</div>

	@endforeach

	@else

	<div style="text-align: center; margin-top: 50px">
		<p>Nothing to show.</p>
	</div>

	@endif

	<div class="clear"></div>
</div>

<script type="text/javascript">
	$(function() {
		$('.video-box').click(function() {
			var id = $(this).find($('.video_id')).html();
			console.log(id)
		});
	});
</script>
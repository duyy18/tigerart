@extends('layouts.togetherness_layout')

@section('section-title', 'solving the problems')

@section('unit-menu')

@foreach($unit as $item)
<li><a href="{{ route('togetherSp', $item->id) }}">{{ $item->name }}</a></li>
@endforeach

@endsection

@section('main-content')

<style type="text/css">
input[type=file] {
	margin-bottom: 10px;
}

select {
	width: 100%;
}

textarea {
	resize: none;
}

.edit-image {
	padding: 0;
	margin-left: 12px;
}

.edit-image img {
	width: 100%;
}

.container1 {
	margin-bottom: 20px;
}



.container {
	position: relative;
}

.image {
	opacity: 1;
	display: block;
	height: auto;
	transition: .5s ease;
	backface-visibility: hidden;
}

.middle {
	cursor: pointer;
	transition: .5s ease;
	opacity: 0;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	-ms-transform: translate(-50%, -50%);
	text-align: center;
}

.container:hover .image {
	opacity: 0.3;
}

.container:hover .middle {
	opacity: 1;
}

.text {
	font-size: .8em;
	background-color: red;
	color: white;
}

</style>
<div class="main-content-content container-fluid">

	<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">

		@include('includes.message')

		<form action="{{ route('sp-put', $data->id) }}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="put" />
			<div class="form-group col-md-12">
				<label for="title">Activity</label>	
				<input value="{{ $data->title }}"  class="form-control" type="text" name="title" placeholder="Activity" required>
			</div>
			<div class="form-group col-md-12">
				<label for="date">Time</label>	
				<input value="{{ date("Y-m-d\TH:i:s", strtotime($data->time)) }}" class="form-control" type="datetime-local" name="time" placeholder="Time" required>
			</div>
			<div class="form-group col-md-12">
				<label for="date">Location</label>	
				<input value="{{ $data->location }}" class="form-control" type="text" name="location" placeholder="Location" required>
			</div>
			<div class="form-group col-md-12">
				<label for="desc">Description</label>	
				<textarea rows="5" class="form-control" name="description" placeholder="Description">{{ $data->description }}</textarea>
			</div>
			<div class="form-group col-md-12">
				<label for="cat">Unit</label><br />
				<select class="form-control" name="unit" id="unit" required>

					@foreach($unit as $item)
					<option value="{{ $item->id }}">{{ $item->name }}</option>
					@endforeach
				
				</select>
			</div>
			<div class="container1 col-md-12">
				<label>Photos</label>
				<div class="clear"></div>

				@if(!empty($data->photos))

				@foreach (unserialize($data->photos) as $photo)

				<div class="col-md-2 edit-image container">
					<img class="image" src="{{ asset('images/togetherness/sp/thumbs/' . $photo) }}">
					<a onclick="return confirm('Are you sure you want to delete this item?');" href="{{ route('SpDeletePhoto', ['photo' => $data->id, 'meeting_id' => $photo]) }}">
						<div class="middle">
							<div class="text">Delete</div>
						</div>
					</a>
				</div>

				@endforeach

				@else

				<div>
					<p>This activity has no photo.</p>
				</div>

				@endif

			</div>
			<div class="container1 col-md-12">
				<label>Mom</label>
				<input type="file" name="mom" accept=".pdf">
			</div>

			<button class="btn btn-sm btn-primary" style="width: 100%; margin: 40px 0 0 0; ">Save</button>

		</form>

		<form onclick="return confirm('Are you sure you want to delete this activity?');" action="{{ route('sp-destroy', $data->id) }}" method="post">
			<input type="hidden" name="_method" value="delete" />
			{!! csrf_field() !!}
			<button class="btn btn-sm btn-danger" style="width: 100%; margin: 5px 0 0 0; ">Delete</button>
		</form>
	</div>

</div>

@endsection

@section('script')

<script type="text/javascript">
	$("#unit").val("{{ $data->unit_id }}")
</script>

@endsection
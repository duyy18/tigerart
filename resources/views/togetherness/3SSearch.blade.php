@extends('layouts.togetherness_layout')

@section('section-title', 'solid speed smart')

@section('unit-menu')

@foreach($unit as $item)
<li><a href="{{ route('together3S', $item->id) }}">{{ $item->name }}</a></li>
@endforeach

@endsection

@section('add-route')
{{ route('add-3s-route') }}
@endsection

@section('main-content')

<div class="main-content-content container-fluid">

	@include('includes.message')

	{{-- Unit Box --}}
	<div class="box-container t-box-container col-xs-12">
		<div class="box border-red">
			<div class="box-title bg-red" style="text-align: left; padding-left: 15px">Search results</div>
			<div class="image-container">

				@if(count($es) > 0)

				@foreach($es as $item)

				<div class="es-div col-md-3">
					<p class="es-id">{{ $item->id }}</p>
					<a href="{{ route('edit-3s-route', $item->id) }}"><div class="es-close-btn">
						<span class="glyphicon glyphicon-edit"></span>
					</div></a>
					<div class="es-content-container well">

						@if(strlen($item->title) > 34)
						<p class="es-title">{{ $item->title }} ...</p>
						@else
						<p class="es-title">{{ $item->title }}</p>
						@endif

						<p class="es-date"><span class="glyphicon glyphicon-time"></span> {{ date("l, d-m-Y", strtotime($item->time)) }}</p>
						<p class="es-desc">

							@if(strlen($item->description) > 150)
							{{ substr($item->description, 0, 140) }} <span class="btn-desc">[Read More]</span>
							@else
							{{ $item->description }}
							@endif

						</p>
						<div class="es-photo-video">
							<button class="btn-photos es-button half blue"><span class="glyphicon glyphicon-picture"></span> Photos</button>
							<button class="btn-videos es-button half green"><span class="glyphicon glyphicon-facetime-video"></span> Videos</button>
						</div>
					</div>
				</div>

				@endforeach

				<div class="clear"></div>
				
				{{ $es->links() }}

				@else

				<p style="text-align: center; padding-bottom: 15px">Nothing was found, sorry :(</p>

				@endif

				<div class="clear"></div>
			</div>
		</div>
	</div>

</div>

@endsection


@section('script')

<script type="text/javascript">

	function sendAjax(get_url) {

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
		});

		jQuery.ajax({
			url: get_url,
			cache: false,
			method: 'get',
			success: function(result){
				$('.pu-content').html(result);
			},
			error:function(data){
				console.log("Sending message failed" + ".");
			}
		});
	}

	function popupFocus() {
		$('.t-main-content').css({'filter': 'blur(11px)', '-webkit-filter': 'blur(11px)', 'height': '100vh'});
		$('.overlay').fadeIn(150);
	}

	$('.pu-close-btn').click(function() {
		$('.t-main-content').css({'filter': 'blur(0)', '-webkit-filter': 'blur(0)', 'height': 'auto'});
		$('.overlay').fadeOut(200);
		$('.pu-content').html('');
	});

	$('.btn-desc').click(function() {
		var itemId = $(this).parent().parent().parent().find($('.es-id')).html();
		popupFocus();
		sendAjax("{{ url('togetherness/3s/ajax/description') }}/" + itemId);
	});

	$('.btn-photos').click(function() {
		var itemId = $(this).parent().parent().parent().find($('.es-id')).html();
		popupFocus();
		sendAjax("{{ url('togetherness/3s/ajax/photos') }}/" + itemId);
	});

	$('.btn-videos').click(function() {
		var itemId = $(this).parent().parent().parent().find($('.es-id')).html();
		popupFocus();
		sendAjax("{{ url('togetherness/3s/ajax/videos') }}/" + itemId);
	})
</script>

@endsection
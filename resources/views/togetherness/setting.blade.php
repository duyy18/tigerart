@extends('layouts.togetherness_layout')

@section('section-title', 'attendance setting')

@section('unit-menu')

@foreach($unit as $item)
<li><a href="{{ route('togetherSp', $item->id) }}">{{ $item->name }}</a></li>
@endforeach

@endsection

@section('main-content')

<style type="text/css">
input[type=file] {
	margin-bottom: 10px;
}

select {
	width: 100%;
}

textarea {
	resize: none;
}

</style>
<div class="main-content-content container-fluid">

	<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">

		@include('includes.message')

		<form action="{{ route('attendanceSettingPost') }}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-group col-md-12">
				<label for="title">Server Hostname or IP Address</label>	
				<input id="url" class="form-control" type="text" name="url" placeholder="www.example.com or 192.168.1.1" value="{{ $url }}" required autocomplete="off">
				<div style="font-size: .9em; margin-top: 20px">
					<p>Fill the field above with the hostname or IP address of the server where SiginjaiART is hosted.</p>
					<p>
						The meeting attendees will be redirected to <br /> <u>http://<b id="url-p">{{ $url }}</b>/attendance/submission/&#60;meeting_id&#62;</u> <br />
						after scanning the QR code.
					</p>
				</div>
			</div>
			<button class="btn btn-sm btn-primary" style="width: 36%; margin: 20px 32%; ">Save</button>
			
		</form>
	</div>

</div>

@endsection

@section('script')
<script type="text/javascript">

	$("#url").on("paste keyup", function() {
		$('#url-p').html($(this).val());
	});

</script>
@endsection
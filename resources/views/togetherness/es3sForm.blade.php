@extends('layouts.togetherness_layout')

@section('section-title')
{{ $title }}
@endsection

@section('unit-menu')

@foreach($unit as $item)
<li><a href="{{ route($index_route, $item->id) }}">{{ $item->name }}</a></li>
@endforeach

@endsection

@section('main-content')

<style type="text/css">
	input[type=file], input[type=url] {
		margin-bottom: 10px;
	}

	select {
		width: 100%;
	}

	textarea {
		resize: none;
	}

</style>
<div class="main-content-content container-fluid">

	<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">
		
		@include('includes.message')

		<form action="{{ $es3sFormAction }}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-group col-md-12">
				<label for="title">Activity</label>	
				<input class="form-control" type="text" name="title" placeholder="Activity" required>
			</div>
			<div class="form-group col-md-12">
				<label for="date">Date</label>	
				<input class="form-control" type="datetime-local" name="time" placeholder="Time" required>
			</div>
			<div class="form-group col-md-12">
				<label for="desc">Description</label>	
				<textarea name="description" rows="5" class="form-control" placeholder="Description"></textarea>
			</div>
			<div class="form-group col-md-12">
				<label for="cat">Unit</label><br />
				<select class="form-control" name="unit" required>
					@foreach($unit as $item)
					<option value="{{ $item->id }}">{{ $item->name }}</option>
					@endforeach

				</select>
			</div>
			<div class="container1 col-md-6">
				<label>Photos</label>
				<input class="form-control" type="file" name="im_0" accept=".jpg, .jpeg, .png">
				<input class="form-control" type="file" name="im_1" accept=".jpg, .jpeg, .png">
				<input class="form-control" type="file" name="im_2" accept=".jpg, .jpeg, .png">
				<input class="form-control" type="file" name="im_3" accept=".jpg, .jpeg, .png">
				<input class="form-control" type="file" name="im_4" accept=".jpg, .jpeg, .png">
			</div>
			<div class="container1 col-md-6">
				<label>Videos</label>
				<input class="form-control" type="url" name="vid_0" placeholder="YouTube url here">
				<input class="form-control" type="url" name="vid_1" placeholder="YouTube url here">
				<input class="form-control" type="url" name="vid_2" placeholder="YouTube url here">
				<input class="form-control" type="url" name="vid_3" placeholder="YouTube url here">
				<input class="form-control" type="url" name="vid_4" placeholder="YouTube url here">
			</div>
			<button class="btn btn-sm btn-primary" style="width: 36%; margin: 40px 32%; ">Submit</button>
			
		</form>
	</div>

</div>

@endsection
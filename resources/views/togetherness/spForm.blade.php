@extends('layouts.togetherness_layout')

@section('section-title', 'solving the problems')

@section('unit-menu')

@foreach($unit as $item)
<li><a href="{{ route('togetherSp', $item->id) }}">{{ $item->name }}</a></li>
@endforeach

@endsection

@section('main-content')

<style type="text/css">
	input[type=file] {
		margin-bottom: 10px;
	}

	select {
		width: 100%;
	}

	textarea {
		resize: none;
	}

</style>
<div class="main-content-content container-fluid">

	<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">

		@include('includes.message')

		<form action="{{ route('sp-post') }}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="form-group col-md-12">
				<label for="title">Activity</label>	
				<input class="form-control" type="text" name="title" placeholder="Activity" required>
			</div>
			<div class="form-group col-md-12">
				<label for="date">Time</label>	
				<input class="form-control" type="datetime-local" name="time" placeholder="Time" required>
			</div>
			<div class="form-group col-md-12">
				<label for="date">Location</label>	
				<input class="form-control" type="text" name="location" placeholder="Location" required>
			</div>
			<div class="form-group col-md-12">
				<label for="desc">Description</label>	
				<textarea rows="5" class="form-control" name="description" placeholder="Description"></textarea>
			</div>
			<div class="form-group col-md-12">
				<label for="cat">Unit</label><br />
				<select class="form-control" name="unit" required>

					@foreach($unit as $item)
					<option value="{{ $item->id }}">{{ $item->name }}</option>
					@endforeach

				</select>
			</div>
			<div class="container1 col-md-6">
				<label>Photos</label>
				<input type="file" name="im_0" accept=".jpg, .jpeg, .png">
				<input type="file" name="im_1" accept=".jpg, .jpeg, .png">
				<input type="file" name="im_2" accept=".jpg, .jpeg, .png">
				<input type="file" name="im_3" accept=".jpg, .jpeg, .png">
				<input type="file" name="im_4" accept=".jpg, .jpeg, .png">
			</div>
			<div class="container1 col-md-6">
				<label>Mom</label>
				<input type="file" name="mom" accept=".pdf">
			</div>
			<button class="btn btn-sm btn-primary" style="width: 36%; margin: 40px 32%; ">Submit</button>
			
		</form>
	</div>

</div>

@endsection
<!DOCTYPE html>
<html>
<head>
	<title>Video Player</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
</head>
<body style="background: #000">
	<div class="video_player" style="padding-top: 30px; width: 100%">
		<iframe style="width: 900px; height: 510px; margin: 0 auto; display: block" src="https://www.youtube.com/embed/{{ $id }}?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
</body>
</html>
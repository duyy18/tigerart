@extends('layouts.attendance')

@section('content')

@include('includes.message')

<form action="{{ route('attendancePost') }}" method="POST" >
	{{ csrf_field() }}
	<input type="hidden" name="meeting_id" value="{{ $meeting_id }}">
	<div class="form-group">
		<label for="name">Name</label>
		<input class="form-control" type="text" name="name" id="name" placeholder="Name">
	</div>
	<div class="form-group">
		<label for="nik">NIK</label>
		<input class="form-control" name="nik" id="nik" placeholder="NIK"
			oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
			type="number"
			maxlength="6" 
		>
	</div>
	<div class="form-group">
		<label for="unit">Unit</label>
		<select class="form-control" name="unit">
			@foreach($unit as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
			@endforeach
		</select>
	</div>
	<button class="att-submit-btn btn btn-primary" type="submit">Submit</button>
</form>

@endsection
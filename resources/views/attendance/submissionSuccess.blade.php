@extends('layouts.attendance')

@section('content')

<div class="col-md-12 col-sm-12 col-xs-12 att-success">
	<p class="att-ty">Thank You!</p>
	<p>Your attendance has been submitted.</p>
</div>

@endsection
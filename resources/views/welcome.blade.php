<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8" http-equiv="refresh, content=1">
	<title>Tiger ART</title>

	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">

	<link rel="stylesheet" href="{{ asset('slick/slick.css') }}"/>
	<link rel="stylesheet" href="{{ asset('slick/slick-theme.css') }}"/>
	<link rel="stylesheet" href="{{ asset('css/home.css') }}"/>

</head>

<body>
	<div class="background">
		<div class="bg-contents">
			<div class="logo-striving">
				<img src="{{ asset('images/home/striving.png') }}">
			</div>

			<div class="logo-telkom">
				<img src="{{ asset('images/home/logo.png') }}">
			</div>

			<div class="logo-siginjai">
				<img src="{{ asset('images/home/phone.png') }}">
			</div>

			<div class="peta-sumatera">
				<img src="{{ asset('images/home/map.png') }}">
			</div>

			<div class="content-container" style="width: 100%"></div>

		</div>

		<div class="slick-starter">
			<div class="slick-bg bg1">

			</div>
			<div class="slick-bg bg2">

			</div>
			<div class="slick-bg bg3">

			</div>
		</div>
	</div>



	<div class="hidden" id="home_content">
		<div class="logo-main" style="padding-top:10%">
			<img id="foo" src="{{ asset('images/home/tigers_art.png') }}" alt="siginjai-art" usemap="#siginjai-art">
			<map name="#siginjai-art">
				<area title="Siginjai" shape="rect" href="{{ '#' }}" alt="Siginjai">
				<area title="Area" onclick="show_area()" id="area_link" shape="rect" coords="355,125,401,196" href="javascript:void(0)" alt="Area">
				<area title="Report Performance" onclick="show_report()" id="reports_link" shape="rect" coords="406,126,452,197" href="javascript:void(0)" alt="Reports">
				<area title="Togetherness" onclick="togetherness_report()" id="togetherness_link" shape="rect" coords="456,127,497,198" href="javascript:void(0)" alt="Togetherness">
			</map>
		</div>
	</div>

	<div class="hidden" id="area_content">
		<div class="menu-section">
			<h1 style="margin-bottom: 20px; font-family: 'montez'"><strong class="first-letter">A</strong>rea Profiling</h1>
			<div class="menu-list col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-md-4 col-sm-4 col-xs-12">
				<p>Provinsi Jambi</p>  
				<p class="sub-menu">Geographic Map</p>
				<p class="sub-menu">Dataran Tinggi dan Rendah</p>
				<p class="sub-menu">Kecamatan dan Desa</p>
				<p class="sub-menu">Penduduk</p>
				<p class="sub-menu">Komoditi</p>
				<p class="sub-menu">Fasilitas Kesehatan</p>  
				<p class="sub-menu">Hotel dan Pariwisata</p>
			</div>
			<div class="menu-list col-md-4 col-sm-4 col-xs-12">
				<p>Witel Jambi</p>   
				<p class="sub-menu">Revenue</p>
				<p class="sub-menu">Line in Service</p>
				<p class="sub-menu">Alat Produksi</p>
				<p class="sub-menu">Backbone</p>
				<p class="sub-menu">Telkomsel</p>
			</div>

		</div>
		<a class="control-buttons control-back" onclick="show_home()" href="javascript:void(0)">
			<i class="fa fa-arrow-left"></i> Back
		</a>

		<a class="control-buttons control-next" href="{{ route('areaShowHome') }}">
			Next <i class="fa fa-arrow-right"></i>
		</a>
	</div>

	<div class="hidden" id="report_content">
		<div class="menu-section">
			<h1 style="margin-bottom: 20px; font-family: 'montez'"><strong class="first-letter">R</strong>eport Performance</h1>
			<div class="menu-list col-lg-offset-2 col-md-offset-2 col-sm-offset-2 col-md-4 col-sm-4 col-xs-12">
				<p>Financial Performance</p>  
				<p class="sub-menu"><a href="{{ route('showRevenue') }}">Revenue Consumer</a></p>
				<p class="sub-menu">Revenue BGES</p>
				<p class="sub-menu">Revenue Wholesale</p>  
			</div>
			<div class="menu-list col-md-4 col-sm-4 col-xs-12">
				<p>Operational Performance</p>   
				<p class="sub-menu"><a href="{{ route('showNal') }}">LIS & NAL IndiHome</a></p>
				<p class="sub-menu"><a href="{{ route('progress_new') }}">Closing PSB Kendala</a></p>
				<p class="sub-menu">Team</p>
				<p class="sub-menu">MOBI</p>
				<p class="sub-menu">Scaling BGES</p>
				<p class="sub-menu">Sales Wifi.id</p>
				<p class="sub-menu">Collection</p>
				<p class="sub-menu">SQUADRON</p>
			</div>
		</div>

		<a class="control-buttons control-back" onclick="show_home()" href="javascript:void(0)">
			<i class="fa fa-arrow-left"></i> Back
		</a>

	</div>

	<div class="hidden" id="togetherness_content">
		<div class="menu-section">
			<h1 style="margin-bottom: 50px; font-family: 'montez'"><strong class="first-letter">T</strong>ogetherness</h1>
			<div class="menu-list col-md-offset-3 col-sm-offset-3 col-md-2 col-sm-2 col-xs-12">
				<a href="{{ route('togetherEs', 1) }}" class="t_div">
					<img src="{{ asset('images/home/t_1.png') }}">
					<p>towards excellent service quality</p>
				</a>
			</div>
			<div class="menu-list col-md-2 col-sm-2 col-xs-12">
				<a href="{{ route('together3S', 1) }}" class="t_div">
					<img src="{{ asset('images/home/t_2.png') }}">
					<p>solid speed smart</p>
				</a>
			</div>
			<div class="menu-list col-md-2 col-sm-2 col-xs-12">
				<a href="{{ route('togetherSp', 1) }}" class="t_div">
					<img src="{{ asset('images/home/t_3.png') }}">
					<p>solving the problems</p>
				</a>
			</div>
		</div>

		<a class="control-buttons control-back" onclick="show_home()" href="javascript:void(0)">
			<i class="fa fa-arrow-left"></i> Back
		</a>

	</div>



	<script src="{{ asset('js/jquery.js') }}"></script>
	<script src="{{ asset('slick/slick.min.js') }}"></script>
	<script src="{{ asset('js/home.js') }}"></script>

	<script>

		function show_home() {
			$(".content-container").html($("#home_content").html());
		}

		function show_area() {
			$(".content-container").html($("#area_content").html());
		}

		function show_report() {
			$(".content-container").html($("#report_content").html());
		}

		function togetherness_report() {
			$(".content-container").html($("#togetherness_content").html());
		}

		$(document).ready(function(){

			show_home();

			$(".slick-starter").slick({
				dots:false,
				arrows:false,
				fade:true,
				speed:1000,
				autoplay:true,
				autoplaySpeed:2000,
				cssEase:'linear'
			});

		});
	</script>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Telkom Area</title>
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.gallery.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
</head>
<body>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
					<img src="{{ asset('images/logo.png') }}">
				</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="header-title" style="font-family: 'montez'; font-size: 2.5em"><a href="{{ url('/area') }}">Area Profiling</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ url('/') }}">Home</a></li>
					<li><a href="{{ route('logout') }}" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">Logout</a></li>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>

	<div class="clear"></div>

	<div class="container-fluid main-content-container">

		<div class="col-md-8 col-md-offset-2 main-content-grid">
			
			<form method="post" action="{{ route('areaUpdate') }}">

				{{ csrf_field() }}
				
				<div class="card main-content">
					<div class="card-title main-content-title">
						<strong>Witel Jambi</strong>
					</div>
					<div class="main-content-content container-fluid">

						@include('includes.message')

						<div class="box-container col-xs-12">
							<div class="box border-blue" id="section-revenue">
								<div class="box-title bg-blue">Revenue</div>
								<div class="image-container text-container" >
									<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 np">
										<table>
											<tr>
												<th colspan="2" style="font-size: 1.1em;">Segmen Consumer</th>
											</tr>
											<tr>
												<td>Ytd <input style="width: 70%" type="text" name="revenue_segment_consumer_periode" value="{{ $w->revenue_segment_consumer_periode }}"></td>
												<td>Rp. <input style="width: 50%" type="text" name="revenue_segment_consumer" value="{{ $w->revenue_segment_consumer }}"> M</td>
											</tr>
										</table>
									</div>
									<div class="col-md-5 col-lg-5 col-sm-5 col-xs-12 np" style="left: -10px; position: relative;">
										<table>
											<tr>&nbsp;</tr>
											<tr>
												<th colspan="2">Product</th>
											</tr>
											<tr>
												<td style="padding-right: 15px">- Revenue IndiHome</td>
												<td>Rp. <input style="width: 50%" type="text" name="revenue_indihome" value="{{ $w->revenue_indihome }}"> M</td>
											</tr>
											<tr>
												<td style="padding-right: 15px">- Revenue Non IndiHome</td>
												<td>Rp. <input style="width: 50%" type="text" name="revenue_non_indihome" value="{{ $w->revenue_non_indihome }}"> M</td>
											</tr>
										</table>
									</div>
									<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 np" style="padding-left: 10px">
										<table>
											<tr>&nbsp;</tr>
											<tr>
												<th>Service</th>
											</tr>
											<tr>
												<td style="padding-right: 15px">- Legacy</td>
												<td>Rp. <input style="width: 50%" type="text" name="revenue_legacy" value="{{ $w->revenue_legacy }}"> M</td>
											</tr>
											<tr>
												<td style="padding-right: 15px">- Connectivity</td>
												<td>Rp. <input style="width: 50%" type="text" name="revenue_connectivity" value="{{ $w->revenue_connectivity }}"> M</td>
											</tr>
											<tr>
												<td style="padding-right: 15px">- Digital Service</td>
												<td>Rp. <input style="width: 50%" type="text" name="revenue_digital_service" value="{{ $w->revenue_digital_service }}"> M</td>
											</tr>
										</table>
									</div>

									<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np">
										<table>
											<tr>
												<th colspan="2" style="font-size: 1.1em;">Segmen BGES</th>
											</tr>
											<tr>
												<td style="padding-right: 15px">Ytd BGES</td>
												<td>Rp. <input style="width: 50%" type="text" name="revenue_ytd_bges" value="{{ $w->revenue_ytd_bges }}"></td>
											</tr>
											<tr>
												<td style="padding-right: 15px">Ytd ES</td>
												<td>Rp. <input style="width: 50%" type="text" name="revenue_ytd_es" value="{{ $w->revenue_ytd_es }}"></td>
											</tr>
										</table>
									</div>

									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 np">
										<table>
											<tr>
												&nbsp;
											</tr>
											<tr>
												<td style="padding-right: 15px">Ytd GS</td>
												<td>Rp. <input style="width: 50%" type="text" name="revenue_ytd_gs" value="{{ $w->revenue_ytd_gs }}"></td>
											</tr>
											<tr>
												<td style="padding-right: 15px">Ytd BS</td>
												<td>Rp. <input style="width: 50%" type="text" name="revenue_ytd_bs" value="{{ $w->revenue_ytd_bs }}"></td>
											</tr>
										</table>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

						<div class="box-container col-xs-12">
							<div class="box border-yellow">
								<div class="box-title bg-yellow">Line in Service</div>
								<div class="image-container text-container">
									<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np">
										<table>
											<tr>&nbsp;</tr>
											<tr>
												<th>LIS <input style="width: 50%" type="text" name="lis_year_date" value="{{ $w->lis_year_date }}"></th>
											</tr>
											<tr>
												<th>NAL Ytd <input style="width: 50%" type="text" name="nal_year_date" value="{{ $w->nal_year_date }}"></th>
											</tr>
											<tr>
												<th>LIS <input style="width: 50%" type="text" name="lis_month_date" value="{{ $w->lis_month_date }}"></th>
											</tr>
										</table>
									</div>
									<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np" style="padding-left: 10px">
										<table>
											<tr>
												<th>IndiHome</th>
											</tr>
											<tr>
												<td><input style="width: 50%" type="text" name="lis_i_year" value="{{ $w->lis_i_year }}"> ssl</td>
											</tr>
											<tr>
												<td><input style="width: 50%" type="text" name="nal_i_year" value="{{ $w->nal_i_year }}"> ssl</td>
											</tr>
											<tr>
												<td><input style="width: 50%" type="text" name="lis_i_month" value="{{ $w->lis_i_month }}"> ssl</td>
											</tr>
										</table>
									</div>
									<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np" style="padding-left: 10px">
										<table>
											<tr>
												<th>Non IndiHome</th>
											</tr>
											<tr>
												<td><input style="width: 50%" type="text" name="lis_n_year" value="{{ $w->lis_n_year }}"> ssl</td>
											</tr>
											<tr>
												<td><input style="width: 50%" type="text" name="nal_n_year" value="{{ $w->nal_n_year }}"> ssl</td>
											</tr>
											<tr>
												<td><input style="width: 50%" type="text" name="lis_n_month" value="{{ $w->lis_n_month }}"> ssl</td>
											</tr>
										</table>
									</div>
									<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np" style="padding-left: 10px">
										<table>
											<tr>
												<th>Total</th>
											</tr>
											<tr>
												<td><input style="width: 50%" type="text" name="lis_t_year" value="{{ $w->lis_t_year }}"> ssl</td>
											</tr>
											<tr>
												<td><input style="width: 50%" type="text" name="nal_t_year" value="{{ $w->nal_t_year }}"> ssl</td>
											</tr>
											<tr>
												<td><input style="width: 50%" type="text" name="lis_t_month" value="{{ $w->lis_t_month }}"> ssl</td>
											</tr>
										</table>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

						<div class="box-container col-xs-12">
							<div class="box border-green">
								<div class="box-title bg-green">Alat Produksi</div>
								<div class="image-container text-container">
									<div class="col-md-3 np">
										<table>
											<tr>
												<td style="padding-right: 15px">ODC</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_odc" value="{{ $w->apro_odc }}"></td>
											</tr>
											<tr>
												<td style="padding-right: 15px">ODP</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_odp" value="{{ $w->apro_odp }}"></td>
											</tr>
											<tr>
												<td style="padding-right: 15px">GPON</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_gpon" value="{{ $w->apro_gpon }}"></td>
											</tr>
											<tr>
												<td style="padding-right: 15px">MSAN</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_msan" value="{{ $w->apro_msan }}"></td>
											</tr>
											<tr>
												<td style="padding-right: 15px">DSLAM</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_dslam" value="{{ $w->apro_dslam }}"></td>
											</tr>
											<tr>
												<td style="padding-right: 15px">R-DSLAM</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_rdslam" value="{{ $w->apro_rdslam }}"></td>
											</tr>
										</table>
									</div>
									<div class="col-md-3 np" style="padding-left: 10px">
										<table>
											<tr>
												<th colspan="2">ODP</th>
											</tr>
											<tr style="color: #000">
												<td style="padding-right: 15px">ODP BLACK</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_d_black" value="{{ $w->apro_d_black }}"></td>
											</tr>
											<tr style="color: #009e07">
												<td style="padding-right: 15px">ODP GREEN</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_d_green" value="{{ $w->apro_d_green }}"></td>
											</tr>
											<tr style="color: #d6ad0a">
												<td style="padding-right: 15px">ODP YELLOW</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_d_yellow" value="{{ $w->apro_d_yellow }}"></td>
											</tr>
											<tr style="color: #f00">
												<td style="padding-right: 15px">ODP RED</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_d_red" value="{{ $w->apro_d_red }}"></td>
											</tr>
											<tr style="color: #000">
												<td style="padding-right: 15px">TOTAL</td>
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_d_total" value="{{ $w->apro_d_total }}"></td>
											</tr>
										</table>
									</div>
									<div class="col-md-3 np" style="padding-left: 10px">
										<table>
											<tr>
												<th class="ta-r">OCC</th>
											</tr>
											<tr style="color: #000">
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_c_black" value="{{ $w->apro_c_black }}">%</td>
											</tr>
											<tr style="color: #009e07">
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_c_green" value="{{ $w->apro_c_green }}">%</td>
											</tr>
											<tr style="color: #d6ad0a">
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_c_yellow" value="{{ $w->apro_c_yellow }}">%</td>
											</tr>
											<tr style="color: #f00">
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_c_red" value="{{ $w->apro_c_red }}">%</td>
											</tr>
											<tr style="color: #000">
												<td class="ta-r"><input style="width: 50%" type="text" name="apro_c_total" value="{{ $w->apro_c_total }}">%</td>
											</tr>
										</table>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

						<div class="box-container col-xs-12">
							<div class="box border-red" id="section-telkomsel">
								<div class="box-title bg-red">Telkomsel</div>
								<div class="image-container text-container">
									<div class="col-md-3 np">
										<table>
											<tr>
												<th colspan="2">Market Share</th>
											</tr>
											<tr>
												<td style="padding-right: 15px">- Telkomsel</td>
												<td><input style="width: 50%" type="text" name="tsel_tsel" value="{{ $w->tsel_tsel }}">%</td>
											</tr>
											<tr>
												<td style="padding-right: 15px">- Indosat</td>
												<td><input style="width: 50%" type="text" name="tsel_indosat" value="{{ $w->tsel_indosat }}">%</td>
											</tr>
											<tr>
												<td style="padding-right: 15px">- XL Axiata</td>
												<td><input style="width: 50%" type="text" name="tsel_xl" value="{{ $w->tsel_xl }}">%</td>
											</tr>
											<tr>
												<td style="padding-right: 15px">- 3</td>
												<td><input style="width: 50%" type="text" name="tsel_3" value="{{ $w->tsel_3 }}">%</td>
											</tr>
										</table>
									</div>
									<div class="col-md-6 np" style="padding-left: 10px">
										<table>
											<tr>
												<th colspan="2">Node-B</th>
											</tr>
											<tr>
												<td style="padding-right: 20px">- High (400-700 GB)</td>
												<td><input style="width: 50%" type="text" name="tsel_high" value="{{ $w->tsel_high }}"> Node</td>
											</tr>
											<tr>
												<td style="padding-right: 20px">- Medium (200-400 GB)</td>
												<td><input style="width: 50%" type="text" name="tsel_medium" value="{{ $w->tsel_medium }}"> ssl</td>
											</tr>
											<tr>
												<td style="padding-right: 20px">- Low (<200 GB)</td>
													<td><input style="width: 50%" type="text" name="tsel_low" value="{{ $w->tsel_low }}"> Node</td>
												</tr>
											</table>
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>

							<div class="clear"></div>

							<div style="padding-top: 20px; padding-left: 15px">
								<button type="submit" class="btn btn-primary">Save</button>
							</div>

						</div>
					</div>

				</form>

			</div>

		</div>

		<div class="clear"></div>

		<div class="footer">
			Copyright &copy; <strong>Telkom Indonesia Witel Jambi</strong> - 2018
		</div>

		<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

		<script type="text/javascript">

		</script>

	</body>
	</html>
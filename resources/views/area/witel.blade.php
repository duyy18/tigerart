@extends('layouts.area_layout')


@section('main-content')

<div class="col-md-9 main-content-grid">
	<div class="card main-content">
		<div class="card-title main-content-title">
			<strong>Kantor Witel Jambi</strong>
		</div>
		<div class="main-content-content container-fluid">

			<div class="box-container col-xs-12">
				<div class="box border-green" style="margin-bottom: 25px;">
					<div class="box-title bg-green">Today</div>
					<div class="image-container">
						@foreach (unserialize($sto->photos) as $photo)
						<div class="simple_img_gallery image-row image-row-sto col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/' . $photo) }}"><img src="{{ asset('images/photos/' . $photo) }}"></a>
						</div>
						@endforeach
						<div class="clear"></div>
					</div>
				</div>
				<div class="box border-blue">
					<div class="box-title bg-blue">In the Future</div>
					<div class="image-container row-home">
						<div class=" simple_img_gallery image-row col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/WTL_later_1.jpg') }}"><img src="{{ asset('images/photos/WTL_later_1.jpg') }}"></a>
						</div>
						<div class="simple_img_gallery image-row col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/WTL_later_2.jpg') }}"><img src="{{ asset('images/photos/WTL_later_2.jpg') }}"></a>
						</div>
						<div class="simple_img_gallery image-row col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/WTL_later_3.jpg') }}"><img src="{{ asset('images/photos/WTL_later_3.jpg') }}"></a>
						</div>
						<div class="simple_img_gallery image-row col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/WTL_later_4.jpg') }}"><img src="{{ asset('images/photos/WTL_later_4.jpg') }}"></a>
						</div>
						<div class="simple_img_gallery image-row col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/WTL_later_5.jpg') }}"><img src="{{ asset('images/photos/WTL_later_5.jpg') }}"></a>
						</div>
						<div class="simple_img_gallery image-row col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/WTL_later_6.jpg') }}"><img src="{{ asset('images/photos/WTL_later_6.jpg') }}"></a>
						</div>
						<div class="simple_img_gallery image-row col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/WTL_later_7.jpg') }}"><img src="{{ asset('images/photos/WTL_later_7.jpg') }}"></a>
						</div>
						<div class="simple_img_gallery image-row col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/WTL_later_8.jpg') }}"><img src="{{ asset('images/photos/WTL_later_8.jpg') }}"></a>
						</div>
						<div class="simple_img_gallery image-row col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/WTL_later_9.jpg') }}"><img src="{{ asset('images/photos/WTL_later_9.jpg') }}"></a>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		<div class="clear"></div>

		<div class="building-data container-fluid">
			<div class="col-md-6 npl">
				<table class="table main-table table-hover">
					<tr>
						<th class="table-label bg-blue" colspan="2">DATA ASET</th>
					</tr>
					<tr>
						<th>Alamat</th>
						<td>{{ $sto->address }}</td>
					</tr>
					<tr>
						<th>Luas Tanah</th>
						<td>{{ $sto->ground_area }} m<sup>2</sup></td>
					</tr>
					<tr>
						<th>Luas Bangunan</th>
						<td>{{ $sto->building_area }} m<sup>2</sup></td>
					</tr>
					<tr>
						<th>Daya</th>
						<td>{{ $sto->electrical_capacity }}</td>
					</tr>
					<tr>
						<th>Batas Lahan</th>
						<td>{!! nl2br($sto->border) !!}</td>
					</tr>
					<tr>
						<th>Properti Sekitar</th>
						<td>{{ $sto->surrounding_properties }}</td>
					</tr>
				</table>
			</div>

			<div class="col-md-6 npr">
				<table class="table main-table table-hover">
					<tr>
						<th class="table-label bg-green" colspan="2">RINCIAN LAHAN</th>
					</tr>
					<tr>
						<th>Lebar Jalan</th>
						<td>{{ $sto->lane_width }}</td>
					</tr>
					<tr>
						<th>Bentuk Lahan</th>
						<td>{{ $sto->ground_shape }}</td>
					</tr>
					<tr>
						<th>Kondisi Lahan</th>
						<td>{{ $sto->ground_condition }}</td>
					</tr>
				</table>
			</div>
		</div>

	</div>
</div>
</div>

@endsection


@section('script')

<script type="text/javascript"  src="{{ asset('js/jquery.gallery.js') }}"></script>

<script>

	$('.image-row').each(function() {
		var img = $(this).find('img');
		if($(this).width() / $(this).height() < img.width() / img.height()) {
			img.height("100%");
			img.width("auto");
		}else{
			img.height("auto");
			img.width("100%");
		}
	});

	$(function() {
		$('.simple_img_gallery').createSimpleImgGallery();
	});

</script>

@endsection
@extends('layouts.area_layout')


@section('main-content')

<div class="col-md-9 main-content-grid">
	<div class="card main-content">
		<div class="card-title main-content-title">
			<strong>{{ $sto->sto_desc }}</strong> ({{ $sto->id }})
		</div>
		<div class="main-content-content container-fluid">

			<div class="box-container col-md-12 col-xs-12">
				<div class="box border-red">
					<div class="box-title bg-red">Gedung</div>
					<div class="image-container">
						@foreach (unserialize($sto->photos) as $photo)
						<div class="simple_img_gallery image-row image-row-sto col-md-3 col-sm-4 col-xs-12">
							<a href="{{ asset('images/photos/' . $photo) }}"><img src="{{ asset('images/photos/' . $photo) }}"></a>
						</div>
						@endforeach
						<div class="clear"></div>
					</div>
				</div>
			</div>

			<div class="clear"></div>

			<form action="{{ route('updateSTO', $sto->id) }}" method="post">

			{{ csrf_field() }}

			<div class="building-data container-fluid">
				<div class="col-md-6 npl">
					<table class="table main-table table-hover">
						<tr>
							<th class="table-label bg-blue" colspan="2">ASET</th>
						</tr>
						<tr>
							<th>Alamat</th>
							<td><input type="text" name="address" value="{{ $sto->address }}"></td>
						</tr>
						<tr>
							<th>Luas Tanah</th>
							<td><input type="number" name="ground_area" value="{{ $sto->ground_area }}"> m<sup>2</sup></td>
						</tr>
						<tr>
							<th>Luas Bangunan</th>
							<td><input type="number" name="building_area" value="{{ $sto->building_area }}"> m<sup>2</sup></td>
						</tr>
						
						@if(!$sto->id == 'JMB')
						<tr>
							<th>Daya</th>
							<td><input type="text" name="electrical_capacity" value="{{ $sto->electrical_capacity }}"></td>
						</tr>
						@endif

						<tr>
							<th>Batas Lahan</th>
							<td><textarea name="border" rows="8" cols="35">{{ $sto->border }}</textarea></td>
						</tr>
						<tr>
							<th>Properti Sekitar</th>
							<td><input type="text" name="surrounding_properties" value="{{ $sto->surrounding_properties }}"></td>
						</tr>
						<tr>
							<th>Latitude</th>
							<td><input type="text" name="lat" value="{{ $sto->lat }}"></td>
						</tr>
						<tr>
							<th>Longitude</th>
							<td><input type="text" name="lng" value="{{ $sto->lng }}"></td>
						</tr>
					</table>
				</div>

				<div class="col-md-6 npr">
					<table class="table main-table table-hover">
						<tr>
							<th class="table-label bg-green" colspan="2">LAHAN</th>
						</tr>
						<tr>
							<th>Lebar Jalan</th>
							<td><input type="text" name="lane_width" value="{{ $sto->lane_width }}"></td>
						</tr>
						<tr>
							<th>Bentuk Lahan</th>
							<td><input type="text" name="ground_shape" value="{{ $sto->ground_shape }}"></td>
						</tr>
						<tr>
							<th>Kondisi Lahan</th>
							<td><input type="text" name="ground_condition" value="{{ $sto->ground_condition }}"></td>
						</tr>
					</table>
				</div>

				@if($sto->id == 'JMB')

				<div class="col-md-6 npr">
					<table class="table main-table table-hover">
						<tr>
							<th class="table-label bg-red" colspan="6">ALPRO</th>
						</tr>
						<tr>
							<th style="width: 16.5%; font-size: .9em">ODC</th>
							<th style="width: 16.5%; font-size: .9em">ODP</th>
							<th style="width: 16.5%; font-size: .9em">GPON</th>
							<th style="width: 16.5%; font-size: .9em">MSAN</th>
							<th style="width: 16.5%; font-size: .9em">DSLAM</th>
							<th style="width: 16.5%; font-size: .9em">R-DSLAM</th>
						</tr>
						<tr>
							<td>90</td>
							<td>3526</td>
							<td>4</td>
							<td>25</td>
							<td>-</td>
							<td>-</td>
						</tr>
					</table>
				</div>

				<div class="col-md-12 np">
					<table class="table main-table table-hover">
						<tr>
							<th class="table-label bg-yellow" colspan="5">LINE IN SERVICE</th>
						</tr>
						<tr>
							<th style="width: 29%; font-size: .9em">Line in Service Indihome</th>
							<td style="width: 14%; font-size: .9em">22,672</td>

							<td style="width: 14%; font-size: .9em"></td>
							
							<th style="width: 29%; font-size: .9em">Line in Service Non Indihome</th>
							<td style="width: 14%; font-size: .9em">0</td>
						</tr>
						<tr>
							<th style="width: 29%; font-size: .9em">Nal Contribution</th>
							<td style="width: 14%; font-size: .9em">60.5%</td>

							<td style="width: 14%; font-size: .9em"></td>
							
							<th style="width: 29%; font-size: .9em">Target Nal</th>
							<td style="width: 14%; font-size: .9em">6,281</td>
						</tr>
						<tr>
							<th style="width: 29%; font-size: .9em">Realisasi NAL</th>
							<td style="width: 14%; font-size: .9em">6,015</td>

							<td style="width: 14%; font-size: .9em"></td>
							
							<th style="width: 29%; font-size: .9em">Ach</th>
							<td style="width: 14%; font-size: .9em">95.77%</td>
						</tr>
						<tr>
							<th style="width: 29%; font-size: .9em">Rank Ach</th>
							<td style="width: 14%; font-size: .9em">3</td>

							<td style="width: 14%; font-size: .9em"></td>
							
							<th style="width: 29%; font-size: .9em">Dev Ytd</th>
							<td style="width: 14%; font-size: .9em">-266</td>
						</tr>
						<tr>
							<th style="width: 29%; font-size: .9em">Growth MoM</th>
							<td style="width: 14%; font-size: .9em">46.90%</td>

							<td style="width: 14%; font-size: .9em"></td>
							
							<th style="width: 29%; font-size: .9em">Target Mtd 26/8/18</th>
							<td style="width: 14%; font-size: .9em">1,123</td>
						</tr>
						<tr>
							<th style="width: 29%; font-size: .9em">Realisasi Mtd 26/8/18</th>
							<td style="width: 14%; font-size: .9em">939</td>

							<td style="width: 14%; font-size: .9em"></td>
							
							<th style="width: 29%; font-size: .9em">Dev Mtd</th>
							<td style="width: 14%; font-size: .9em">-184</td>
						</tr>
						<tr>
							<th style="width: 29%; font-size: .9em">Target 28/8/18</th>
							<td style="width: 14%; font-size: .9em">70</td>

							<td style="width: 14%; font-size: .9em"></td>
							
							<th style="width: 29%; font-size: .9em"></th>
							<td style="width: 14%; font-size: .9em"></td>
						</tr>
					</table>
				</div>

				@endif

			</div>

			<button class="btn btn-primary">Save</button>

			</form>

			<div class="clear"></div>

		</div>
	</div>
</div>

@endsection


@section('script')

<script type="text/javascript"  src="{{ asset('js/jquery.gallery.js') }}"></script>

<script>

	$('.image-row').each(function() {
		var img = $(this).find('img');
		if($(this).width() / $(this).height() < img.width() / img.height()) {
			img.height("100%");
			img.width("auto");
		}else{
			img.height("auto");
			img.width("100%");
		}
	});

	$(function() {
		$('.simple_img_gallery').createSimpleImgGallery();
	});

</script>

@endsection
@extends('layouts.area_layout')


@section('main-content')

<div class="col-md-9 main-content-grid">
	<div class="card main-content">
		<div class="card-title main-content-title">
			Add a New STO
		</div>
		<div class="main-content-content container-fluid">

			<form action="{{ route('storeSTO') }}" method="post" enctype="multipart/form-data">

			<input type="text" name="name" class="form-control" placeholder="Nama STO">
			<input size="3" type="text" name="abbr" class="form-control" placeholder="Singkatan">

			<div class="box-container col-md-12 col-xs-12">
				<div class="box border-red">
					<div class="box-title bg-red">Foto gedung</div>
					<input type="file" name="bld_0" class="form-control" placeholder="Photo 1">
					<input type="file" name="bld_1" class="form-control" placeholder="Photo 2">
					<input type="file" name="bld_2" class="form-control" placeholder="Photo 3">
					<input type="file" name="bld_3" class="form-control" placeholder="Photo 4">
					<input type="file" name="bld_4" class="form-control" placeholder="Photo 5">
				</div>
			</div>

			<div class="clear"></div>

			{{ csrf_field() }}

			<div class="building-data container-fluid">
				<div class="col-md-6 npl">
					<table class="table main-table table-hover">
						<tr>
							<th class="table-label bg-blue" colspan="2">ASET</th>
						</tr>
						<tr>
							<th>Alamat</th>
							<td><input type="text" name="address"></td>
						</tr>
						<tr>
							<th>Luas Tanah</th>
							<td><input type="number" name="ground_area"> m<sup>2</sup></td>
						</tr>
						<tr>
							<th>Luas Bangunan</th>
							<td><input type="number" name="building_area"> m<sup>2</sup></td>
						</tr>

						<tr>
							<th>Batas Lahan</th>
							<td><textarea name="border" rows="8" cols="35"></textarea></td>
						</tr>
						<tr>
							<th>Properti Sekitar</th>
							<td><input type="text" name="surrounding_properties" ></td>
						</tr>
						<tr>
							<th>Latitude</th>
							<td><input type="text" name="lat" ></td>
						</tr>
						<tr>
							<th>Longitude</th>
							<td><input type="text" name="lng" ></td>
						</tr>
					</table>
				</div>

				<div class="col-md-6 npr">
					<table class="table main-table table-hover">
						<tr>
							<th class="table-label bg-green" colspan="2">LAHAN</th>
						</tr>
						<tr>
							<th>Lebar Jalan</th>
							<td><input type="text" name="lane_width"></td>
						</tr>
						<tr>
							<th>Bentuk Lahan</th>
							<td><input type="text" name="ground_shape"></td>
						</tr>
						<tr>
							<th>Kondisi Lahan</th>
							<td><input type="text" name="ground_condition"></td>
						</tr>
					</table>
				</div>

			</div>

			<button class="btn btn-primary">Save</button>

			</form>

			<div class="clear"></div>

		</div>
	</div>
</div>

@endsection


@section('script')

<script type="text/javascript"  src="{{ asset('js/jquery.gallery.js') }}"></script>

<script>

	$('.image-row').each(function() {
		var img = $(this).find('img');
		if($(this).width() / $(this).height() < img.width() / img.height()) {
			img.height("100%");
			img.width("auto");
		}else{
			img.height("auto");
			img.width("100%");
		}
	});

	$(function() {
		$('.simple_img_gallery').createSimpleImgGallery();
	});

</script>

@endsection
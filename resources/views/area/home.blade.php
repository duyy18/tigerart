<!DOCTYPE html>
<html>
<head>
	<title>Telkom Area</title>
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.gallery.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
</head>
<body>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
					<img src="{{ asset('images/logo.png') }}">
				</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="header-title" style="font-family: 'montez'; font-size: 2.5em"><a href="{{ url('/area') }}">Area Profiling</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ url('/') }}">Home</a></li>
					<li><a href="{{ route('logout') }}" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">Logout</a></li>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>

	<div class="clear"></div>

	<div class="container-fluid main-content-container">

		<div class="col-md-8 col-md-offset-2 main-content-grid">
			<div class="card main-content">
				<div class="card-title main-content-title">
					<strong>Sekilas Provinsi Jambi</strong>
				</div>
				<div class="main-content-content container-fluid">
					<div class="jambi-details col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="op-section-1" class="section">
							<img style="width: 100%; border-radius: 10px" src="{{ asset('images/demography/jambi.png') }}">
							<p style="margin-top: 10px;" class="slogan">"Sepucuk Jambi Sembilan Lurah"</p>
							<div class="clear"></div>
							<div class="demography col-md-3">
								<table style="line-height: 30px">
									<tr>
										<th>Berdiri</th>
										<td>2 Juli 1958</td>
									</tr>
									<tr>
										<th>Ibu Kota</th>
										<td>Kota Jambi</td>
									</tr>
									<tr>
										<th>Luas Wil.</th>
										<td>53.435 KM<sup>2</sup></td>
									</tr>
								</table>
							</div>
							<div class="regencies col-md-9">
								<p style="font-weight: bold;" class="col-md-12 np">Kota/Kabupaten</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kota Jambi</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kab. Muaro Jambi</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kab. Batanghari</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kab. Tanjabtim</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kab. Tanjabbar</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kab. Sarolangun</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kota Sungaipenuh</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kab. Kerinci</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kab. Tebo</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kab. Merangin</p>
								<p class="col-md-4 col-sm-4 col-xs-12 np">Kab. Bungo</p>
							</div>
						</div>

						<div class="clear"></div>

						<div id="section-demo-1" style="margin-bottom: 35px"></div>
						<div class="demography-image">
							<div class="simple_img_gallery col-md-6 col-sm-6 col-xs-12 ">
								<p class="bold">Dataran tertinggi dan terendah</p>
								<a href="{{ asset('images/demography/datar.png') }}"><img src="{{ asset('images/demography/datar.png') }}"></a>
							</div>
							<div class="simple_img_gallery col-md-6 col-sm-6 col-xs-12">
								<p class="bold">Jumlah kecamatan dan desa</p>
								<a href="{{ asset('images/demography/kab.png') }}"><img src="{{ asset('images/demography/kab.png') }}"></a>
							</div>
							<div class="simple_img_gallery col-md-6 col-sm-6 col-xs-12" style="margin-top: 30px">
								<p class="bold">Jumlah penduduk</p>
								<a href="{{ asset('images/demography/sex.png') }}"><img src="{{ asset('images/demography/sex.png') }}"></a>
							</div>
							<div class="simple_img_gallery col-md-6 col-sm-6 col-xs-12" style="margin-top: 30px">
								<p class="bold">Jumlah fasilitas kesehatan</p>
								<a href="{{ asset('images/demography/health.png') }}"><img src="{{ asset('images/demography/health.png') }}"></a>
							</div>
							<div class="simple_img_gallery col-md-6 col-sm-6 col-xs-12" id="section-demo-2" style="padding-top: 35px"> 
								<p class="bold">Hotel dan pariwisata</p>
								<a href="{{ asset('images/demography/hotel.png') }}"><img src="{{ asset('images/demography/hotel.png') }}"></a>
							</div>
						</div>

						<div class="clear"></div>
						<div id="op-section-4" class="box-container col-xs-12 section">
							<div class="box border-yellow">
								<div class="box-title bg-yellow">Komoditi Utama</div>
								<div class="image-container">
									<div class="image-row row-home col-md-3  col-sm-4 col-xs-12">
										<img src="{{ asset('images/ko1.png') }}">
										<div class="middle">
											<div class="text bg-yellow">Kayu</div>
										</div>
									</div>
									<div class="image-row row-home col-md-3  col-sm-4 col-xs-12">
										<img src="{{ asset('images/ko2.png') }}">
										<div class="middle">
											<div class="text bg-yellow">Karet</div>
										</div>
									</div>
									<div class="image-row row-home col-md-3 col-sm-4 col-xs-12">
										<img src="{{ asset('images/ko3.png') }}">
										<div class="middle">
											<div class="text bg-yellow">Kelapa Sawit</div>
										</div>
									</div>
									<div class="image-row row-home col-md-3 col-sm-4 col-xs-12">
										<img src="{{ asset('images/ko4.png') }}">
										<div class="middle">
											<div class="text bg-yellow">Batu Gamping</div>
										</div>
									</div>
									<div class="image-row row-home col-md-3 col-sm-4 col-xs-12">
										<img src="{{ asset('images/ko5.png') }}">
										<div class="middle">
											<div class="text bg-yellow">Batu Granit</div>
										</div>
									</div>
									<div class="image-row row-home col-md-3 col-sm-4 col-xs-12">
										<img src="{{ asset('images/ko6.png') }}">
										<div class="middle">
											<div class="text bg-yellow">Marmer</div>
										</div>
									</div>
									<div class="image-row row-home col-md-3 col-sm-4 col-xs-12">
										<img src="{{ asset('images/ko7.png') }}">
										<div class="middle">
											<div class="text bg-yellow">Pasir Kuarsa</div>
										</div>
									</div>
									<div class="image-row row-home col-md-3 col-sm-4 col-xs-12">
										<img src="{{ asset('images/ko8.png') }}">
										<div class="middle">
											<div class="text bg-yellow">Batu Bara</div>
										</div>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

					</div>

					<!-- <div class="box-container col-xs-12">
						<div class="box border-green">
							<div class="box-title bg-green">Industri</div>
							<div class="image-container">
								<div class="image-row row-home col-md-3 col-sm-4 col-xs-12">
									<img src="{{ asset('images/ind1.png') }}">
									<div class="middle">
										<div class="text bg-green">Crumb Rubber</div>
									</div>
								</div>
								<div class="image-row row-home col-md-3 col-sm-4 col-xs-12">
									<img src="{{ asset('images/in2.png') }}">
									<div class="middle">
										<div class="text bg-green">Penggergajian Kayu</div>
									</div>
								</div>
								<div class="image-row row-home col-md-3 col-sm-4 col-xs-12">
									<img src="{{ asset('images/ind3.png') }}">
									<div class="middle">
										<div class="text bg-green">Virgin Coconut Oil</div>
									</div>
								</div>
								<div class="image-row row-home col-md-3 col-sm-4 col-xs-12">
									<img src="{{ asset('images/ind4.png') }}">
									<div class="middle">
										<div class="text bg-green">PetroCina</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div> -->

				</div>
			</div>

			<div id="section-witel" style="margin-top: 130px"></div>
			<div class="card main-content">
				<div class="card-title main-content-title">
					<strong>Witel Jambi</strong>
				</div>
				<div class="main-content-content container-fluid">
					<div class="jambi-map col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="dvMap" style="width: 100%; height: 600px; border-radius: 10px"></div>
					</div>
					
					<div class="clear"></div>

					<div class="box-container col-xs-12">
						<div class="box border-red">
							<div class="box-title bg-red">STO</div>
							<div class="image-container text-container">
								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np">
									<table>
										<tr>
											<td><a href="{{ route('showWitel') }}">Kantor Witel Jambi</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'JMB') }}">Jambi Centrum (JMB)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'PAP') }}">Pasir Putih (PAP)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'KOA') }}">Kota Baru (KOA)</a></td>
										</tr>
									</table>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np" style="padding-left: 10px">
									<table>
										<tr>
											<td><a href="{{ route('showSTO', 'MND') }}">Mendalo (MND)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'KTL') }}">Kuala Tungkal (KTL)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'PKB') }}">Pangkalan Bulian (PKB)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'MBN') }}">Muara Bulian (MBN)</a></td>
										</tr>
									</table>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np" style="padding-left: 10px">
									<table>
										<tr>
											<td><a href="{{ route('showSTO', 'MAB') }}">Muara Bungo (MAB)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'SRJ') }}">Sarolangun (SRJ)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'BKO') }}">Bangko (BKO)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'SPN') }}">Sungai Penuh (SPN)</a></td>
										</tr>
									</table>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np" style="padding-left: 10px">
									<table>
										<tr>
											<td><a href="{{ route('showSTO', 'RBB') }}">Rimbo Bujang (RBB)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'MRO') }}">Muara Tebo (MRO)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'JMI') }}">Telanaipura (JMI)</a></td>
										</tr>
										<tr>
											<td><a href="{{ route('showSTO', 'TTJ') }}">Tebing Tinggi (TTJ)</a></td>
										</tr>
									</table>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<div class="clear"></div>

					<div class="box-container col-xs-12">
						<div class="box border-blue" id="section-revenue">
							<div class="box-title bg-blue">Revenue</div>
							<div class="image-container text-container" >
								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 np">
									<table>
										<tr>
											<th colspan="2" style="font-size: 1.1em;">Segmen Consumer</th>
										</tr>
										<tr>
											<td style="padding-right: 15px">Ytd {{ $w->revenue_segment_consumer_periode }}</td>
											<td>Rp. {{ $w->revenue_segment_consumer }} M</td>
										</tr>
									</table>
								</div>
								<div class="col-md-5 col-lg-5 col-sm-5 col-xs-12 np" style="left: -10px; position: relative;">
									<table>
										<tr>&nbsp;</tr>
										<tr>
											<th colspan="2">Product</th>
										</tr>
										<tr>
											<td style="padding-right: 15px">- Revenue IndiHome</td>
											<td>Rp. {{ $w->revenue_indihome }} M</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">- Revenue Non IndiHome</td>
											<td>Rp. {{ $w->revenue_non_indihome }} M</td>
										</tr>
									</table>
								</div>
								<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 np" style="padding-left: 10px">
									<table>
										<tr>&nbsp;</tr>
										<tr>
											<th>Service</th>
										</tr>
										<tr>
											<td style="padding-right: 15px">- Legacy</td>
											<td>Rp. {{ $w->revenue_legacy }} M</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">- Connectivity</td>
											<td>Rp. {{ $w->revenue_connectivity }} M</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">- Digital Service</td>
											<td>Rp. {{ $w->revenue_digital_service }} M</td>
										</tr>
									</table>
								</div>

								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np">
									<table>
										<tr>
											<th colspan="2" style="font-size: 1.1em;">Segmen BGES</th>
										</tr>
										<tr>
											<td style="padding-right: 15px">Ytd BGES</td>
											<td>Rp. {{ $w->revenue_ytd_bges }}</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">Ytd ES</td>
											<td>Rp. {{ $w->revenue_ytd_es }}</td>
										</tr>
									</table>
								</div>

								<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 np">
									<table>
										<tr>
											&nbsp;
										</tr>
										<tr>
											<td style="padding-right: 15px">Ytd GS</td>
											<td>Rp. {{ $w->revenue_ytd_gs }}</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">Ytd BS</td>
											<td>Rp. {{ $w->revenue_ytd_bs }}</td>
										</tr>
									</table>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<div class="box-container col-xs-12">
						<div class="box border-yellow">
							<div class="box-title bg-yellow">Line in Service</div>
							<div class="image-container text-container">
								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np">
									<table>
										<tr>&nbsp;</tr>
										<tr>
											<th>LIS {{ $w->lis_year_date }}</th>
										</tr>
										<tr>
											<th>NAL Ytd {{ $w->nal_year_date }}</th>
										</tr>
										<tr>
											<th>LIS {{ $w->lis_month_date }}</th>
										</tr>
									</table>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np" style="padding-left: 10px">
									<table>
										<tr>
											<th>IndiHome</th>
										</tr>
										<tr>
											<td>{{ $w->lis_i_year }} ssl</td>
										</tr>
										<tr>
											<td>{{ $w->nal_i_year }} ssl</td>
										</tr>
										<tr>
											<td>{{ $w->lis_i_month }} ssl</td>
										</tr>
									</table>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np" style="padding-left: 10px">
									<table>
										<tr>
											<th>Non IndiHome</th>
										</tr>
										<tr>
											<td>{{ $w->lis_n_year }} ssl</td>
										</tr>
										<tr>
											<td>{{ $w->nal_n_year }} ssl</td>
										</tr>
										<tr>
											<td>{{ $w->lis_n_month }} ssl</td>
										</tr>
									</table>
								</div>
								<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 np" style="padding-left: 10px">
									<table>
										<tr>
											<th>Total</th>
										</tr>
										<tr>
											<td>{{ $w->lis_t_year }} ssl</td>
										</tr>
										<tr>
											<td>{{ $w->nal_t_year }} ssl</td>
										</tr>
										<tr>
											<td>{{ $w->lis_t_month }} ssl</td>
										</tr>
									</table>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<div class="box-container col-xs-12">
						<div class="box border-green">
							<div class="box-title bg-green">Alat Produksi</div>
							<div class="image-container text-container">
								<div class="col-md-3 np">
									<table>
										<tr>
											<td style="padding-right: 15px">ODC</td>
											<td class="ta-r">{{ $w->apro_odc }}</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">ODP</td>
											<td class="ta-r">{{ $w->apro_odp }}</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">GPON</td>
											<td class="ta-r">{{ $w->apro_gpon }}</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">MSAN</td>
											<td class="ta-r">{{ $w->apro_msan }}</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">DSLAM</td>
											<td class="ta-r">{{ $w->apro_dslam }}</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">R-DSLAM</td>
											<td class="ta-r">{{ $w->apro_rdslam }}</td>
										</tr>
									</table>
								</div>
								<div class="col-md-3 np" style="padding-left: 10px">
									<table>
										<tr>
											<th colspan="2">ODP</th>
										</tr>
										<tr style="color: #000">
											<td style="padding-right: 15px">ODP BLACK</td>
											<td class="ta-r">{{ $w->apro_d_black }}</td>
										</tr>
										<tr style="color: #009e07">
											<td style="padding-right: 15px">ODP GREEN</td>
											<td class="ta-r">{{ $w->apro_d_green }}</td>
										</tr>
										<tr style="color: #d6ad0a">
											<td style="padding-right: 15px">ODP YELLOW</td>
											<td class="ta-r">{{ $w->apro_d_yellow }}</td>
										</tr>
										<tr style="color: #f00">
											<td style="padding-right: 15px">ODP RED</td>
											<td class="ta-r">{{ $w->apro_d_red }}</td>
										</tr>
										<tr style="color: #000">
											<td style="padding-right: 15px">TOTAL</td>
											<td class="ta-r">{{ $w->apro_d_total }}</td>
										</tr>
									</table>
								</div>
								<div class="col-md-3 np" style="padding-left: 10px">
									<table>
										<tr>
											<th class="ta-r">OCC</th>
										</tr>
										<tr style="color: #000">
											<td class="ta-r">{{ $w->apro_c_black }}%</td>
										</tr>
										<tr style="color: #009e07">
											<td class="ta-r">{{ $w->apro_c_green }}%</td>
										</tr>
										<tr style="color: #d6ad0a">
											<td class="ta-r">{{ $w->apro_c_yellow }}%</td>
										</tr>
										<tr style="color: #f00">
											<td class="ta-r">{{ $w->apro_c_red }}%</td>
										</tr>
										<tr style="color: #000">
											<td class="ta-r">{{ $w->apro_c_total }}%</td>
										</tr>
									</table>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<div class="box-container col-xs-12">
						<div class="box border-red" id="section-telkomsel">
							<div class="box-title bg-red">Telkomsel</div>
							<div class="image-container text-container">
								<div class="col-md-3 np">
									<table>
										<tr>
											<th colspan="2">Market Share</th>
										</tr>
										<tr>
											<td style="padding-right: 15px">- Telkomsel</td>
											<td>{{ $w->tsel_tsel }}%</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">- Indosat</td>
											<td>{{ $w->tsel_indosat }}%</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">- XL Axiata</td>
											<td>{{ $w->tsel_xl }}%</td>
										</tr>
										<tr>
											<td style="padding-right: 15px">- 3</td>
											<td>{{ $w->tsel_3 }}%</td>
										</tr>
									</table>
								</div>
								<div class="col-md-6 np" style="padding-left: 10px">
									<table>
										<tr>
											<th colspan="2">Node-B</th>
										</tr>
										<tr>
											<td style="padding-right: 20px">- High (400-700 GB)</td>
											<td>{{ $w->tsel_high }} Node</td>
										</tr>
										<tr>
											<td style="padding-right: 20px">- Medium (200-400 GB)</td>
											<td>{{ $w->tsel_medium }} ssl</td>
										</tr>
										<tr>
											<td style="padding-right: 20px">- Low (<200 GB)</td>
												<td>{{ $w->tsel_low }} Node</td>
											</tr>
										</table>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

						<div class="clear"></div>

						@if(Auth::user()->role == 1)
						<div style="padding-top: 20px; padding-left: 15px">
							<a href="{{ route('areaEdit') }}"><button class="btn btn-primary">Edit</button></a>
						</div>
						@endif
						

					</div>
				</div>



			</div>

		</div>

		<div class="clear"></div>

		<div class="footer">
			Copyright &copy; <strong>Telkom Indonesia Witel Jambi</strong> - 2018
		</div>

		<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBHzEtuVlr36M5MJwT7EtHna7cLFTzDTWs&v=3&amp;sensor=false"></script>
		<script type="text/javascript" src="{{ asset('js/markerwithlabel.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
		<script type="text/javascript"  src="{{ asset('js/jquery.gallery.js') }}"></script>

		<script type="text/javascript">

			var bookmarks = [$('body'), $('#section-demo-1'), $('#section-demo-2'), $('#section-witel'), $('#section-revenue'), $('section-telkomsel')];
			var nowOn = 0;

			var markers = removeEmpty({!! json_encode($sto) !!});

			function removeEmpty(obj) {
				Object.keys(obj).forEach(function(key) {
					(obj[key] && typeof obj[key] === 'object') && removeEmpty(obj[key]) ||
					(obj[key] === '' || obj[key] === null) && delete obj[key]
				});
				return obj;
			};

			function goToSection(direction) {

				console.log(nowOn);

				if(direction == 'up') {
					if(nowOn != 0) nowOn--;
				}else{
					if(bookmarks.length - 1 != nowOn) nowOn++;	
				} 

				console.log(nowOn);

				$('html, body').animate({ scrollTop: bookmarks[nowOn].offset().top }, 'slow');

			}


			window.onload = function () {

				$(document).keydown(function(e) {
					switch(e.which) {

						case 33:
						goToSection('up');
						break;

						case 34:
						goToSection('down');
						break;

						default:
						return;
					}

					e.preventDefault();
				});

				var mapOptions = {
					center: new google.maps.LatLng('-1.75754', '102.69160'),
					zoom: 8,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var infoWindow = new google.maps.InfoWindow();
				var latlngbounds = new google.maps.LatLngBounds();
				var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
				var i = 0;
				var interval = setInterval(function () {
					var data = markers[i];
					var myLatlng = new google.maps.LatLng(data.lat, data.lng);
					icon = "/images/marker/" + 'sto2' + ".png";
					var marker = new MarkerWithLabel({
						position: myLatlng,
						map: map,
						id: data.id,
						zIndex: 40,
						draggable: true,
						animation: google.maps.Animation.DROP,
						labelContent: data.sto_desc,
						labelAnchor: new google.maps.Point(22, -5),
						labelClass: "labels",
						labelStyle: {opacity: 0.9},
						icon: new google.maps.MarkerImage(icon),
						category: data.category
					});

					if(data.lat == null || data.lat == "null" || data.lat == "") {
						marker.setMap(null);
					}

					(function (marker, data) {
						google.maps.event.addListener(marker, "click", function (e) {
							if(marker.id != "WTL") {
								window.location='/area/sto/' + marker.id;
							}else{
								window.location='/area/witel';
							}
						});
					})(marker, data);
					latlngbounds.extend(marker.position);

					i++;
					if (i == markers.length) {
						clearInterval(interval);
						var bounds = new google.maps.LatLngBounds();
						map.setCenter(latlngbounds.getCenter());
						map.fitBounds(latlngbounds);
					}
				}, 80);

				$('.image-row').each(function() {
					var img = $(this).find('img');
					if($(this).width() / $(this).height() < img.width() / img.height()) {
						img.height("100%");
						img.width("auto");
					}else{
						img.height("auto");
						img.width("100%");
					}
				});

				$(function() {
					$('.simple_img_gallery').createSimpleImgGallery();
				});

			}
		</script>

	</body>
	</html>
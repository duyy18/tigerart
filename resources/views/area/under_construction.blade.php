@extends('layouts.area_layout')


@section('main-content')

<div class="col-md-9 main-content-grid">
	<div class="card main-content">
		<div class="card-title main-content-title">
			Under Construction
		</div>
		<div class="main-content-content container-fluid">
			<div class="jambi-map col-md-6 col-md-offset-3" style="margin-top: 40px;">
				<img style="width: 70%; display: block; margin: 0 auto" src="{{ asset('images/uc.png') }}">
				<h2 style="text-align: center">Under<br />Construction</h2>
			</div>
			
		</div>
	</div>
</div>

@endsection
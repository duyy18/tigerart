@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('style')
	<style type="text/css">
		table {
			box-shadow:2px 2px 2px grey;
		}
		table, tr, td {
			border: 2px solid grey;
			text-align:left !important;
		}
		th, tr, td {
			text-align: center;
		}
		th {
			background: #b6bab2;
		}
		thead tr {
			border-bottom: 3px solid grey
		}
	</style>
@endsection

@section('content')

<div class="container">
	<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="with-border">
							<div style="padding:30px;">
								<div class="row" st>
									<div class="col-md-12">
										<form action="{{Route('apresiasi_progress')}}" method="GET">
											<label>Tampilkan dari: </label>
											@if(!isset($_GET['tgl_start']))
											<input type="date" name="tgl_start" id="tgl_start" value="{{date('Y-m-d',strtotime('-7 day',strtotime(date('Y-m-d'))))}}">
											@else
											<input type="date" name="tgl_start" id="tgl_start" value="{{$_GET['tgl_start']}}">
											@endif
											s.d.
											@if(!isset($_GET['tgl_start']))
											<input type="date" name="tgl_end" id="tgl_end" value="{{date('Y-m-d')}}"> 
											@else
											<input type="date" name="tgl_end" id="tgl_end" value="{{$_GET['tgl_end']}}"> 
											@endif
											<button class="btn btn-sm btn-primary">Tampilkan</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<!-- new -->
</section>

<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="row">
			<div class="col-md-12">
			</div>
		</div>
	</div>
	<div class="col-md-12" style="padding-left: 0">
			<div class="row">
				<div class="col-md-6">
					<div class="box" style="min-height: 370px">
						<div class="with-border">
							<div style="padding:30px;">
								<h3>Apresiasi</h3>
								<hr>
								<table class="table" id="tabel_target">
									<thead>
										<tr>
											<th>Unit/Sektor</th>
											<th>Jumlah SC</th>
											<th>Rp</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td>JMB</td>
											<td>
												{{number_format(count($apresiasi['sc_jmb']))}}
											</td>
											<td>
												Rp{{number_format(count($apresiasi['sc_jmb'])*10000)}}
											</td>
											
										</tr>

										<tr>
											<td>KOA-PAP</td>
											<td>
												{{number_format(count($apresiasi['sc_koa-pap']))}}
											</td>
											<td>
												Rp{{number_format(count($apresiasi['sc_koa-pap'])*10000)}}
											</td>
										</tr>

										<tr>
											<td>JMI</td>
											<td>
												{{number_format(count($apresiasi['sc_jmi']))}}
											</td>
											<td>
												Rp{{number_format(count($apresiasi['sc_jmi'])*10000)}}
											</td>
										</tr>

										<tr>
											<td>MND-OUTER</td>
											<td>
												{{number_format(count($apresiasi['sc_mnd-outer']))}}
											</td>
											<td>
												Rp{{number_format(count($apresiasi['sc_mnd-outer'])*10000)}}
											</td>
										</tr>

										<tr>
											<td>MAB</td>
											<td>
												{{number_format(count($apresiasi['sc_mab']))}}
											</td>
											<td>
												Rp{{number_format(count($apresiasi['sc_mab'])*10000)}}
											</td>
										</tr>

										<tr>
											<td>SDI - Tim Push Tenoss</td>
											<td>
												{{number_format(count($apresiasi['sc_sdi_push']))}}
											</td>
											<td>
												Rp{{number_format(count($apresiasi['sc_sdi_push'])*10000)}}
											</td>
										</tr>

										<tr>
											<td>SDI - Tim Go Live</td>
											<td>
												{{number_format(count($apresiasi['sc_sdi_go_live']))}}
											</td>
											<td>
												Rp{{number_format(count($apresiasi['sc_sdi_go_live'])*10000)}}
											</td>
										</tr>

										<tr>
											<td>CS</td>
											<td>
												{{number_format(count($apresiasi['sc_cs']))}}
											</td>
											<td>
												Rp{{number_format(count($apresiasi['sc_cs'])*10000)}}
											</td>
										</tr>

										<tr>
											<td>MTC</td>
											<td>
												{{number_format(count($apresiasi['sc_mtc']))}}
											</td>
											<td>
												Rp{{number_format(count($apresiasi['sc_mtc'])*10000)}}
											</td>
										</tr>

										<tr>
											<td>CONS</td>
											<td>
												{{number_format(count($apresiasi['sc_cons']))}}
											</td>
											<td>
												Rp{{number_format(count($apresiasi['sc_cons'])*10000)}}
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="box" style="min-height: 370px">
						<div class="with-border">
							<div style="padding:30px;">
								<h3>Denda</h3>
								<hr>
								<table class="table" id="tabel_target">
									<thead>
										<tr>
											<th>Unit/Sektor</th>
											<th>Jumlah SC</th>
											<th>Jumlah Hari Keterlambatan</th>
											<th>Rp</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td>JMB</td>
											<td>{{number_format($denda['JMB']['sc'])}}</td>
											<td>{{number_format($denda['JMB']['day'])}}</td>
											<td>Rp{{number_format($denda['JMB']['sc']*$denda['JMB']['day']*5000)}},-</td>
										</tr>

										<tr>
											<td>KOA-PAP</td>
											<td>{{number_format($denda['KOA-PAP']['sc'])}}</td>
											<td>{{number_format($denda['KOA-PAP']['day'])}}</td>
											<td>Rp{{number_format($denda['KOA-PAP']['sc']*$denda['KOA-PAP']['day']*5000)}},-</td>
										</tr>

										<tr>
											<td>JMI</td>
											<td>{{number_format($denda['JMI']['sc'])}}</td>
											<td>{{number_format($denda['JMI']['day'])}}</td>
											<td>Rp{{number_format($denda['JMI']['sc']*$denda['JMI']['day']*5000)}},-</td>
										</tr>

										<tr>
											<td>MND-OUTER</td>
											<td>{{number_format($denda['MND-OUTER']['sc'])}}</td>
											<td>{{number_format($denda['MND-OUTER']['day'])}}</td>
											<td>Rp{{number_format($denda['MND-OUTER']['sc']*$denda['MND-OUTER']['day']*5000)}},-</td>
										</tr>

										<tr>
											<td>MAB</td>
											<td>{{number_format($denda['MAB']['sc'])}}</td>
											<td>{{number_format($denda['MAB']['day'])}}</td>
											<td>Rp{{number_format($denda['MAB']['sc']*$denda['MAB']['day']*5000)}},-</td>
										</tr>

										<tr>
											<td>SDI - Tim Push Tenoss</td>
											<td>{{number_format($denda['SDI_PUSH']['sc'])}}</td>
											<td>{{number_format($denda['SDI_PUSH']['day'])}}</td>
											<td>Rp{{number_format($denda['SDI_PUSH']['sc']*$denda['SDI_PUSH']['day']*5000)}},-</td>
										</tr>

										<tr>
											<td>SDI - Tim Go Live</td>
											<td>{{number_format($denda['SDI_GO_LIVE']['sc'])}}</td>
											<td>{{number_format($denda['SDI_GO_LIVE']['day'])}}</td>
											<td>Rp{{number_format($denda['SDI_GO_LIVE']['sc']*$denda['SDI_GO_LIVE']['day']*5000)}},-</td>
										</tr>

										<tr>
											<td>CS</td>
											<td>{{number_format($denda['CS']['sc'])}}</td>
											<td>{{number_format($denda['CS']['day'])}}</td>
											<td>Rp{{number_format($denda['CS']['sc']*$denda['CS']['day']*5000)}},-</td>
										</tr>

										<tr>
											<td>MTC</td>
											<td>{{number_format($denda['MTC']['sc'])}}</td>
											<td>{{number_format($denda['MTC']['day'])}}</td>
											<td>Rp{{number_format($denda['MTC']['sc']*$denda['MTC']['day']*5000)}},-</td>
										</tr>

										<tr>
											<td>CONS</td>
											<td>{{number_format($denda['CONS']['sc'])}}</td>
											<td>{{number_format($denda['CONS']['day'])}}</td>
											<td>Rp{{number_format($denda['CONS']['sc']*$denda['CONS']['day']*5000)}},-</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<!-- new -->
</section>

@endsection
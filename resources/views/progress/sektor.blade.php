@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section("style")
<style type="text/css">
	th {
		text-align:center;
	}
	td {
		text-align:center;
	}
</style>
@endsection

@section('content')

<div class="container">
	<div class="col-md-12" style="padding-left: 0">
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="with-border">
							<div style="padding:30px;">
								<form action="{{route('do_filter_progress_kendala')}}" method="POST">
									<!-- <label>Filter Kendala: </label>
									<select class="form_control" name="filter">
										@if($current_filter_kendala=="ALL")
										<option value="all" selected>Seluruh Kendala</option>
										@else
										<option value="all">Seluruh Kendala</option>
										@endif
										@foreach($filter_kendala as $fil)
											@if($fil->kendala == $current_filter_kendala)
											<option value="{{$fil->kendala}}" selected>{{$fil->kendala}}</option>
											@else
											<option value="{{$fil->kendala}}">{{$fil->kendala}}</option>
											@endif
										@endforeach
									</select>
									<button class="btn btn-sm btn-primary">Filter</button> -->
								</form>
								<div style="margin:10px"></div>
								<form action="{{route('do_filter_progress')}}" method="POST">
									<label>Filter Status: </label>
									<select class="form_control" name="filter">
										@if($current_filter=="ALL")
										<option value="all" selected>Seluruh Status</option>
										@else
										<option value="all">Seluruh Status</option>
										@endif
										@foreach($filter as $fil)
											@if($fil->status == $current_filter)
											<option value="{{$fil->status}}" selected>{{$fil->status}}</option>
											@else
											<option value="{{$fil->status}}">{{$fil->status}}</option>
											@endif
										@endforeach
									</select>
									<button class="btn btn-sm btn-primary">Filter</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="box" style="min-height: 370px">
						<div class="with-border">
							<div style="padding:30px;text-align:right;">
								<table class="table" id="tabel_target" style="width:100%">
									<thead>
										<tr>
											<th>No</th>
											<th>Sektor</th>
											<th>STO</th>
											<th>Status</th>
											<th>Kendala</th>
											<th>SC</th>
											<th>Nama Pelanggan</th>
											<th>No HP</th>
											@if(Auth::user()->group!="SDI")
											<th>Tanggal HS</th>
											@endif
											@if(Auth::user()->group=="SDI")
											<th>Tgl Tutup Konstruksi</th>
											@endif
											<th>Progress</th>
											<th>Kekurangan</th>
											<th>#</th>
										</tr>
									</thead>
									<tbody>
										@foreach($data as $index => $val)
										<tr>
											<td>{{$index+=1}}</td>
											<td>{{$val->sektor}}</td>
											<td>{{$val->sto}}</td>
											<td>{{$val->status}}</td>
											<td>{{$val->kendala}}</td>
											<td>{{$val->sc}}</td>
											<td style="text-align:left;">{{$val->nama_pelanggan}}</td>
											<td>{{$val->no_hp}}</td>
											@if(Auth::user()->group!="SDI")
											<td>{{$val->tgl_hs}}</td>
											@endif
											@if(Auth::user()->group=="SDI")
											<td>
												@if($val->tgl_close_sektor!=null)
												{{date('Y-m-d',strtotime($val->tgl_close_sektor))}}
												@else
												-
												@endif
											</td>
											@endif
											<!-- Block Status -->
											@if(
												Auth::user()->group!="SDI" &&
												Auth::user()->group!="MTC" &&
												Auth::user()->group!="CONS"
											)
											<td>
												@if($val->kendala=="UNSPEC")
													@if($val->receive_unspec)
													In Progress
													@else
													No Progress
													@endif
												@elseif(!$val->received_level_one)
												No Progress
												@elseif($val->received_level_one && $val->status=="SURVEI-PT2/PT3")
												In Progress
												@elseif($val->kendala=="TANAM TIANG" || $val->kendala=="UNSPEC")
												In Progress
												@elseif($val->is_pending==1)
												Revisi
												@elseif($val->odp_name!=null && $val->foto_redaman!=null && $val->mancore!=null)
												Data
												@elseif($val->odp_name!=null && $val->foto_redaman!=null)
												Konstruksi
												@else
												Survei
												@endif
											</td>
											@endif
											<!-- Block Status -->
											<!-- Block Kekurangan -->
											@if(
												Auth::user()->group!="SDI" &&
												Auth::user()->group!="MTC" &&
												Auth::user()->group!="CONS"
											)
											<td>
												@if($val->kendala=="UNSPEC")
													@if(!$val->receive_unspec)
													-
													@elseif($val->odp_name==null)
														@if($val->foto_redaman_unspec==null)
															@if($val->foto_after==null)
																@if($val->kolom_perbaikan==null)
																Nama ODP, Foto Redaman, Foto Redaman Pasca Perbaikan, dan Kolom Perbaikan
																@else
																Nama ODP, Foto Redaman, dan Foto Redaman Pasca Perbaikan
																@endif
															@else
																@if($val->kolom_perbaikan==null)
																Nama ODP, Foto Redaman, dan Kolom Perbaikan
																@else
																Nama ODP, dan Foto Redaman
																@endif
															@endif
														@else
															@if($val->foto_after==null)
																@if($val->kolom_perbaikan==null)
																Nama ODP, Foto Redaman Pasca Perbaikan, dan Kolom Perbaikan
																@else
																Nama ODP, Foto Redaman, dan Foto Redaman Pasca Perbaikan
																@endif
															@else
																@if($val->kolom_perbaikan==null)
																Nama ODP, dan Kolom Perbaikan
																@else
																Nama ODP
																@endif
															@endif
														@endif
													@else
														@if($val->foto_redaman_unspec==null)
															@if($val->foto_after==null)
																@if($val->kolom_perbaikan==null)
																Foto Redaman, Foto Redaman Pasca Perbaikan, dan Kolom Perbaikan
																@else
																Foto Redaman, dan Foto Redaman Pasca Perbaikan
																@endif
															@else
																@if($val->kolom_perbaikan==null)
																Foto Redaman, dan Kolom Perbaikan
																@else
																Foto Redaman
																@endif
															@endif
														@else
															@if($val->foto_after==null)
																@if($val->kolom_perbaikan==null)
																Foto Redaman Pasca Perbaikan, dan Kolom Perbaikan
																@else
																Foto Redaman, dan Foto Redaman Pasca Perbaikan
																@endif
															@else
																@if($val->kolom_perbaikan==null)
																dan Kolom Perbaikan
																@else
																-
																@endif
															@endif
														@endif
													@endif
												@elseif(!$val->received_level_one)
												-
												@elseif($val->is_pending==1)
												{{$val->pending_reason}}
												@elseif($val->status=="SURVEI-PT2/PT3")
												-
												@elseif($val->kendala=="TANAM TIANG")
												Foto Tiang
												@elseif(
													!$FungsiGlobal::containString($val->status,"PT2") ||
													$FungsiGlobal::containString($val->status,"SURVEI") ||
													!$FungsiGlobal::stringOnArray($val->kendala,[
														"ODP PENUH","TARIKAN JAUH"
													])
												)
												-
												@elseif($val->odp_name==null)
													@if($val->foto_redaman==null)
														@if($val->kml==null)
															@if($val->mancore==null)
																Nama ODP, Foto Redaman, File KML, dan Mancore
															@else
																Nama ODP, Foto Redaman, dan File KML
															@endif
														@else
															@if($val->mancore==null)
																Nama ODP, Foto Redaman, dan Mancore
															@else
																Nama ODP, dan Foto Redaman
															@endif
														@endif
													@else
														@if($val->kml==null)
															@if($val->mancore==null)
																Nama ODP, File KML, dan Mancore
															@else
																Nama ODP, dan File KML
															@endif
														@else
															@if($val->mancore==null)
																Nama ODP, dan Mancore
															@else
																Nama ODP
															@endif
														@endif
													@endif
												@else
													@if($val->foto_redaman==null)
														@if($val->kml==null)
															@if($val->mancore==null)
																Foto Redaman, File KML, dan Mancore
															@else
																Foto Redaman, dan File KML
															@endif
														@else
															@if($val->mancore==null)
																Foto Redaman, dan Mancore
															@else
																Foto Redaman
															@endif
														@endif
													@else
														@if($val->kml==null)
															@if($val->mancore==null)
																File KML, dan Mancore
															@else
																File KML
															@endif
														@else
															@if($val->mancore==null)
																Mancore
															@else
																-
															@endif
														@endif
													@endif
												@endif
											</td>
											@endif
											<!-- Block Kekurangan -->


											<!-- MTC -->
											<!-- Block Status -->
											@if(Auth::user()->group=="MTC")
											<td>
												@if($val->kendala=="UNSPEC")
													@if($val->receive_unspec)
													In Progress
													@else
													No Progress
													@endif
												@elseif($val->odp_name!=null && $val->foto_redaman)
												Closed
												@else
												In Progress
												@endif
											</td>
											@endif
											<!-- Block Status -->
											<!-- Block Kekurangan -->
											@if(
												Auth::user()->group=="MTC" 
											)
											<td>
												@if(
													!$val->receive_unspec &&
													$val->status == "UNSPEC"
												)
												-
												@elseif($val->odp_name==null)
													@if($val->foto_redaman_unspec==null)
														@if($val->foto_after==null)
															@if($val->kolom_perbaikan==null)
															Nama ODP, Foto Redaman, Foto Redaman Pasca Perbaikan, dan Kolom Perbaikan
															@else
															Nama ODP, Foto Redaman, dan Foto Redaman Pasca Perbaikan
															@endif
														@else
															@if($val->kolom_perbaikan==null)
															Nama ODP, Foto Redaman, dan Kolom Perbaikan
															@else
															Nama ODP, dan Foto Redaman
															@endif
														@endif
													@else
														@if($val->foto_after==null)
															@if($val->kolom_perbaikan==null)
															Nama ODP, Foto Redaman Pasca Perbaikan, dan Kolom Perbaikan
															@else
															Nama ODP, Foto Redaman, dan Foto Redaman Pasca Perbaikan
															@endif
														@else
															@if($val->kolom_perbaikan==null)
															Nama ODP, dan Kolom Perbaikan
															@else
															Nama ODP
															@endif
														@endif
													@endif
												@else
													@if($val->foto_redaman_unspec==null)
														@if($val->foto_after==null)
															@if($val->kolom_perbaikan==null)
															Foto Redaman, Foto Redaman Pasca Perbaikan, dan Kolom Perbaikan
															@else
															Foto Redaman, dan Foto Redaman Pasca Perbaikan
															@endif
														@else
															@if($val->kolom_perbaikan==null)
															Foto Redaman, dan Kolom Perbaikan
															@else
															Foto Redaman
															@endif
														@endif
													@else
														@if($val->foto_after==null)
															@if($val->kolom_perbaikan==null)
															Foto Redaman Pasca Perbaikan, dan Kolom Perbaikan
															@else
															Foto Redaman, dan Foto Redaman Pasca Perbaikan
															@endif
														@else
															@if($val->kolom_perbaikan==null)
															dan Kolom Perbaikan
															@else
															-
															@endif
														@endif
													@endif
												@endif
											</td>
											@endif
											<!-- Block Kekurangan -->

											<!-- CONS -->
											<!-- Block Status -->
											@if(Auth::user()->group=="CONS")
											<td>
												@if($val->odp_name!=null && $val->foto_redaman)
												Closed
												@else
												In Progress
												@endif
											</td>
											@endif
											<!-- Block Status -->
											<!-- Block Kekurangan -->
											@if(
												Auth::user()->group=="CONS" 
											)
											<td>
												@if($val->odp_name==null && $val->foto_redaman==null)
												Nama ODP dan Foto redaman
												@elseif ($val->odp_name==null && $val->foto_redaman!=null)
												Nama ODP
												@elseif ($val->odp_name!=null && $val->foto_redaman==null)
												Foto Redaman
												@else
												-
												@endif
											</td>
											@endif
											<!-- Block Kekurangan -->

											<!-- SDI -->
											<!-- Block Status -->
											@if(Auth::user()->group=="SDI")
											<td>
												@if($val->need_validate_cs)
												Menunggu Validasi CS
												@elseif(!$val->received_level_one)
												No Progress
												@elseif($val->foto_push!=null && $val->foto_connectivity!=null)
												Selesai
												@elseif($val->foto_push!=null && $val->foto_connectivity==null)
												Progress Go Live
												@else
												Push Tenoss
												@endif
											</td>
											@endif
											<!-- Block Status -->
											<!-- Block Kekurangan -->
											@if(
												Auth::user()->group=="SDI" 
											)
											<td>
												@if(!$val->received_level_one || $val->need_validate_cs)
												-
												@elseif($val->message_rejected!=null)
												{{$val->message_rejected}}
												@elseif($val->foto_push==null)
												Foto Push
												@elseif($val->foto_connectivity==null)
												Foto Connectivity
												@else
												-
												@endif
											</td>
											@endif
											<!-- Block Kekurangan -->

											<td>
												@if(
													Auth::user()->group=="SDI" &&
													$val->need_validate_cs
												)
												<a class="btn btn-sm btn-success" href="#" disabled>Get Order</a>
												@elseif($val->kendala=="UNSPEC" && !$val->receive_unspec)
												<a class="btn btn-sm btn-success" href="{{Route('action_decision')}}/{{$val->sc}}">Action</a>
												@elseif(!$val->received_level_one)
												<a class="btn btn-sm btn-success" href="{{Route('receive_sc')}}/{{$val->sc}}/1">Get Order</a>
												@else
												<a class="btn btn-sm btn-primary" href="{{Route('fill_progress')}}/{{$val->sc}}">Edit</a>
												@endif
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								<!-- <div style="text-align:left;">
									<a class="btn btn-md btn-primary" href="{{Route('riwayat_progress')}}/{{Auth::user()->group}}" target="_BLANK">Riwayat</a>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<!-- new -->
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$("#tabel_target").DataTable({
			  "scrollX": true
		});
	})
</script>
@endsection
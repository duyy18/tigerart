@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12" style="text-align:center;">
				<h3 style="font-weight:bold;"><u>~{{$data->sc}}~</u></h3>
			</div>
			<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">
				<div class="form-group col-md-12">
					<label>Foto Push Tenoss</label>	
					<div style="clear:both;"></div>
					@if($data->foto_push==null)
					<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:240px;height:320px" id="foto_push">
					@else
					<img src="{{asset('foto')}}/{{$data->foto_push}}" alt="Foto Push Tenoss" style="border:1px solid grey;width:240px;height:320px" id="foto_push">
					@endif
					<div style="clear:both;margin:10px"></div>
					<form id="formKu" method="post" style="display:none" action="#">
						<input type="file" accept=".jpg, .jpeg" id="fileKu" onchange="upload()">
					</form>
				</div>
				<div class="form-group col-md-12">
					<label>Foto Connectivity Dashboard Fulfillment</label>	
					<div style="clear:both;"></div>
					@if($data->foto_connectivity==null)
					<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:240px;height:320px" id="foto_connectivity">
					@else
					<img src="{{asset('foto')}}/{{$data->foto_connectivity}}" alt="Foto Connectivity" style="border:1px solid grey;width:240px;height:320px" id="foto_connectivity">
					@endif
					<div style="clear:both;margin:10px"></div>
					<form id="formKu2" method="post" style="display:none" action="#">
						<input type="file" accept=".jpg, .jpeg" id="fileKu2" onchange="upload2()">
					</form>
				</div>
				<button class="btn btn-sm btn-primary" style="width: 36%; margin: 40px 32%;" onclick="dosubmit()">Submit</button>
			</div>
		</div>
	</div>
</div>
@endsection
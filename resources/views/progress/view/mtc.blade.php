@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12" style="text-align:center;">
				<h3 style="font-weight:bold;"><u>~{{$data->sc}}~</u></h3>
			</div>
			<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">
				<div class="form-group col-md-12">
					<label for="odp">Nama ODP</label>	
					<input class="form-control" type="text" name="odp_name" placeholder="Contoh: ODP-JMB-FA/01" required value="{{$data->odp_name}}" id="odp_name" disabled>
				</div>
				<div class="form-group col-md-12">
					<label>Foto Redaman ODP</label>	
					<div style="clear:both;"></div>
					@if($data->foto_redaman==null)
					<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:240px;height:320px" id="foto_redaman">
					@else
					<img src="{{asset('foto')}}/{{$data->foto_redaman}}" alt="Foto Redaman" style="border:1px solid grey;width:240px;height:320px" id="foto_redaman">
					@endif
					<div style="clear:both;margin:10px"></div>
					<form id="formKu" method="post" style="display:none" action="#">
						<input type="file" accept=".jpg" id="fileKu" onchange="upload()">
					</form>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection
@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section("style")
<style type="text/css">
.center {
	text-align:center;
}
.image-preview {
  width: 240px;
  height: 320px;
  background-color: #ffffff;
  color: #ecf0f1;
  border:2px solid black;
  box-shadow: 2px 2px 2px grey;
  margin:10px;
  display:inline-block;
}
.image-preview input {
  line-height: 200px;
  font-size: 200px;
  position: absolute;
  opacity: 0;
  z-index: 10;
}
.image-preview label {
  position: absolute;
  z-index: 5;
  opacity: 0.8;
  cursor: pointer;
  background-color: #bdc3c7;
  width: 200px;
  height: 50px;
  font-size: 20px;
  line-height: 50px;
  text-transform: uppercase;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  text-align: center;
}
.image-preview img {
	width:100%;
	height:100%;
}
</style>
@endsection

@section('content')
<div class="container" style="margin:auto">
	<div class="row">
		<div class="main-content-content container-fluid box" style="padding-bottom:20px">
			<form action="{{Route('upload_tiang')}}/{{$data->sc}}" method="POST" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="col-md-12" style="text-align:center;margin-bottom:20px">
				<h3 style="font-weight:bold;"><u>~{{$data->sc}}~</u></h3>
			</div>
			@for($i=0; $i<count($foto); $i++)
			<div class="col-md-3">
				<div class="image-preview foto-tiang-{{$i}}">
					<img src="{{asset('foto/'.$foto[$i])}}">
				</div>
			</div>
			@endfor
			</form>
		</div>
	</div>
</div>
@endsection

@section("script")
<script type="text/javascript" src="{{asset('js/jquery.uploadPreview.js')}}"></script>
@endsection
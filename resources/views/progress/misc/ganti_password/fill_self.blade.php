@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12" style="text-align:center;">
				<h3 style="font-weight:bold;"><u>~Ganti Password~</u></h3>
			</div>
			<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">
				<div class="form-group col-md-12">
					<label for="odp">Pengguna</label>	
					<input class="form-control" type="text" value="{{$data->call_as}}" disabled="disabled">
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Sandi Saat ini</label>	
					<input class="form-control" type="password" id="password_current">
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Sandi Baru</label>	
					<input class="form-control" type="password" id="password_new">
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Ketik Ulang Sandi Baru</label>	
					<input class="form-control" type="password" id="password_confirm">
				</div>
				<button class="btn btn-sm btn-primary" style="width: 36%; margin: 40px 32%;" onclick="dosubmit()">Ganti</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function dosubmit() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('action_change_password')}}/{{Crypt::encrypt($data->id)}}",
			method: "POST",
			data  : {
				"target": "self",
				"password_current" : $("#password_current").val(),
				"password_new" : $("#password_new").val(),
				"password_confirm" : $("#password_confirm").val(),
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: 'Sandi berhasil diganti!',
					});
					setTimeout(function(){
						window.open("{{Route('ganti_password')}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Error!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
</script>
@endsection
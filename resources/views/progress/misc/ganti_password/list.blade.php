@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')

<div class="container">
	<div class="col-md-12" style="padding-left: 0">
			<div class="row">
				<!-- <div class="col-md-12">
					<div class="box">
						<div class="with-border">
							<div style="padding:30px;">
								<div style="margin:10px"></div>
							</div>
						</div>
					</div>
				</div> -->
				<div class="col-md-12">
					<div class="box" style="min-height: 370px">
						<div class="with-border">
							<div style="padding:30px;">
								<table class="table" id="tabel_target" style="width:100%">
									<thead>
										<tr>
											<th>No</th>
											<th>User</th>
											<th>User Name</th>
											<th>#</th>
										</tr>
									</thead>
									<tbody>
										@foreach($data as $index => $val)
										<tr>
											<td>{{$index+=1}}</td>
											<td>{{$val->call_as}}</td>
											<td>{{$val->username}}</td>
											<td>
												<a href="{{Route('fill_password')}}/{{Crypt::encrypt($val->id)}}" class="btn btn-sm btn-primary">Ganti Password</a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								<!-- <div style="text-align:left;">
									<a class="btn btn-md btn-primary" href="{{Route('riwayat_progress')}}/{{Auth::user()->group}}" target="_BLANK">Riwayat</a>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<!-- new -->
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$("#tabel_target").DataTable({
			  "scrollX": true
		});
	})
</script>
@endsection
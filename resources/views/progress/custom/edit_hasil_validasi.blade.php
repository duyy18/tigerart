@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section("style")
<style type="text/css">
	th {
		text-align:center;
	}
	td {
		text-align:center;
	}
</style>
@endsection

@section('content')

<div class="container">
	<div class="col-md-12" style="padding-left: 0">
			<div class="row">
				@if(Auth::user()->group=="CS")
				<div class="col-md-12">
					<div class="box">
						<div class="with-border">
							<div style="padding:30px">
								<a href="{{Route('edit_data_no_sc')}}" class="btn btn-sm btn-success">Edit Lihat yang tidak memiliki SC</a>
							</div>
						</div>
					</div>
				</div>
				@endif
				<div class="col-md-12">
					<div class="box" style="min-height: 370px">
						<div class="with-border">
							<div style="padding:30px;text-align:right;">
								<table class="table" id="tabel_target" style="width:100%">
									<thead>
										<tr>
											<th>No</th>
											<th>Sektor</th>
											<th>STO</th>
											<th>Status</th>
											<th>Kendala</th>
											<th>SC</th>
											<th>Nama Pelanggan</th>
											<th>No HP</th>
											<th>Tgl HS</th>
											<th>#</th>
										</tr>
									</thead>
									<tbody>
										@foreach($data as $index => $val)
										<tr>
											<td>{{$index+=1}}</td>
											<td>{{$val->sektor}}</td>
											<td>{{$val->sto}}</td>
											<td>{{$val->status}}</td>
											<td>{{$val->kendala}}</td>
											<td>{{$val->sc}}</td>
											<td style="text-align:left;">{{$val->nama_pelanggan}}</td>
											<td>{{$val->no_hp}}</td>
											<td>{{$val->tgl_hs}}</td>
											<td>
												<a class="btn btn-sm btn-success" href="{{Route('fill_validate_force_close')}}/{{$val->sc}}">Validasi</a>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								<!-- <div style="text-align:left;">
									<a class="btn btn-md btn-primary" href="{{Route('riwayat_progress')}}/{{Auth::user()->group}}" target="_BLANK">Riwayat</a>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<!-- new -->
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$("#tabel_target").DataTable({
			  "scrollX": true
		});
	})
</script>
@endsection
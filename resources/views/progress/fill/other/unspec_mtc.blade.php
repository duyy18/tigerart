@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12" style="text-align:center;">
				<h3 style="font-weight:bold;"><u>~{{$data->sc}}~</u></h3>
			</div>
			<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">
				<div class="form-group col-md-12">
					<label for="odp">Nama ODP</label>	
					<input class="form-control" type="text" name="odp_name" placeholder="Contoh: ODP-JMB-FA/01" required value="{{$data->odp_name}}" id="odp_name">
				</div>
				<div class="form-group col-md-6">
					<label>Foto Redaman ODP</label>	
					<div style="clear:both;"></div>
					@if($data->foto_redaman==null)
					<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:240px;height:320px" id="foto_redaman">
					@else
					<img src="{{asset('foto')}}/{{$data->foto_redaman}}" alt="Foto Redaman" style="border:1px solid grey;width:240px;height:320px" id="foto_redaman">
					@endif
					<div style="clear:both;margin:10px"></div>
					<button class="btn btn-primary" onclick="pilihFile()"><i class="fa fa-upload"></i> Upload Foto</button>
					<button class="btn btn-danger" onclick="hapus()"><i class="fa fa-trash"></i> Hapus Foto</button>
					<form id="formKu" method="post" style="display:none" action="#">
						<input type="file" accept=".jpg,.jpeg" id="fileKu" onchange="upload()">
					</form>
				</div>
				<div class="form-group col-md-6">
					<label>Foto After</label>	
					<div style="clear:both;"></div>
					@if($data->foto_after==null)
					<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:240px;height:320px" id="foto_after">
					@else
					<img src="{{asset('foto')}}/{{$data->foto_after}}" alt="Foto After aintenance" style="border:1px solid grey;width:240px;height:320px" id="foto_after">
					@endif
					<div style="clear:both;margin:10px"></div>
					<button class="btn btn-primary" onclick="pilihFile2()"><i class="fa fa-upload"></i> Upload Foto</button>
					<button class="btn btn-danger" onclick="hapus2()"><i class="fa fa-trash"></i> Hapus Foto</button>
					<form id="formKu2" method="post" style="display:none" action="#">
						<input type="file" accept=".jpg,.jpeg" id="fileKu2" onchange="upload2()">
					</form>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Kolom Perbaikan</label>	
					<textarea class="form-control" name="kolom_perbaikan" id="kolom_perbaikan"></textarea>
				</div>
				<button class="btn btn-sm btn-primary" style="width: 36%; margin: 40px 32%;" onclick="dosubmit()">Submit</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function dosubmit() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('update_progress')}}/mtc/{{$data->sc}}",
			method: "POST",
			data  : {
				"odp_name" : $("#odp_name").val(),
				"kolom_perbaikan" : $("#kolom_perbaikan").val(),
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: 'Data berhasil disubmit',
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress')}}/{{Auth::user()->group}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Error!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function pilihFile() {
		$("#fileKu").click();
	}
	function pilihFile2() {
		$("#fileKu2").click();
	}
	function hapus() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('del_foto_progress')}}/foto_redaman/{{$data->sc}}",
			method: "DELETE",
			success: function(res) {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Foto redaman berhasil dihapus!',
				});
				$("#foto_redaman").attr("src","{{asset('foto')}}/"+res.foto);
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function hapus2() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('del_foto_progress')}}/foto_after/{{$data->sc}}",
			method: "DELETE",
			success: function(res) {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Foto after berhasil dihapus!',
				});
				$("#foto_after").attr("src","{{asset('foto')}}/"+res.foto);
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function upload() {
		$.LoadingOverlay("show");
		$file_data = $("#formKu input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('foto_redaman',$file_data);
        //fd.append("CustomField", "This is some extra data");
        $.ajax({
            url: "{{Route('upload_foto_progress')}}/{{$data->sc}}",
            method: 'POST',
            data: $form_data,
            cache  : false,
			contentType: false,
			processData: false,
            success:function(res){
            	$("#fileKu").val("");
            	$.LoadingOverlay("hide");
            	if(res.success) {
            		success_upload();
            		$("#foto_redaman").attr("src","{{asset('foto')}}/"+res.foto);
            	} else {
            		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
            	}
            },
            error: function() {
            	$("#fileKu").val("");
            	$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
            },
            cache: false,
            contentType: false,
            processData: false
        });
	}
	function upload2() {
		$.LoadingOverlay("show");
		$file_data = $("#formKu2 input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('foto_after',$file_data);
        //fd.append("CustomField", "This is some extra data");
        $.ajax({
            url: "{{Route('upload_foto_progress')}}/{{$data->sc}}",
            method: 'POST',
            data: $form_data,
            cache  : false,
			contentType: false,
			processData: false,
            success:function(res){
            	$("#fileKu2").val("");
            	$.LoadingOverlay("hide");
            	if(res.success) {
            		success_upload();
            		$("#foto_after").attr("src","{{asset('foto')}}/"+res.foto);
            	} else {
            		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
            	}
            },
            error: function() {
            	$("#fileKu2").val("");
            	$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
            },
            cache: false,
            contentType: false,
            processData: false
        });
	}
	function success_upload() {
		$.alert({
		    title: 'Success!',
		    content: 'File success uploaded!',
		});
	}
</script>
@endsection
@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12" style="text-align:center;">
				<h3 style="font-weight:bold;"><u>~{{$data->sc}}~</u></h3>
			</div>
			<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">
				<div class="form-group col-md-12">
					<label for="odp">Nama ODP</label>	
					<input class="form-control" type="text" name="odp_name" placeholder="Contoh: ODP-JMB-FA/01" required value="{{$data->odp_name}}" id="odp_name" disabled>
				</div>
				<div class="form-group col-md-12">
					<label>Foto Redaman ODP</label>	
					<div style="clear:both;"></div>
					@if($data->foto_redaman==null)
					<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:240px;height:320px" id="foto_redaman">
					@else
					<img src="{{asset('foto')}}/{{$data->foto_redaman}}" alt="Foto Redaman" style="border:1px solid grey;width:240px;height:320px" id="foto_redaman">
					@endif
					<div style="clear:both;margin:10px"></div>
					<form id="formKu" method="post" style="display:none" action="#">
						<input type="file" accept=".jpg,.jpeg" id="fileKu" onchange="upload()">
					</form>
					</div>
				<div class="form-group col-md-12">
					<label for="mancore">Mancore</label>
					<textarea class="form-control" placeholder="Mancore Code" id="mancore" disabled>{{$data->mancore}}</textarea>
				</div>
				@if(!$data->is_validated_by_cs)
				<div class="form-group col-md-12">
					<label>Foto Push Tenoss</label>	
					<div style="clear:both;"></div>
					@if($data->foto_push==null)
					<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:240px;height:320px" id="foto_push">
					@else
					<img src="{{asset('foto')}}/{{$data->foto_push}}" alt="Foto Push Tenoss" style="border:1px solid grey;width:240px;height:320px" id="foto_push">
					@endif
					<div style="clear:both;margin:10px"></div>
					<form id="formKu" method="post" style="display:none" action="#">
						<input type="file" accept=".jpg, .jpeg" id="fileKu" onchange="upload()">
					</form>
				</div>
				@endif
				<div class="form-group col-md-12">
					<p>{{$data->sc}} meminta konfirmasi Go Live</p>
				</div>
				<div style="text-align:center;margin-bottom:30px">
					<!-- <a class="btn btn-sm btn-danger" href="{{Route('do_validate_progress')}}/{{$data->sc}}/DEC"><i class="fa fa-times"></i> Tidak Valid</a> -->
					<a class="btn btn-sm btn-warning" href="{{Route('do_validate_progress')}}/{{$data->sc}}/ACC"><i class="fa fa-time"></i> Konfirmasi Tidak Go Live</a>
					<a class="btn btn-sm btn-primary" href="{{Route('do_validate_progress')}}/{{$data->sc}}/ACC"><i class="fa fa-check"></i> Konfirmasi Go Live</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function dosubmit() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('update_progress')}}/sdi/{{$data->sc}}",
			method: "POST",
			data  : {
				"submit_final" : true,
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: 'Data berhasil disubmit',
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress')}}/{{Auth::user()->group}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Error!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function pilihFile() {
		$("#fileKu").click();
	}
	function pilihFile2() {
		$("#fileKu2").click();
	}
	function hapus() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('del_foto_progress')}}/foto_push/{{$data->sc}}",
			method: "DELETE",
			success: function(res) {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Foto push tenoss berhasil dihapus!',
				});
				$("#foto_push").attr("src","{{asset('foto')}}/"+res.foto);
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function hapus2() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('del_foto_progress')}}/foto_connectivity/{{$data->sc}}",
			method: "DELETE",
			success: function(res) {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Foto Connectivity Dashboard Fulfillment berhasil dihapus!',
				});
				$("#foto_connectivity").attr("src","{{asset('foto')}}/"+res.foto);
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function upload() {
		$.LoadingOverlay("show");
		$file_data = $("#formKu input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('foto_push',$file_data);
        //fd.append("CustomField", "This is some extra data");
        $.ajax({
            url: "{{Route('upload_foto_progress')}}/{{$data->sc}}",
            method: 'POST',
            data: $form_data,
            cache  : false,
			contentType: false,
			processData: false,
            success:function(res){
            	$("#fileKu").val("");
            	$.LoadingOverlay("hide");
            	if(res.success) {
            		success_upload();
            		$("#foto_push").attr("src","{{asset('foto')}}/"+res.foto);
            	} else {
            		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
            	}
            },
            error: function() {
            	$("#fileKu").val("");
            	$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
            },
            cache: false,
            contentType: false,
            processData: false
        });
	}
	function upload2() {
		$.LoadingOverlay("show");
		$file_data = $("#formKu2 input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('foto_connectivity',$file_data);
        //fd.append("CustomField", "This is some extra data");
        $.ajax({
            url: "{{Route('upload_foto_progress')}}/{{$data->sc}}",
            method: 'POST',
            data: $form_data,
            cache  : false,
			contentType: false,
			processData: false,
            success:function(res){
            	$("#fileKu2").val("");
            	$.LoadingOverlay("hide");
            	if(res.success) {
            		success_upload();
            		$("#foto_connectivity").attr("src","{{asset('foto')}}/"+res.foto);
            	} else {
            		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
            	}
            },
            error: function() {
            	$("#fileKu2").val("");
            	$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
            },
            cache: false,
            contentType: false,
            processData: false
        });
	}
	function success_upload() {
		$.alert({
		    title: 'Success!',
		    content: 'File success uploaded!',
		});
	}
</script>
@endsection
@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		//$('[data-magnify=gallery]').magnify();
	})
</script>
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12">
				<div class="col-md-12" style="text-align:center;">
					<h3 style="font-weight:bold;"><u>~Edit Data~</u></h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group col-md-12">
					<label for="odp">Nomor SC</label>
					<input class="form-control" type="text" value="{{$data->sc}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Nama Pelanggan</label>
					<input class="form-control" type="text" value="{{$data->nama_pelanggan}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Nomor HP</label>
					<input class="form-control" type="text" value="{{$data->no_hp}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Logs</label>
					@if($data->status!="UNSPEC")
					<textarea class="form-control" disabled>{{$data->pesan_dari_tim_validasi_survei}}</textarea>
					@else
					<textarea class="form-control" disabled>{{$data->message_rejected}}</textarea>
					@endif
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group col-md-12">
					<label>Status Hasil Survei</label>
					@php
						$val_survei = [
							["text" => "SURVEI-PT2/PT3","id" => "survei"],
							["text" => "PT 1 GO LIVE","id" => "pt1"],
							["text" => "PS","id" => "ps"],
							["text" => "DOUBLE","id" => "double_sc"],
							["text" => "SC","id" => "cancel"],
							["text" => "CANCEL","id" => "pending_pelanggan"],
							["text" => "PENDING PELANGGAN","id" => "no_address"],
							["text" => "ALAMAT TIDAK KETEMU / CP RNA","id" => "pelanggan_ragu"],
							["text" => "KOORDINAT PELANGGAN TIDAK ADA","id" => "no_coordinate"],
							["text" => "PT3","id" => "pt3"],
							["text" => "PT4","id" => "pt4"],
							["text" => "TERCOVER PLAN QE","id" => "qe"],
							["text" => "PT2","id" => "pt2"],
							["text" => "ODP BELUM GO LIVE","id" => "odp_belum_go_live"],
							["text" => "TANAM TIANG","id" => "tanam_tiang"],
							["text" => "UNSPEC/LOSS","id" => "unspec"],
						];
					@endphp
					<select class="form-control" id="status" onchange="smart_handle()">
						@foreach($val_survei as $val)
							@if($val['text'] == $data->status)
							<option value="{{$val['id']}}" selected>{{$val['text']}}</option>
							@else
							<option value="{{$val['id']}}">{{$val['text']}}</option>
							@endif
						@endforeach
					</select>
				</div>
				<div class="syarat-cancel">
					<div class="form-group col-md-10">
						<label for="odp">Tanggal HS</label>
						<input class="form-control" type="date" id="tgl_hs" value="{{$data->tgl_hs}}" disabled="disabled">
					</div>
					<div class="col-md-2" style="padding-top:5px;padding-left:0px">
						<label></label>
						<button class="btn btn-primary btn-sm" onclick="enable_edit_hs()">Rubah tgl HS</button>
					</div>
					<div class="form-group col-md-12">
						<label>Catatan</label>
						<textarea placeholder="Catatan perubahan" class="form-control" id="pesan_reject"></textarea>
					</div>
				</div>

				<!-- @if(
					$data->status=="PENDING PELANGGAN" ||
					$data->status=="ALAMAT TIDAK KETEMU / CP RNA" ||
					$data->status=="PELANGGAN RAGU" ||
					$data->status=="CANCEL" ||
					$data->status=="KOORDINAT PELANGGAN TIDAK ADA"
				)
				<div class="syarat-cancel">
					<div class="form-group col-md-12">
						<label for="odp">Nama Pelanggan</label>
						<input class="form-control" type="text" placeholder="Contoh: John Gilbert" required id="nama_pelanggan">
					</div>
					<div class="form-group col-md-12">
						<label for="odp">Nomor HP</label>
						<input class="form-control" type="text" placeholder="Contoh: 082282170000" required id="no_hp">
					</div>
					<input type="hidden" name="" value="need_info_pelanggan" id="need_info_pelanggan">
				</div>
				@else
					<input type="hidden" name="" value="null" id="need_info_pelanggan">
				@endif
				@if($data->status=="KOORDINAT PELANGGAN TIDAK ADA")
					<div class="form-group col-md-12">
						<label>Foto GPS MAP Rumah Pelanggan</label>	
						<div style="clear:both;"></div>
						@if($data->foto_gps_map==null)
						<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:100px;height:130px" id="foto_gps_map" data-src="{{asset('foto/default.jpg')}}" data-magnify="gallery">
						@else
						<img src="{{asset('foto')}}/{{$data->foto_gps_map}}" alt="Foto GP MAP" style="border:1px solid grey;width:100px;height:130px" id="foto_gps_map" data-src="{{asset('foto')}}/{{$data->foto_gps_map}}" data-magnify="gallery">
						@endif
						<div style="clear:both;margin:10px"></div>
						<button class="btn btn-primary" onclick="pilihFile2()"><i class="fa fa-upload"></i> Upload Foto</button>
						<button class="btn btn-danger" onclick="hapus2()"><i class="fa fa-trash"></i> Hapus Foto</button>
						<form id="formKu2" method="post" style="display:none" action="#">
							<input type="file" accept=".jpg,.jpeg" id="fileKu2" onchange="upload2()">
						</form>
					</div>
					<input type="hidden" name="" value="need" id="need_gps_map">
				@else
					<input type="hidden" name="" value="null" id="need_gps_map">
				@endif -->
				
				<div style="text-align:center;margin-bottom:30px">
					<button class="btn btn-sm btn-success" onclick="valid()">
						<i class="fa fa-save"></i> Simpan Perubahan
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$mode_edit_hs = false;
	function enable_edit_hs() {
		if($mode_edit_hs) {
			$("#tgl_hs").attr("disabled","disabled");
			$mode_edit_hs = false;
		} else {
			$("#tgl_hs").removeAttr("disabled");
			$mode_edit_hs = true;
		}
	}
	function pilihFile2() {
		$("#fileKu2").click();
	}
	function hapus2() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('del_foto_progress')}}/foto_gps_map/{{$data->sc}}",
			method: "DELETE",
			success: function(res) {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Foto GPS Map berhasil dihapus!',
				});
				$("#foto_gps_map").attr("src","{{asset('foto')}}/"+res.foto);
				$("#foto_gps_map").attr("data-src","{{asset('foto')}}/"+res.foto);
				$has_upload_gps_map = false;
			},
			error : function(res) {
				$.LoadingOverlay("hide");
	        	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function upload2() {
		$.LoadingOverlay("show");
		$file_data = $("#formKu2 input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('foto_gps_map',$file_data);
	    //fd.append("CustomField", "This is some extra data");
	    $.ajax({
	        url: "{{Route('upload_foto_progress')}}/{{$data->sc}}",
	        method: 'POST',
	        data: $form_data,
	        cache  : false,
			contentType: false,
			processData: false,
	        success:function(res){
	        	$("#fileKu").val("");
	        	$.LoadingOverlay("hide");
	        	if(res.success) {
	        		success_upload();
	        		$("#foto_gps_map").attr("src","{{asset('foto')}}/"+res.foto);
	        		$("#foto_gps_map").attr("data-src","{{asset('foto')}}/"+res.foto);
	        		$has_upload_gps_map = true;
	        	} else {
	        		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
	        	}
	        },
	        error: function() {
	        	$("#fileKu").val("");
	        	$.LoadingOverlay("hide");
	        	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		
	})

	@if($data->foto_gps_map==null)
		$has_upload_gps_map = false;
	@else
		$has_upload_gps_map = true;
	@endif

	function tidak_valid() {
		if($("#need_info_pelanggan").val() == "need_info_pelanggan") {
			$nama = $("#nama_pelanggan").val();
			$ponsel = $("#no_hp").val();
			if($nama=="") {
				$.alert({
				    title: 'Oops!',
				    content: 'Nama pelanggan belum diinput',
				});
				return;
			}
			if($ponsel=="") {
				$.alert({
				    title: 'Oops!',
				    content: 'No hp pelanggan belum diinput',
				});
				return;
			}
			$new_msg = "\n - Nama Pelanggan: "+$nama;
			$new_msg+= "\n - No Pelanggan: "+$ponsel;
			$new_msg+= "\n - Catatan: "+$("#pesan_reject").val();
		} else {
			$new_msg = $("#pesan_reject").val();
		}
		if($("#need_gps_map").val() == "need") {
			if(!$has_upload_gps_map) {
				$.alert({
				    title: 'Oops!',
				    content: "Foto GPS map belum diupload",
				});
				return;
			}
		}
		$pesan = $("#pesan_reject").val();
		if($pesan=="") {
			$.alert({
			    title: 'Oops!',
			    content: "Catatan perlu diisi untuk konfirmasi!",
			});
			return;
		}
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('validasi_hasil_survei_pt23')}}/{{$data->sc}}",
			method: "POST",
			data  : {
				"validasi" : "not_valid",
				"pesan_reject" : $new_msg,
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: res.pesan,
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress_closing')}}/{{Auth::user()->group}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Oops!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}

	function valid() {
		$tgl_hs = $("#tgl_hs").val();
		if($tgl_hs=="") {
			$.alert({
			    title: 'Oops!',
			    content: 'Tanggal HS belum diinput!',
			});
			return;
		}
		$pesan = $("#pesan_reject").val();
		if($pesan=="") {
			$.alert({
			    title: 'Oops!',
			    content: 'Catatan/pesan belum diinput!',
			});
			return;
		}
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('admin_decision_edit')}}/{{$data->sc}}",
			method: "POST",
			data  : {
				"status" : $("#status").val(),
				"tgl_hs" : $tgl_hs,
				"pesan"  : $pesan,
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: res.pesan,
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress_closing')}}/{{Auth::user()->group}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Oops!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
</script>
@endsection
@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section("style")
<style type="text/css">
.center {
	text-align:center;
}
.image-preview {
  width: 240px;
  height: 320px;
  background-color: #ffffff;
  color: #ecf0f1;
  border:2px solid black;
  box-shadow: 2px 2px 2px grey;
  margin:10px;
  display:inline-block;
}
.image-preview input {
  line-height: 200px;
  font-size: 200px;
  position: absolute;
  opacity: 0;
  z-index: 10;
}
.image-preview label {
  position: absolute;
  z-index: 5;
  opacity: 0.8;
  cursor: pointer;
  background-color: #bdc3c7;
  width: 200px;
  height: 50px;
  font-size: 20px;
  line-height: 50px;
  text-transform: uppercase;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  text-align: center;
}
</style>
@endsection

@section('content')
<div class="container" style="margin:auto">
	<div class="row">
		<div class="main-content-content container-fluid box">
			<form action="{{Route('upload_tiang')}}/{{$data->sc}}" method="POST" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="col-md-12" style="text-align:center;margin-bottom:20px">
				<h3 style="font-weight:bold;"><u>~{{$data->sc}}~</u></h3>
			</div>
			@for($i=0; $i<$data->jumlah_tiang; $i++)
			<div class="col-md-3">
				<div class="image-preview foto-tiang-{{$i}}">
					<label for="image-upload" id="image-label-{{$i}}">Pilih Foto Tiang {{$i+1}}</label>
					<input type="file" name="image[]" id="image-upload-{{$i}}" accept=".jpg,.jpeg" required>
				</div>
			</div>
			@endfor
			<button class="btn btn-md btn-primary" style="width: 36%; margin: 40px 32%;" onclick="dosubmit()">Submit</button>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		@for($i=0; $i<$data->jumlah_tiang; $i++)
		  $.uploadPreview({
		    input_field: "#image-upload-{{$i}}",   // Default: .image-upload
		    preview_box: ".foto-tiang-{{$i}}",  // Default: .image-preview
		    label_field: "#image-label-{{$i}}",    // Default: .image-label
		    label_default: "Pilih Foto Tiang {{$i+1}}",   // Default: Choose File
		    label_selected: "Ganti Foto Tiang {{$i+1}}",  // Default: Change File
		    no_label: false                 // Default: false
		  });
		@endfor
	});
</script>
@endsection

@section("script")
<script type="text/javascript" src="{{asset('js/jquery.uploadPreview.js')}}"></script>
@endsection
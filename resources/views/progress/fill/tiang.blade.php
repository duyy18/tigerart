@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12" style="text-align:center;">
				<h3 style="font-weight:bold;"><u>~{{$data->sc}}~</u></h3>
			</div>
			<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">
				<div class="form-group col-md-12">
					<label for="odp">Jumlah Tiang</label>
					@if($data->jumlah_tiang==null)
					<input class="form-control" type="number" name="jumlah_tiang" placeholder="Contoh: 3" required id="jumlah_tiang" value="3">
					@else
					<input class="form-control" type="number" name="jumlah_tiang" placeholder="Contoh: 3" required value="{{$data->jumlah_tiang}}" id="jumlah_tiang">
					@endif
				</div>

				<button class="btn btn-sm btn-primary" style="width: 36%; margin: 40px 32%;" onclick="dosubmit()">Submit</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function dosubmit() {
		$jumlah_tiang = $("#jumlah_tiang").val();
		if(!Number.isInteger(parseInt($jumlah_tiang))) {
			$.alert({
			    title: 'Ooops!',
			    content: 'Jumlah tiang tidak valid!',
			});
			return;
		} else
		if($jumlah_tiang<1) {
			$.alert({
			    title: 'Ooops!',
			    content: 'Setidaknya jumlah tiang adalah 1!',
			});
			return;
		};
		if($jumlah_tiang>7) {
			$.alert({
			    title: 'Ooops!',
			    content: 'Jumlah tiang paling banyak adalah 7!',
			});
			return;
		}
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('update_progress')}}/sektor/{{$data->sc}}",
			method: "POST",
			data  : {
				"jumlah_tiang" : $("#jumlah_tiang").val(),
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					window.open("{{Route('fill_progress')}}/{{$data->sc}}?next_step","_SELF");
				} else
					$.alert({
					    title: 'Error!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
</script>
@endsection
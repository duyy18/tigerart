@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		$('[data-magnify=gallery]').magnify();
	})
</script>
@endsection

@section('content')


<!-- Model Pengumuman -->
<div class="modal large fade text-left" id="modal-pengumuman" tabindex="-1" role="dialog" aria-labelledby="myModalLabel12"
aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document" style="width:40%;height:50%;">
	  <div class="modal-content">
	    <div class="modal-header bg-primary white">
	      <h4 class="modal-title white" id="myModalLabel12"><i class="la la-lightbulb-o"></i> Confirmation Message</h4>
	    </div>
	    <div class="modal-body">
	    		<center>Confirm Pending This Order ?</center>
	    </div>
		<div class="modal-footer" style="text-align:center;">
	      <button type="button" class="btn btn-md btn-danger" data-dismiss="modal">NO</button>
	      <button type="button" class="btn btn-md btn-primary" data-dismiss="modal">YES</button>
	    </div>
	  </div>
   </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#modal-pengumuman").modal({backdrop: 'static',keyboard: false});
	})
</script>
<!-- Model Pengumuman -->



<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12">
				<div class="col-md-12" style="text-align:center;">
					<h3 style="font-weight:bold;"><u>~Edit Data~</u></h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group col-md-12">
					<label for="odp">Nomor SC</label>
					<input class="form-control" type="text" value="{{$data->sc}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Nama Pelanggan</label>
					<input class="form-control" type="text" value="{{$data->nama_pelanggan}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Nomor HP</label>
					<input class="form-control" type="text" value="{{$data->no_hp}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Logs</label>
					<textarea class="form-control" disabled>{{$data->pesan_dari_tim_validasi_survei}}</textarea>
				</div>
			</div>
			@if($data->nama_pelanggan_2!=null)
			<div class="col-md-4">
				@if($data->nama_pelanggan_2!=null)
				<div class="syarat-cancel">
					<div class="form-group col-md-12">
						<label for="odp">Nama Pelanggan (Yang di survei sektor)</label>
						<input class="form-control" type="text" placeholder="Contoh: John Gilbert" required value="{{$data->nama_pelanggan_2}}" disabled="disabled">
					</div>
					<div class="form-group col-md-12">
						<label for="odp">Nomor HP (Yang di survei sektor)</label>
						<input class="form-control" type="text" placeholder="Contoh: 082282170000" required value="{{$data->no_hp_2}}" disabled="disabled">
					</div>
				</div>
				@else
				<div class="syarat-cancel">
					<div class="form-group col-md-12">
						<label for="odp">Nama Pelanggan (Yang di survei sektor)</label>
						<input class="form-control" type="text" placeholder="Contoh: John Gilbert" required value="{{$data->nama_pelanggan}}" disabled="disabled">
					</div>
					<div class="form-group col-md-12">
						<label for="odp">Nomor HP (Yang di survei sektor)</label>
						<input class="form-control" type="text" placeholder="Contoh: 082282170000" required value="{{$data->no_hp}}" disabled="disabled">
					</div>
				</div>
				@endif
			</div>
			@endif
			<div class="col-md-4">
				<div class="form-group col-md-12">
					<label>Status Hasil Survei</label>
					<select class="form-control" id="status" onchange="smart_handle()" disabled>
						<option>{{$data->status}}</option>
					</select>
				</div>
				@if(
					$data->status=="PENDING PELANGGAN" ||
					$data->status=="ALAMAT TIDAK KETEMU / CP RNA" ||
					$data->status=="PELANGGAN RAGU" ||
					$data->status=="CANCEL" ||
					$data->status=="KOORDINAT PELANGGAN TIDAK ADA"
				)
				<div class="syarat-cancel">
					<div class="form-group col-md-12">
						<label for="odp">Nama Pelanggan</label>
						<input class="form-control" type="text" placeholder="Contoh: John Gilbert" required id="nama_pelanggan">
					</div>
					<div class="form-group col-md-12">
						<label for="odp">Nomor HP</label>
						<input class="form-control" type="text" placeholder="Contoh: 082282170000" required id="no_hp">
					</div>
					<input type="hidden" name="" value="need_info_pelanggan" id="need_info_pelanggan">
				</div>
				@else
					<input type="hidden" name="" value="null" id="need_info_pelanggan">
				@endif
				@if($data->status=="KOORDINAT PELANGGAN TIDAK ADA")
					<div class="form-group col-md-12">
						<label>Foto GPS MAP Rumah Pelanggan</label>	
						<div style="clear:both;"></div>
						@if($data->foto_gps_map==null)
						<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:100px;height:130px" id="foto_gps_map" data-src="{{asset('foto/default.jpg')}}" data-magnify="gallery">
						@else
						<img src="{{asset('foto')}}/{{$data->foto_gps_map}}" alt="Foto GP MAP" style="border:1px solid grey;width:100px;height:130px" id="foto_gps_map" data-src="{{asset('foto')}}/{{$data->foto_gps_map}}" data-magnify="gallery">
						@endif
						<div style="clear:both;margin:10px"></div>
						<button class="btn btn-primary" onclick="pilihFile2()"><i class="fa fa-upload"></i> Upload Foto</button>
						<button class="btn btn-danger" onclick="hapus2()"><i class="fa fa-trash"></i> Hapus Foto</button>
						<form id="formKu2" method="post" style="display:none" action="#">
							<input type="file" accept=".jpg,.jpeg" id="fileKu2" onchange="upload2()">
						</form>
					</div>
					<input type="hidden" name="" value="need" id="need_gps_map">
				@else
					<input type="hidden" name="" value="null" id="need_gps_map">
				@endif
				<div class="form-group col-md-12">
					<label>Catatan</label>
					<textarea placeholder="*) Dibutuhkan jika hasil survei tidak valid" class="form-control" id="pesan_reject"></textarea>
				</div>
				<div style="text-align:center;margin-bottom:30px">
					<button class="btn btn-sm btn-danger" onclick="tidak_valid()">
						<i class="fa fa-times"></i> Tidak Valid
					</button>
					<button class="btn btn-sm btn-primary" onclick="valid()">
						<i class="fa fa-check"></i> Validasi
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function pilihFile2() {
		$("#fileKu2").click();
	}
	function hapus2() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('del_foto_progress')}}/foto_gps_map/{{$data->sc}}",
			method: "DELETE",
			success: function(res) {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Foto GPS Map berhasil dihapus!',
				});
				$("#foto_gps_map").attr("src","{{asset('foto')}}/"+res.foto);
				$("#foto_gps_map").attr("data-src","{{asset('foto')}}/"+res.foto);
				$has_upload_gps_map = false;
			},
			error : function(res) {
				$.LoadingOverlay("hide");
	        	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function upload2() {
		$.LoadingOverlay("show");
		$file_data = $("#formKu2 input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('foto_gps_map',$file_data);
	    //fd.append("CustomField", "This is some extra data");
	    $.ajax({
	        url: "{{Route('upload_foto_progress')}}/{{$data->sc}}",
	        method: 'POST',
	        data: $form_data,
	        cache  : false,
			contentType: false,
			processData: false,
	        success:function(res){
	        	$("#fileKu").val("");
	        	$.LoadingOverlay("hide");
	        	if(res.success) {
	        		$.alert({
					    title: 'Success!',
					    content: 'File success uploaded!!',
					});
	        		$("#foto_gps_map").attr("src","{{asset('foto')}}/"+res.foto);
	        		$("#foto_gps_map").attr("data-src","{{asset('foto')}}/"+res.foto);
	        		$has_upload_gps_map = true;
	        	} else {
	        		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
	        	}
	        },
	        error: function() {
	        	$("#fileKu").val("");
	        	$.LoadingOverlay("hide");
	        	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		
	})

	@if($data->foto_gps_map==null)
		$has_upload_gps_map = false;
	@else
		$has_upload_gps_map = true;
	@endif

	function tidak_valid() {
		if($("#need_info_pelanggan").val() == "need_info_pelanggan") {
			$nama = $("#nama_pelanggan").val();
			$ponsel = $("#no_hp").val();
			if($nama=="") {
				$.alert({
				    title: 'Oops!',
				    content: 'Nama pelanggan belum diinput',
				});
				return;
			}
			if($ponsel=="") {
				$.alert({
				    title: 'Oops!',
				    content: 'No hp pelanggan belum diinput',
				});
				return;
			}
			$new_msg = "\n - Nama Pelanggan: "+$nama;
			$new_msg+= "\n - No Pelanggan: "+$ponsel;
			$new_msg+= "\n - Catatan: "+$("#pesan_reject").val();
		} else {
			$new_msg = $("#pesan_reject").val();
		}
		if($("#need_gps_map").val() == "need") {
			if(!$has_upload_gps_map) {
				$.alert({
				    title: 'Oops!',
				    content: "Foto GPS map belum diupload",
				});
				return;
			}
		}
		$pesan = $("#pesan_reject").val();
		if($pesan=="") {
			$.alert({
			    title: 'Oops!',
			    content: "Catatan perlu diisi untuk konfirmasi!",
			});
			return;
		}
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('validasi_hasil_survei_pt23')}}/{{$data->sc}}",
			method: "POST",
			data  : {
				"validasi" : "not_valid",
				"pesan_reject" : $new_msg,
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: res.pesan,
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress_closing')}}/{{Auth::user()->group}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Oops!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}

	function valid() {
		$pesan = $("#pesan_reject").val();
		if($pesan=="") {
			$.alert({
			    title: 'Oops!',
			    content: "Catatan perlu diisi untuk konfirmasi!",
			});
			return;
		}
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('validasi_hasil_survei_pt23')}}/{{$data->sc}}",
			method: "POST",
			data  : {
				"validasi" : "valid",
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: res.pesan,
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress_closing')}}/{{Auth::user()->group}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Oops!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
</script>
@endsection
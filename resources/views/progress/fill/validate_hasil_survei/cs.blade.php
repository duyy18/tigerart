@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12">
				<div class="col-md-12" style="text-align:center;">
					<h3 style="font-weight:bold;"><u>~Edit Data~</u></h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group col-md-12">
					<label for="odp">Nomor SC</label>
					<input class="form-control" type="text" value="{{$data->sc}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Nama Pelanggan</label>
					<input class="form-control" type="text" value="{{$data->nama_pelanggan}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Nomor HP</label>
					<input class="form-control" type="text" value="{{$data->no_hp}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Logs</label>
					<textarea class="form-control" disabled>{{$data->pesan_dari_tim_validasi_survei}}</textarea>
				</div>
			</div>
			<div class="col-md-8">
				<div class="col-md-6 col-md-offset-3">
				<div class="form-group col-md-12" style="margin-top:20px">
					<label>Status Hasil Survei</label>
					<select class="form-control" id="status" onchange="smart_handle()" disabled>
						<option>{{$data->status}}</option>
					</select>
				</div>
				@if($data->status=="CANCEL")
					@if($data->nama_pelanggan_2!=null)
					<div class="syarat-cancel">
						<div class="form-group col-md-12">
							<label for="odp">Nama Pelanggan</label>
							<input class="form-control" type="text" placeholder="Contoh: John Gilbert" required value="{{$data->nama_pelanggan_2}}" id="nama_pelanggan" disabled="disabled">
						</div>
						<div class="form-group col-md-12">
							<label for="odp">Nomor HP</label>
							<input class="form-control" type="text" placeholder="Contoh: 082282170000" required value="{{$data->no_hp_2}}" id="no_hp" disabled="disabled">
						</div>
					</div>
					@else
					<div class="syarat-cancel">
						<div class="form-group col-md-12">
							<label for="odp">Nama Pelanggan</label>
							<input class="form-control" type="text" placeholder="Contoh: John Gilbert" required value="{{$data->nama_pelanggan}}" id="nama_pelanggan" disabled="disabled">
						</div>
						<div class="form-group col-md-12">
							<label for="odp">Nomor HP</label>
							<input class="form-control" type="text" placeholder="Contoh: 082282170000" required value="{{$data->no_hp}}" id="no_hp" disabled="disabled">
						</div>
					</div>
					@endif
				@endif
				@if($data->pesan_dari_tim_validasi_survei!=null)
				<div class="form-group col-md-12">
					<label>Logs</label>
					<textarea class="form-control" disabled="disabled">{{$data->pesan_dari_tim_validasi_survei}}</textarea>
				</div>
				@endif
				<div class="form-group col-md-12">
					<label>Catatan</label>
					<textarea placeholder="*) Dibutuhkan jika hasil survei tidak valid" class="form-control" id="pesan_reject"></textarea>
				</div>
				<div style="text-align:center;margin-bottom:30px">
					<button class="btn btn-sm btn-danger" onclick="tidak_valid()">
						<i class="fa fa-times"></i> Tidak Valid
					</button>
					<button class="btn btn-sm btn-primary" onclick="valid()">
						<i class="fa fa-check"></i> Validasi
					</button>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		
	})

	function tidak_valid() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('validasi_hasil_survei_pt23')}}/{{$data->sc}}",
			method: "POST",
			data  : {
				"validasi" : "not_valid",
				"pesan_reject" : $("#pesan_reject").val(),
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: res.pesan,
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress_closing')}}/{{Auth::user()->group}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Oops!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}

	function valid() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('validasi_hasil_survei_pt23')}}/{{$data->sc}}",
			method: "POST",
			data  : {
				"validasi" : "valid",
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: res.pesan,
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress_closing')}}/{{Auth::user()->group}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Oops!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
</script>
@endsection
@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		$('[data-magnify=gallery]').magnify();
	})
</script>
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12">
				<div class="col-md-12" style="text-align:center;">
					<h3 style="font-weight:bold;"><u>~Edit Data~</u></h3>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group col-md-12">
					<label for="odp">Nomor SC</label>
					<input class="form-control" type="text" value="{{$data->sc}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Nama Pelanggan</label>
					<input class="form-control" type="text" value="{{$data->nama_pelanggan}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Nomor HP</label>
					<input class="form-control" type="text" value="{{$data->no_hp}}" disabled>
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Logs</label>
					<textarea class="form-control" disabled>{{$data->pesan_dari_tim_validasi_survei}}</textarea>
				</div>
			</div>
			<div class="col-md-6">
				<!-- <div class="col-md-6 col-md-offset-3"> -->
					<div class="form-group col-md-12" style="margin-top:20px">
						<label>Pilih Status</label>
						<select class="form-control" id="status" onchange="smart_handle()">
							<option value="pt1">PT 1 GO LIVE</option>
							<option value="ps">PS</option>
							<option value="double_sc">DOUBLE SC</option>
							<option value="cancel">CANCEL</option>
							<option value="pending_pelanggan">PENDING PELANGGAN</option>
							<option value="no_address">ALAMAT TIDAK KETEMU / CP RNA</option>
							<option value="pelanggan_ragu">PELANGGAN RAGU</option>
							<option value="no_coordinate">KOORDINAT PELANGGAN TIDAK ADA</option>
							<option value="pt3">PT3</option>
							<option value="pt4">PT4</option>
							<option value="qe">TERCOVER PLAN QE</option>
							<option value="pt2" selected>PT2</option>
							<option value="odp_belum_go_live">ODP BELUM GO LIVE</option>
							<option value="tanam_tiang">TANAM TIANG</option>
							<option value="unspec">UNSPEC/LOSS</option>
						</select>
					</div>
					<div class="syarat-pt3 syarat-pt4 syarat-qe" style="display:none">
						<div class="form-group col-md-12">
							<label>Foto GPS MAP</label>	
							<div style="clear:both;"></div>
							@if($data->foto_gps_map==null)
							<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:100px;height:130px" id="foto_gps_map" data-src="{{asset('foto/default.jpg')}}" data-magnify="gallery">
							@else
							<img src="{{asset('foto')}}/{{$data->foto_gps_map}}" alt="Foto GP MAP" style="border:1px solid grey;width:100px;height:130px" id="foto_gps_map" data-src="{{asset('foto')}}/{{$data->foto_gps_map}}" data-magnify="gallery">
							@endif
							<div style="clear:both;margin:10px"></div>
							<button class="btn btn-primary" onclick="pilihFile2()"><i class="fa fa-upload"></i> Upload Foto</button>
							<button class="btn btn-danger" onclick="hapus2()"><i class="fa fa-trash"></i> Hapus Foto</button>
							<form id="formKu2" method="post" style="display:none" action="#">
								<input type="file" accept=".jpg,.jpeg" id="fileKu2" onchange="upload2()">
							</form>
						</div>
					</div>
					<div class="syarat-odp_go_live syarat-pt_1 syarat-pending" style="display:none">
						<div class="form-group col-md-12">
							<label for="odp">Nama ODP</label>
							<input class="form-control" type="text" name="odp_name" placeholder="Contoh: ODP-JMB-FA/01" required value="{{$data->odp_name}}" id="odp_name">
						</div>
					</div>
					<div class="syarat-odp_go_live" style="display:none">
						<div class="form-group col-md-12">
							<label>Foto GPS MAP</label>	
							<div style="clear:both;"></div>
							@if($data->foto_gps_map==null)
							<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:100px;height:130px" id="foto_gps_map2" data-src="{{asset('foto/default.jpg')}}" data-magnify="gallery">
							@else
							<img src="{{asset('foto')}}/{{$data->foto_gps_map}}" alt="Foto GP MAP" style="border:1px solid grey;width:100px;height:130px" id="foto_gps_map2" data-src="{{asset('foto')}}/{{$data->foto_gps_map}}" data-magnify="gallery">
							@endif
							<div style="clear:both;margin:10px"></div>
							<button class="btn btn-primary" onclick="pilihFile3()"><i class="fa fa-upload"></i> Upload Foto</button>
							<button class="btn btn-danger" onclick="hapus3()"><i class="fa fa-trash"></i> Hapus Foto</button>
							<form id="formKu3" method="post" style="display:none" action="#">
								<input type="file" accept=".jpg,.jpeg" id="fileKu3" onchange="upload3()">
							</form>
						</div>
					</div>
					<div class="syarat-pt_1 syarat-pending" style="display:none">
						<div class="form-group col-md-12">
							<label>Foto Redaman ODP</label>	
							<div style="clear:both;"></div>
							@if($data->foto_redaman==null)
							<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:100px;height:130px" id="foto_redaman" data-magnify="gallery" data-src="{{asset('foto/default.jpg')}}">
							@else
							<img src="{{asset('foto')}}/{{$data->foto_redaman}}" alt="Foto Redaman" style="border:1px solid grey;width:100px;height:130px" id="foto_redaman" data-magnify="gallery" data-src="{{asset('foto')}}/{{$data->foto_redaman}}">
							@endif
							<div style="clear:both;margin:10px"></div>
							<button class="btn btn-primary" onclick="pilihFile()"><i class="fa fa-upload"></i> Upload Foto</button>
							<button class="btn btn-danger" onclick="hapus()"><i class="fa fa-trash"></i> Hapus Foto</button>
							<form id="formKu" method="post" style="display:none" action="#">
								<input type="file" accept=".jpg,.jpeg" id="fileKu" onchange="upload()">
							</form>
						</div>
					</div>
					<div class="syarat-cancel" style="display:none">
						<div class="form-group col-md-12">
							<label for="odp">Nama Pelanggan</label>
							<input class="form-control" type="text" placeholder="Contoh: John Gilbert" required value="{{$data->nama_pelanggan_2}}" id="nama_pelanggan">
						</div>
						<div class="form-group col-md-12">
							<label for="odp">Nomor HP</label>
							<input class="form-control" type="text" placeholder="Contoh: 082282170000" required value="{{$data->no_hp_2}}" id="no_hp">
						</div>
					</div>
					@if($data->pesan_dari_tim_validasi_survei!=null)
					<div class="form-group col-md-12">
						<label>Catatan</label>
						<textarea class="form-control" id="catatan_survei"></textarea>
					</div>
					@else
					<div class="form-group col-md-12" style="display:none">
						<label>Catatan</label>
						<textarea class="form-control" id="catatan_survei"></textarea>
					</div>
					@endif
				<!-- </div> -->
			</div>
			<div class="col-md-2">
				<button class="btn btn-sm btn-primary" style="width: 36%; margin: 40px 32%;" onclick="dosubmit()">Submit</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		smart_handle();
	})
	$handle = "pt2";
	function hide_all() {
		$(".syarat-cancel").attr("style","display:none");
		$(".syarat-pt_1").attr("style","display:none");
		$(".syarat-pt3").attr("style","display:none");
		$(".syarat-pt4").attr("style","display:none");
		$(".syarat-pending").attr("style","display:none");
		$(".syarat-odp_go_live").attr("style","display:none");
	}
	function smart_handle() {
		$status = $("#status").val();
		hide_all();
		$handle = $status;
		if($status=="pt1") {
			$(".syarat-pt_1").attr("style","display:block");
		} else if(
				$status=="cancel" ||
				$status=="pending_pelanggan" ||
				$status=="no_address" ||
				$status=="pelanggan_ragu" ||
				$status=="no_coordinate"
			) {
			$(".syarat-cancel").attr("style","display:block");
		} else if($status=="pt3" || $status=="qe") {
			$(".syarat-pt3").attr("style","display:block");
		} else if($status=="pt4") {
			$(".syarat-pt4").attr("style","display:block");
		} else if($status=="odp_belum_go_live") {
			$(".syarat-odp_go_live").attr("style","display:block");
		}
			
	}
	function dosubmit() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('post_survei_progress')}}",
			method: "POST",
			data  : {
				"sc" : '{{$data->sc}}',
				"status" : $handle,
				"odp_name" : $("#odp_name").val(),
				"nama_pelanggan" : $("#nama_pelanggan").val(),
				"no_hp" : $("#no_hp").val(),
				"catatan_survei" : $("#catatan_survei").val(),
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: res.pesan,
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress')}}/{{Auth::user()->group}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Oops!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function pilihFile() {
		$("#fileKu").click();
	}
	function hapus() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('del_foto_progress')}}/foto_redaman/{{$data->sc}}",
			method: "DELETE",
			success: function(res) {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Foto redaman berhasil dihapus!',
				});
				$("#foto_redaman").attr("src","{{asset('foto')}}/"+res.foto);
				$("#foto_redaman").attr("data-src","{{asset('foto')}}/"+res.foto);
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function upload() {
		$.LoadingOverlay("show");
		$file_data = $("#formKu input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('foto_redaman',$file_data);
        //fd.append("CustomField", "This is some extra data");
        $.ajax({
            url: "{{Route('upload_foto_progress')}}/{{$data->sc}}",
            method: 'POST',
            data: $form_data,
            cache  : false,
			contentType: false,
			processData: false,
            success:function(res){
            	$("#fileKu").val("");
            	$.LoadingOverlay("hide");
            	if(res.success) {
            		success_upload();
            		$("#foto_redaman").attr("src","{{asset('foto')}}/"+res.foto);
            		$("#foto_redaman").attr("data-src","{{asset('foto')}}/"+res.foto);
            	} else {
            		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
            	}
            },
            error: function() {
            	$("#fileKu").val("");
            	$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
            },
            cache: false,
            contentType: false,
            processData: false
        });
	}
	function success_upload() {
		$.alert({
		    title: 'Success!',
		    content: 'File success uploaded!',
		});
	}
	function pilihFile2() {
		$("#fileKu2").click();
	}
	function hapus2() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('del_foto_progress')}}/foto_gps_map/{{$data->sc}}",
			method: "DELETE",
			success: function(res) {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Foto GPS Map berhasil dihapus!',
				});
				$("#foto_gps_map").attr("src","{{asset('foto')}}/"+res.foto);
				$("#foto_gps_map").attr("data-src","{{asset('foto')}}/"+res.foto);
			},
			error : function(res) {
				$.LoadingOverlay("hide");
	        	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function upload2() {
		$.LoadingOverlay("show");
		$file_data = $("#formKu2 input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('foto_gps_map',$file_data);
	    //fd.append("CustomField", "This is some extra data");
	    $.ajax({
	        url: "{{Route('upload_foto_progress')}}/{{$data->sc}}",
	        method: 'POST',
	        data: $form_data,
	        cache  : false,
			contentType: false,
			processData: false,
	        success:function(res){
	        	$("#fileKu").val("");
	        	$.LoadingOverlay("hide");
	        	if(res.success) {
	        		success_upload();
	        		$("#foto_gps_map").attr("src","{{asset('foto')}}/"+res.foto);
	        		$("#foto_gps_map").attr("data-src","{{asset('foto')}}/"+res.foto);
	        	} else {
	        		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
	        	}
	        },
	        error: function() {
	        	$("#fileKu").val("");
	        	$.LoadingOverlay("hide");
	        	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	}
	function pilihFile3() {
		$("#fileKu3").click();
	}
	function hapus3() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('del_foto_progress')}}/foto_gps_map/{{$data->sc}}",
			method: "DELETE",
			success: function(res) {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Foto GPS Map berhasil dihapus!',
				});
				$("#foto_gps_map2").attr("src","{{asset('foto')}}/"+res.foto);
				$("#foto_gps_map2").attr("data-src","{{asset('foto')}}/"+res.foto);
			},
			error : function(res) {
				$.LoadingOverlay("hide");
	        	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function upload3() {
		$.LoadingOverlay("show");
		$file_data = $("#formKu3 input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('foto_gps_map',$file_data);
	    //fd.append("CustomField", "This is some extra data");
	    $.ajax({
	        url: "{{Route('upload_foto_progress')}}/{{$data->sc}}",
	        method: 'POST',
	        data: $form_data,
	        cache  : false,
			contentType: false,
			processData: false,
	        success:function(res){
	        	$("#fileKu").val("");
	        	$.LoadingOverlay("hide");
	        	if(res.success) {
	        		success_upload();
	        		$("#foto_gps_map2").attr("src","{{asset('foto')}}/"+res.foto);
	        		$("#foto_gps_map2").attr("data-src","{{asset('foto')}}/"+res.foto);
	        	} else {
	        		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
	        	}
	        },
	        error: function() {
	        	$("#fileKu").val("");
	        	$.LoadingOverlay("hide");
	        	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });
	}
</script>
@endsection
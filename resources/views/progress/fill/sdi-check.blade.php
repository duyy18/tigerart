@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12" style="text-align:center;">
				<h3 style="font-weight:bold;"><u>~{{$data->sc}}~</u></h3>
			</div>
			<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">
				<div class="form-group col-md-12">
					<label for="odp">Nama ODP</label>	
					<input class="form-control" type="text" name="odp_name" placeholder="Contoh: ODP-JMB-FA/01" required value="{{$data->odp_name}}" id="odp_name" disabled>
				</div>
				<div class="form-group col-md-12">
					<label>Foto Redaman ODP</label>	
					<div style="clear:both;"></div>
					@if($data->foto_redaman==null)
					<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:240px;height:320px" id="foto_redaman">
					@else
					<img src="{{asset('foto')}}/{{$data->foto_redaman}}" alt="Foto Redaman" style="border:1px solid grey;width:240px;height:320px" id="foto_redaman">
					@endif
					<div style="clear:both;margin:10px"></div>
				</div>
				<div class="form-group col-md-12">
					<label for="mancore">Mancore</label>
					<textarea class="form-control" placeholder="Mancore Code" id="mancore" disabled>{{$data->mancore}}</textarea>
				</div>
				<div class="form-group col-md-6">
					<label>File KML</label><br>
					@if($data->kml==null)
					#<a href="#" id="file_kml">File KML Belum Diunggah</a>
					@else
					#<a href="{{asset('kml')}}/{{$data->kml}}" target="_BLANK" id="file_kml">{{$data->kml}}</a>
					@endif
					<div style="clear:both;margin:10px"></div>
				</div>
				<div class="form-group col-md-12">
					<label for="mancore">Keterangan Penolakan</label>
					<textarea class="form-control" id="reason" placeholder="Isi jika pengajuan akan ditolak (eg: foto yang diupload tidak valid)"></textarea>
				</div>
				<div class="col-md-12" style="text-align:center;padding-bottom:20px">
					<button class="btn btn-md btn-danger" onclick="dotolak()">Data Tidak Kompit / Tidak Valid</button>
					<button class="btn btn-md btn-primary" onclick="dosubmit()">Data Komplit /  Valid</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function dosubmit() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('update_progress')}}/sdi/{{$data->sc}}",
			method: "POST",
			data  : {
				"reason" : null,
				"is_pending" : 0,
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: 'Submit success!',
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress')}}/{{$data->sc}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Error!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function dotolak() {
		$reason = $("#reason").val();
		if($reason.trim().length<10) {
			$.alert({
			    title: 'Oops!',
			    content: 'Alasan penolakan harus diinput (minimal 10 karakter)!',
			});
			return;
		}
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('update_progress')}}/sdi/{{$data->sc}}",
			method: "POST",
			data  : {
				"reason" : $reason,
				"is_pending" : 1,
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: 'Submit success!',
					});
					setTimeout(function(){
						window.open("{{Route('edit_progress')}}/{{Auth::user()->group}}","_SELF");
					},3000);					
				} else
					$.alert({
					    title: 'Error!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
</script>
@endsection
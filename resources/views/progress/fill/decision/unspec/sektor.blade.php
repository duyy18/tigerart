@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12" style="text-align:center;">
				<h3 style="font-weight:bold;"><u>~{{$data->sc}}~</u></h3>
			</div>
			<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">
				<form action="{{Route('process_decision_unspec')}}/{{$data->sc}}" method="POST" onsubmit="return validasi()">
					{{csrf_field()}}
					<div class="form-group col-md-12">
						<label for="odp">Logs</label>	
						<textarea class="form-control" disabled style="height:100px">{{$data->message_rejected}}</textarea>
					</div>
					@if($data->foto_redaman_unspec!=null)
					@php $foto = json_decode($data->foto_redaman_unspec); @endphp
					<div class="form-group col-md-12">
						<label for="odp">Riwayat foto Redaman Unspec</label>	
					</div>
					@for($i=0;$i<count($foto);$i++)
					<div class="form-group col-md-6">
						<img src="{{asset('foto')}}/{{$foto[$i]}}" alt="Foto Redaman Unspec" style="border:1px solid grey;width:240px;height:320px">
					</div>
					@endfor
					@endif
					<div class="form-group col-md-12">
						<label>Foto Redaman Unspec</label>	
						<div style="clear:both;"></div>
						@if($data->foto_push==null)
						<img src="{{asset('foto/default.jpg')}}" alt="Foto Belum Diunggah" style="border:1px solid grey;width:240px;height:320px" id="foto_push">
						@else
						<img src="{{asset('foto')}}/{{$data->foto_push}}" alt="Foto Redaman Unspec" style="border:1px solid grey;width:240px;height:320px" id="foto_push">
						@endif
						<div style="clear:both;margin:10px"></div>
						<button class="btn btn-primary" onclick="pilihFile(); return false"><i class="fa fa-upload"></i> Upload Foto</button>
						<button class="btn btn-danger" onclick="hapus(); return false"><i class="fa fa-trash"></i> Hapus Foto</button>
					</div>
					<div class="form-group col-md-12">
						<label for="odp">Aksi / Tindakan</label>	
						<select class="form-control" name="sektor_target" onchange="handle()" onselect="handle()" id="choose">
							<option value="{{$data->sektor}}" selected>Ambil</option>
							<option value="MTC">Kirim ke Unit Maintenance</option>
							<option value="CONS">Kirim ke Unit Konstruksi</option>
						</select>
					</div>
					<div class="form-group col-md-12" id="pesan_manual" style="display:none">
						<label for="odp">Pesan</label>	
						<textarea class="form-control" id="pesan_peralihan" name="pesan_peralihan"></textarea>
					</div>
					<input type="submit" class="btn btn-sm btn-primary" style="width: 36%; margin: 40px 32%;" value="Submit">
				</form>
			</div>
		</div>
	</div>
</div>

<form id="formKu" method="post" style="display:none" action="#">
	<input type="file" accept=".jpg, .jpeg" id="fileKu" onchange="upload()">
</form>

<script type="text/javascript">
	$need_validate = false;
	$count_redirect = parseInt("{{$data->redirect_unspec_count}}");
	$has_upload_foto = false;

	function handle() {
		$select = $("#choose").val();
		if($select=="{{$data->sektor}}") {
			$("#pesan_manual").attr("style","display:none");
			$need_validate = false;
		} else {
			$need_validate = true;
			$("#pesan_manual").attr("style","display:block");
		}
	}

	function validasi() {
		if(!$has_upload_foto) {
			$.alert({
			    title: 'Oops!',
			    content: 'Foto redaman unspec belum diupload!',
			});
			return false;
		} else if($need_validate) {
			$pesan = $("#pesan_peralihan").val();
			if($count_redirect>=4) {
				$.alert({
				    title: 'Oops!',
				    content: 'Anda sudah tidak dapat lagi mengalihkan pekerjaan ini!',
				});
				return false;
			} else if($pesan.trim().length==0) {
				$.alert({
				    title: 'Oops!',
				    content: 'Silahkan tinggalkan pesan!',
				});
				return false;
			} else {
				return true;
			}
		} else
			return true;
	}

	function pilihFile() {
		$("#fileKu").click();
	}
	function hapus() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('del_foto_progress')}}/foto_redaman_unspec/{{$data->sc}}",
			method: "DELETE",
			success: function(res) {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Foto push tenoss berhasil dihapus!',
				});
				$("#foto_push").attr("src","{{asset('foto')}}/"+res.foto);
				$has_upload_foto = false;
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
	function upload() {
		$.LoadingOverlay("show");
		$file_data = $("#formKu input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('foto_redaman_unspec',$file_data);
        //fd.append("CustomField", "This is some extra data");
        $.ajax({
            url: "{{Route('upload_foto_progress')}}/{{$data->sc}}",
            method: 'POST',
            data: $form_data,
            cache  : false,
			contentType: false,
			processData: false,
            success:function(res){
            	$("#fileKu").val("");
            	$.LoadingOverlay("hide");
            	if(res.success) {
            		success_upload();
            		$("#foto_push").attr("src","{{asset('foto')}}/"+res.foto);
            		$has_upload_foto = true;
            	} else {
            		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
            	}
            },
            error: function() {
            	$("#fileKu").val("");
            	$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
            },
            cache: false,
            contentType: false,
            processData: false
        });
	}
	function success_upload() {
		$.alert({
		    title: 'Success!',
		    content: 'File success uploaded!',
		});
	}
</script>

@endsection
@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')
<div class="container">
	<div class="col-md-12" style="padding-left: 0">
		<div class="main-content-content container-fluid box">
			<div class="col-md-12" style="text-align:center;">
				<h3 style="font-weight:bold;"><u>~Data Tidak Memiliki SC~</u></h3>
			</div>
			<div class="col-md-6 col-md-offset-3" style="padding-top: 50px;">
				<div class="form-group col-md-12">
					<label for="odp">Nomor SC</label>
					<input type="text" id="sc" class="form-control" placeholder="Contoh: SC28819123">
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Nama Pelanggan</label>
					<input type="text" id="nama_pelanggan" class="form-control" placeholder="Contoh: John Gilbert" value="{{$data->nama_pelanggan}}">
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Nomor HP Pelanggan</label>
					<input type="text" id="no_hp" class="form-control" placeholder="Contoh: 082282170800" value="{{$data->no_hp}}">
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Sektor</label>
					<input class="form-control" disabled="" type="text" value="{{$data->sektor}}">
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Status</label>
					<input class="form-control" disabled="" type="text" value="{{$data->status}}">
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Tanggal HS</label>
					<input class="form-control" disabled="" type="text" value="{{$data->tgl_hs}}">
				</div>
				<div class="form-group col-md-12">
					<label for="odp">Kendala</label>
					<textarea class="form-control" disabled="">{{$data->kendala}}</textarea>
				</div>
				<div style="margin: 40px 32%;">
					<a href="{{route('edit_data_no_sc')}}" class="btn btn-danger">Batal</a>
					<button class="btn btn-primary" onclick="dosubmit()">Submit</button>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	function dosubmit() {
		$sc = $("#sc").val();
		$nama_pelanggan = $("#nama_pelanggan").val();
		$no_hp = $("#no_hp").val();
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('ajax_fill_blank_no_sc')}}/{{$data->id}}",
			method: "POST",
			data  : {
				"sc" : $sc,
				"nama_pelanggan" : $nama_pelanggan,
				"no_hp" : $no_hp,
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					window.open("{{Route('edit_data_no_sc')}}","_SELF");
				} else
					$.alert({
					    title: 'Error!',
					    content: res.pesan,
					});
			},
			error : function(res) {
				$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			}
		})
	}
</script>

@endsection
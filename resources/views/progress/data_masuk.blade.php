@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('style')
	<style type="text/css">
		#tabel_ini {
			box-shadow:2px 2px 2px grey;
		}
		table#tabel_ini, #tabel_ini tr, #tabel_ini td {
			border: 2px solid grey;
			border-collapse: collapse !important;
			text-align:left !important;
		}
		#tabel_ini th {
			background: #b6bab2;
		}

		#tabel_ini thead tr th {
			border-bottom: 2px solid grey;
			text-align: center;
		}
		#tabel_ini td.left{
			text-align: left !important
		}
		#tabel_ini tbody tr td{
			text-align: center !important;
		}
		.padding-monitor {
			padding-left: 20px !important
		}
		.red {
			background: red !important;
		}
		#tabel_ini th {
			border:2px solid black !important;
			vertical-align: middle !important;
		}
		#tabel_ini td {
			vertical-align: middle !important;
		}
		#tabel_ini .top {
			vertical-align: top !important;
		}
		#tabel_ini a {
			color: black;
			font-size:13pt
		}
		.warna_total {
			background: #bab8b8 !important;
		}
		.warna_total_exp {
			background: #e25f5f !important;
		}
		th.red {
			color: white !important;
		}
		.red a {
			color: white !important;
		}
		.arsir {
			background-image: url("{{asset('gambar_arsir/arsir.jpeg')}}") !important;
			background-size: 100% 100%;
			/*background: red !important;*/
		}
		.putih a {
			color:red !important;
		}
	</style>
@endsection

@section('content')

<div class="container">
	<div class="col-md-12" style="padding-left: 0">
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="with-border">
							<div style="padding:30px;text-align:right;">
								<a class="btn btn-success" href="{{Route('data_masuk')}}"><i class="fa fa-refresh"></i> Refresh</a>
								@if(Auth::user()->group=="UPLOADER")
								<button class="btn btn-primary" onclick="pilihFile()"><i class="fa fa-upload"></i> Upload .xls</button>
								<form id="formCSV" method="post" style="display:none" action="#">
									<input type="file" name="dataxls" accept=".xls, .xlsx" id="fileCSV" onchange="upload()">
								</form>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="box" style="min-height: 370px">
						<div class="with-border">
							<div style="padding:30px;text-align:right;">
								<table class="table" id="tabel_ini">
									<thead>
										<tr>
											<th rowspan="2">Tanggal</th>
											<th colspan="10">Status</th>
										</tr>
										<tr>
											<th>Survei PT2/PT3</th>
											<th>Request to Complete</th>
											<th>Pending Order</th>
											<th>PT3/PT4</th>
											<th>PT2</th>
											<th>ODP Belum Go Live</th>
											<th>Tanam Tiang</th>
											<th>Unspec/Loss</th>
											<th>Tidak Ada SC</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
										@foreach($data as $index => $val)
										<tr>
											<td>{{$val->uploaded_at}}</td>
											<td>{{$val->survei_pt23}}</td>
											<td>{{$val->request_to_complete}}</td>
											<td>{{$val->pending_order}}</td>
											<td>{{$val->pt34qe}}</td>
											<td>{{$val->pt2}}</td>
											<td>{{$val->odp_belum_go_live}}</td>
											<td>{{$val->tanam_tiang}}</td>
											<td>{{$val->unspec}}</td>
											<td>{{$val->no_sc}}</td>
											<td>{{number_format(
												$val->survei_pt23+
												$val->request_to_complete+
												$val->pending_order+
												$val->pt34qe+
												$val->pt2+
												$val->odp_belum_go_live+
												$val->tanam_tiang+
												$val->unspec+
												$val->no_sc
											)}}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<!-- new -->
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$("#tabel_ini").DataTable({
			"order": [[ 0, 'desc' ]]
		});
	})
</script>
@if(Auth::user()->group=="UPLOADER")
<script type="text/javascript">
	$(document).ready(function(){
		//$("#tabel_target").DataTable({});
	})
	function pilihFile() {
		$("#fileCSV").click();
	}
	function upload() {
		$.LoadingOverlay("show");
		$file_data = $("#formCSV input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('dataxls',$file_data);
        //fd.append("CustomField", "This is some extra data");
        $.ajax({
            url: "{{Route('upload_progress_ajax')}}",
            method: 'POST',
            data: $form_data,
            cache  : false,
			contentType: false,
			processData: false,
            success:function(res){
            	$("#fileCSV").val("");
            	$.LoadingOverlay("hide");
            	if(res.success) {
            		success_upload();
            	} else {
            		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
            	}
            },
            error: function() {
            	$("#fileCSV").val("");
            	$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
            },
            cache: false,
            contentType: false,
            processData: false
        });
	}
	function success_upload() {
		$.alert({
		    title: 'Success!',
		    content: 'File .xls berhasil diupload! silahkan klik \'Refresh\' untuk melihat perubahan!',
		});
	}
</script>
@endif
@endsection
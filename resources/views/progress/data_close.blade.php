@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')

<div class="container">
	<div class="col-md-12" style="padding-left: 0">
			<div class="row">
				<div class="col-md-12">
					<div class="box" style="min-height: 370px">
						<div class="with-border">
							<div style="padding:30px;text-align:right;">
								<table class="table" id="tabel_target" style="width:100%">
									<thead>
										<tr>
											<th>No</th>
											<th>Sektor</th>
											<th>STO</th>
											<th>Kendala</th>
											<th>SC</th>
											<th>Nama Pelanggan</th>
											<th>No HP</th>
											<th>Tgl HS</th>
											<th>Status</th>
											<th>Status Validasi</th>
											<!-- <th>#</th> -->
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<!-- new -->
</section>

<div class="modal large fade text-left" id="modal-informasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel12"
aria-hidden="true">
   <div class="modal-dialog" role="document">
	  <div class="modal-content">
	    <div class="modal-header bg-primary white">
	      <h4 class="modal-title white" id="myModalLabel12"><i class="la la-lightbulb-o"></i> Validasi Data Close</h4>
	    </div>
	    <div class="modal-body">
	    	<p>Silahkan klik tombol <b>Record Valid</b> jika record <b><span id="sc"></span></b> benar-benar valid, dan klik tombol <b>Record tidak Valid</b> jika record tersebut tidak valid.</p>
	    </div>
	    <div class="modal-footer">
	    	<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
	    	<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="record_tidak_valid_check()">Record Tidak Valid</button>
	      <button type="button" class="btn btn-success" data-dismiss="modal" onclick="record_valid_check()">Record Valid</button>
	    </div>
	  </div>
   </div>
</div>

<div class="modal large fade text-left" id="validcheck" tabindex="-1" role="dialog" aria-labelledby="myModalLabel123" aria-hidden="true">
   <div class="modal-dialog" role="document">
	  <div class="modal-content">
	    <div class="modal-header" style="background:green;color:white">
	      <h4 class="modal-title white" id="myModalLabel123"><i class="la la-lightbulb-o"></i> Record Valid</h4>
	    </div>
	    <div class="modal-body">
	    	<p>Apakah anda yakin <b>Record Valid</b> pada <b><span id="sc_target"></span></b> dengan status <b><span id="status"></span></b> 
	    </div>
	    <div class="modal-footer">
	      	<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" onclick="valid_batal()">Batal</button>
	    	<button type="button" class="btn btn-warning" onclick="valid_ya()">Ya</button>
	    </div>
	  </div>
   </div>
</div>

<div class="modal large fade text-left" id="tidakvalidcheck" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1234"
aria-hidden="true">
   <div class="modal-dialog" role="document">
	  <div class="modal-content">
	    <div class="modal-header" style="background:red;color:white">
	      <h4 class="modal-title white" id="myModalLabel1234"><i class="la la-lightbulb-o"></i> Record Tidak Valid</h4>
	    </div>
	    <div class="modal-body">
	    	<div class="form-group col-md-12">
				<label >Nama PIC</label>	
				<input class="form-control" type="text" name="nama_pic" placeholder="ANDI" id="nama_pic">
			</div>
			<div class="form-group col-md-12">
				<label >No HP</label>	
				<input class="form-control" type="number" name="no_hp" placeholder="0822012345678" id="no_hp">
			</div>
			<div class="form-group col-md-12">
				<label >Catatan</label>	
				<textarea placeholder="Karena" id="catatan" class="form-control"></textarea>
			</div>
	    	<p>Apakah anda yakin <b>Record Tidak Valid</b> pada <b><span id="sc_target_tidakvalid"></span></b> dengan status <b><span id="status_tidakvalid"></span></b> 
	    </div>
	    <div class="modal-footer">
	      	<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" onclick="record_tidak_valid_batal()">Batal</button>
	    	<button type="button" class="btn btn-warning" onclick="record_tidak_valid_ya()">Ya</button>
	    </div>
	  </div>
   </div>
</div>

<script type="text/javascript">
	$sc_target = "";
	$status_target = "";
	function validasi($sc, $status) {
		$sc_target = $sc;
		$status_target = $status; 
		$("#sc").html($sc);
		$("#modal-informasi").modal("show");
	}
	
	function record_tidak_valid_check(){
		$("#sc_target_tidakvalid").html($sc_target);
		$("#status_tidakvalid").html($status_target);
		$("#tidakvalidcheck").modal("show");
	}

	function record_tidak_valid_ya() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('validasi_force_close_pending')}}/"+$sc_target+"/dec",
			method : "POST",
			data : {
				"nama_pic" : $("#nama_pic").val(),
				"no_hp" : $("#no_hp").val(),
				"catatan" : $("#catatan").val(),
			},
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: res.pesan,
					});
					$tabel.ajax.reload();
					$("#modal-informasi").modal("hide");
					$("#tidakvalidcheck").modal("hide");
				} else {
					$.alert({
					    title: 'Oops!',
					    content: res.pesan,
					});
				}
			},
			error: function() {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			},
		})
	}
	$(document).ready(function() {
		$role = "{{Auth::user()->group}}";
	    $tabel = $('#tabel_target').DataTable( {
	        "processing": true,
	        "scrollX": true,
	        "serverSide": true,
	        "ajax": "{{Route('get_data_closing')}}",
	        "columns": [
	        {"data": "sc", render: function (data, type, row, meta) {
	        	return meta.row + meta.settings._iDisplayStart + 1;
            }, searchable: false},
            { "data": "sektor" },
            { "data": "sto" },
            { "data": "kendala", searchable: false, orderable: false},
            { "data": "sc" },
            { "data": "nama_pelanggan"},
            { "data": "no_hp"},
            { "data": "tgl_hs"},
            { "data": "status"},
            { "data": "is_validate", render: function (data, type, row, meta) {
            	if(row['status']=="PS" || row['status']=="DOUBLE SC")
            		return "-";
	        	if(data==1 || data==true)
	        		return "Telah divalidasi";
	        	else
	        		return "Belum divalidasi";
            }},
            /*{ "data": "sc", render: function (data, type, row, meta) {
            	if(
            		row['is_validate']==1 ||
            		row['is_validate']==true ||
            		($role!="CS") && $role!="ADMIN" ||
            		row['status']=="PS" || row['status']=="DOUBLE SC"
            	) {
            		return "<button class='btn btn-sm btn-primary' onclick='validasi(\""+data+"\",\""+row['status']+"\")' disabled='disabled'>Validasi</button>";
            	} else {
            		return "<button class='btn btn-sm btn-primary' onclick='validasi(\""+data+"\",\""+row['status']+"\")'>Validasi</button>";
            	}
            }, "searchable": false, "orderable": false},*/
            
        ],
	    });
	});

		

	function record_tidak_valid_batal(){
		$("#sc").html($sc_target);
		$("#modal-informasi").modal("show");
	}

	function record_valid_check(){

		$("#sc_target").html($sc_target);
		$("#status").html($status_target);
		$("#validcheck").modal("show");
	}
	function valid_ya() {
		$.LoadingOverlay("show");
		$.ajax({
			url : "{{Route('validasi_force_close_pending')}}/"+$sc_target+"/acc",
			method : "POST",
			success: function(res) {
				$.LoadingOverlay("hide");
				if(res.success) {
					$.alert({
					    title: 'Success!',
					    content: res.pesan,
					});
					$tabel.ajax.reload();
					$("#modal-informasi").modal("hide");
					$("#validcheck").modal("hide");
				} else {
					$.alert({
					    title: 'Oops!',
					    content: res.pesan,
					});
				}
			},
			error: function() {
				$.LoadingOverlay("hide");
				$.alert({
				    title: 'Success!',
				    content: 'Tidak dapat terhubung ke server!',
				});
			},
		})
	}
	function valid_batal(){
		$("#sc").html($sc_target);
		$("#modal-informasi").modal("show");
	}

</script>

@endsection
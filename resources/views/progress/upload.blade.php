@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('content')

<div class="container">
	<div class="col-md-12" style="padding-left: 0">
			<div class="row">
				<div class="col-md-12">
					<div class="box" style="min-height: 370px">
						<div class="with-border">
							<div style="padding:30px;text-align:right;">
								<button class="btn btn-primary" onclick="pilihFile()"><i class="fa fa-upload"></i> Import .xls</button>
								<form id="formCSV" method="post" style="display:none" action="#">
									<input type="file" name="dataxls" accept=".xls, .xlsx" id="fileCSV" onchange="upload()">
								</form>
								<hr>
								<table class="table" id="tabel_target">
									<thead>
										<tr>
											<th>No</th>
											<th>Kendala</th>
											<th>Status</th>
											<th>SC</th>
											<th>Nama Pelanggan</th>
											<th>No HP</th>
											<th>Tgl HS</th>
										</tr>
									</thead>
									<tbody>
										@foreach($data as $index => $val)
										<tr>
											<td>{{$index+=1}}</td>
											<td>{{$val->kendala}}</td>
											<td>{{$val->status}}</td>
											<td>{{$val->sc}}</td>
											<td>{{$val->nama_pelanggan}}</td>
											<td>{{$val->no_hp}}</td>
											<td>{{$val->tgl_hs}}</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<!-- new -->
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$("#tabel_target").DataTable({});
	})
	function pilihFile() {
		$("#fileCSV").click();
	}
	function upload() {
		$.LoadingOverlay("show");
		$file_data = $("#formCSV input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('dataxls',$file_data);
        //fd.append("CustomField", "This is some extra data");
        $.ajax({
            url: "{{Route('upload_progress_ajax')}}",
            method: 'POST',
            data: $form_data,
            cache  : false,
			contentType: false,
			processData: false,
            success:function(res){
            	$("#fileCSV").val("");
            	$.LoadingOverlay("hide");
            	if(res.success) {
            		success_upload();
            		setTimeout(function(){
            			window.open("{{Route('upload_progress')}}","_SELF");
            		}, 3000);
            	} else {
            		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
            	}
            },
            error: function() {
            	$("#fileCSV").val("");
            	$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
            },
            cache: false,
            contentType: false,
            processData: false
        });
	}
	function success_upload() {
		$.alert({
		    title: 'Success!',
		    content: 'File success uploaded!',
		});
	}
</script>
@endsection
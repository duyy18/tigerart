@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section('style')
	<style type="text/css">
		#tabel_ini {
			box-shadow:2px 2px 2px grey;
		}
		#tabel_ini th, #tabel_ini td {
			 word-wrap: break-word !important;
			 font-size:10px;
			 font-weight:bold;
		}
		table#tabel_ini, #tabel_ini tr, #tabel_ini td {
			border: 2px solid grey;
			text-align:left !important;
		}
		#tabel_ini th {
			background: #b6bab2;
			padding-top:0px;
			padding-bottom:0px;
		}

		#tabel_ini thead tr th {
			border-bottom: 2px solid grey;
			text-align: center;
		}
		#tabel_ini td.left{
			text-align: left !important
		}
		#tabel_ini tbody tr td{
			text-align: center !important;
		}
		.padding-monitor {
			padding-left: 20px !important
		}
		.red {
			background: red !important;
		}
		#tabel_ini th {
			border:2px solid black !important;
			vertical-align: middle !important;
		}
		#tabel_ini td {
			vertical-align: middle !important;
			padding-top:5px;
			padding-bottom:5px;
		}
		#tabel_ini .top {
			vertical-align: top !important;
		}
		#tabel_ini a {
			color: black;
			font-size:9pt
		}
		.warna_total {
			background: #bab8b8 !important;
		}
		.warna_total_exp {
			background: #e25f5f !important;
		}
		th.red {
			color: white !important;
		}
		.red a {
			color: white !important;
		}
		.arsir {
			background-image: url("{{asset('gambar_arsir/arsir.jpeg')}}") !important;
			background-size: 100% 100%;
			/*background: red !important;*/
		}
		.putih a {
			color:red !important;
		}
		table.dataTable,
		table.dataTable th,
		table.dataTable td {
		  -webkit-box-sizing: content-box;
		  -moz-box-sizing: content-box;
		  box-sizing: content-box;
		}
	</style>
@endsection

@section('content')

<div class="container">
	<div class="col-md-12" style="padding-left: 0">
			<div class="row">
				<div class="col-md-12">
					<div class="box" style="min-height: 370px">
						<div class="with-border">
							<div style="padding:30px;">
								<div class="row">
									<div class="col-md-12" style="margin-top:-20px">
										<form action="{{Route('monitoring_progress')}}" method="GET" style="margin-bottom:-30px">
											<label>Tampilkan dari: </label>
											@if(!isset($_GET['tgl_start']))
											<input type="date" name="tgl_start" id="tgl_start" value="{{date('Y-m-d',strtotime('01/01/2019'))}}">
											@else
											<input type="date" name="tgl_start" id="tgl_start" value="{{$_GET['tgl_start']}}">
											@endif
											s.d.
											@if(!isset($_GET['tgl_start']))
											<input type="date" name="tgl_end" id="tgl_end" value="{{date('Y-m-d')}}"> 
											@else
											<input type="date" name="tgl_end" id="tgl_end" value="{{$_GET['tgl_end']}}"> 
											@endif
											<button class="btn btn-sm btn-primary">Tampilkan</button>
										</form>
									</div>
								</div>
								<hr>
								<table class="table" id="tabel_ini" style="width:100%">
									<thead>
										<tr>
											<th colspan="2" rowspan="2"></th>
											<th colspan="11" class="center">Current Order</th>
											<th colspan="11" class="red">Expired Order</th>
										</tr>
										<tr>
											<th>JMB</th>
											<th>KOA-PAP</th>
											<th>JMI</th>
											<th>MND-OUTER</th>
											<th>MAB</th>
											<th title="Inventori">INV</th>
											<th title="Deployer">DEP</th>
											<th>CS</th>
											<th>MTC</th>
											<th>CONS</th>
											<th class="warna_total">Total</th>
											<th class="red">JMB</th>
											<th class="red">KOA-PAP</th>
											<th class="red">JMI</th>
											<th class="red">MND-OUTER</th>
											<th class="red">MAB</th>
											<th title="Inventori" class="red">INV</th>
											<th title="Deployer" class="red">DEP</th>
											<th class="red">CS</th>
											<th class="red">MTC</th>
											<th class="red">CONS</th>
											<th class="red">Total</th>
										</tr>
									</thead>
									<tbody style="text-align: left;" class="tabel_body">
										<tr>
											<td>1</td>
											<td class="left">Survei PT2/PT3</td>
											<td>
												<a href="#" onclick="showInformasi('JMB','now','next','SURVEI-PT2/PT3')">
													@if(count($data['now']['Survei']['JMB'])==0)
													-
													@else
													{{count($data['now']['Survei']['JMB'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('KOA-PAP','now','next','SURVEI-PT2/PT3')">
													@if(count($data['now']['Survei']['KOA-PAP'])==0)
													-
													@else
													{{count($data['now']['Survei']['KOA-PAP'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('JMI','now','next','SURVEI-PT2/PT3')">
													@if(count($data['now']['Survei']['JMI'])==0)
													-
													@else
													{{count($data['now']['Survei']['JMI'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('MND-OUTER','now','next','SURVEI-PT2/PT3')">
													@if(count($data['now']['Survei']['MND-OUTER'])==0)
													-
													@else
													{{count($data['now']['Survei']['MND-OUTER'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('MAB','now','next','SURVEI-PT2/PT3')">
													@if(count($data['now']['Survei']['MAB'])==0)
													-
													@else
													{{count($data['now']['Survei']['MAB'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="warna_total">
												<a>
												{{
													count($data['now']['Survei']['JMB'])+
													count($data['now']['Survei']['KOA-PAP'])+
													count($data['now']['Survei']['JMI'])+
													count($data['now']['Survei']['MND-OUTER'])+
													count($data['now']['Survei']['MAB'])
												}}
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('JMB','exp','next','SURVEI-PT2/PT3')">
													@if(count($data['exp']['Survei']['JMB'])==0)
													-
													@else
													{{count($data['exp']['Survei']['JMB'])}}
													@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('KOA-PAP','exp','next','SURVEI-PT2/PT3')">
													@if(count($data['exp']['Survei']['KOA-PAP'])==0)
													-
													@else
													{{count($data['exp']['Survei']['KOA-PAP'])}}
													@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('JMI','exp','next','SURVEI-PT2/PT3')">
													@if(count($data['exp']['Survei']['JMI'])==0)
													-
													@else
													{{count($data['exp']['Survei']['JMI'])}}
													@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('MND-OUTER','exp','next','SURVEI-PT2/PT3')">
													@if(count($data['exp']['Survei']['MND-OUTER'])==0)
													-
													@else
													{{count($data['exp']['Survei']['MND-OUTER'])}}
													@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('MAB','exp','next','SURVEI-PT2/PT3')">
													@if(count($data['exp']['Survei']['MAB'])==0)
													-
													@else
													{{count($data['exp']['Survei']['MAB'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="red">
												<a>
												{{
													count($data['exp']['Survei']['JMB'])+
													count($data['exp']['Survei']['KOA-PAP'])+
													count($data['exp']['Survei']['JMI'])+
													count($data['exp']['Survei']['MND-OUTER'])+
													count($data['exp']['Survei']['MAB'])
												}}
												</a>
											</td>
										</tr>
										<tr>
											<td>2</td>
											<td class="left">Request to Complete</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td>
												<a href="#" onclick="showInformasi('CS','now','next','REQUEST_TO_COMPLETE')">
													@if(count($data['now']['REQUEST_TO_COMPLETE']['CS'])==0)
													-
													@else
													{{count($data['now']['REQUEST_TO_COMPLETE']['CS'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="warna_total">
												<a>{{
													count($data['now']['REQUEST_TO_COMPLETE']['CS'])
												}}
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="putih">
												<a href="#" onclick="showInformasi('CS','exp','next','REQUEST_TO_COMPLETE')">
													@if(count($data['exp']['REQUEST_TO_COMPLETE']['CS'])==0)
													-
													@else
													{{count($data['exp']['REQUEST_TO_COMPLETE']['CS'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="red">
												<a>{{
													count($data['exp']['REQUEST_TO_COMPLETE']['CS'])
												}}
												</a>
											</td>
										</tr>
										<tr>
											<td>3</td>
											<td class="left">Pending Order</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td>
												<a href="#" onclick="showInformasi('CS','now','next','PENDING_ORDER')">
													@if(count($data['now']['PENDING_ORDER']['CS'])==0)
													-
													@else
													{{count($data['now']['PENDING_ORDER']['CS'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="warna_total">
												<a>{{
													count($data['now']['PENDING_ORDER']['CS'])
												}}
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="putih">
												<a href="#" onclick="showInformasi('CS','exp','next','PENDING_ORDER')">
													@if(count($data['exp']['PENDING_ORDER']['CS'])==0)
													-
													@else
													{{count($data['exp']['PENDING_ORDER']['CS'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="red">
												<a>{{
													count($data['exp']['PENDING_ORDER']['CS'])
												}}
												</a>
											</td>
										</tr>
										<tr>
											<td>4</td>
											<td class="left">PT3/PT4/QE</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td>
												<a href="#" onclick="showInformasi('DEPLOYER','now','next','SURVEI-PT2/PT3')">
													@if(count($data['now']['Survei']['DEPLOYER'])==0)
													-
													@else
													{{count($data['now']['Survei']['DEPLOYER'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="warna_total">
												<a>
													{{count($data['now']['Survei']['DEPLOYER'])}}
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="putih">
												<a href="#" onclick="showInformasi('DEPLOYER','exp','next','SURVEI-PT2/PT3')">
													@if(count($data['exp']['Survei']['DEPLOYER'])==0)
													-
													@else
													{{count($data['exp']['Survei']['DEPLOYER'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="red">
												<a>
													{{count($data['exp']['Survei']['DEPLOYER'])}}
												</a>
											</td>
										</tr>
										<tr>
											<td rowspan="4" class="left top">5</td>
											<td colspan="23" class="left">PT2</td>
											
										</tr>
										<tr>
											<td class="padding-monitor left">5.1 Survei, Konstruksi, Data</td>
											<td>
												<a href="#" onclick="showInformasi('JMB','now','next','PT2-SURVEI')">
													@if(count($data['now']['PT2']['SURVEI']['JMB'])==0)
													-
													@else
													{{count($data['now']['PT2']['SURVEI']['JMB'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('KOA-PAP','now','next','PT2-SURVEI')">
													@if(count($data['now']['PT2']['SURVEI']['KOA-PAP'])==0)
													-
													@else
													{{count($data['now']['PT2']['SURVEI']['KOA-PAP'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('JMI','now','next','PT2-SURVEI')">
													@if(count($data['now']['PT2']['SURVEI']['JMI'])==0)
													-
													@else
													{{count($data['now']['PT2']['SURVEI']['JMI'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('MND-OUTER','now','next','PT2-SURVEI')">
													@if(count($data['now']['PT2']['SURVEI']['MND-OUTER'])==0)
													-
													@else
													{{count($data['now']['PT2']['SURVEI']['MND-OUTER'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('MAB','now','next','PT2-SURVEI')">
													@if(count($data['now']['PT2']['SURVEI']['MAB'])==0)
													-
													@else
													{{count($data['now']['PT2']['SURVEI']['MAB'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="warna_total">
												<a>
												{{
													count($data['now']['PT2']['SURVEI']['JMB'])+
													count($data['now']['PT2']['SURVEI']['KOA-PAP'])+
													count($data['now']['PT2']['SURVEI']['JMI'])+
													count($data['now']['PT2']['SURVEI']['MND-OUTER'])+
													count($data['now']['PT2']['SURVEI']['MAB'])
												}}
												</a>	
											</td>
											<td class='putih'>
												<a href="#" onclick="showInformasi('JMB','exp','next','PT2-SURVEI')">
												@if(count($data['exp']['PT2']['SURVEI']['JMB'])==0)
													-
												@else
													{{count($data['exp']['PT2']['SURVEI']['JMB'])}}
												@endif
												</a>
											</td>
											<td class='putih'>
												<a href="#" onclick="showInformasi('KOA-PAP','exp','next','PT2-SURVEI')">
												@if(count($data['exp']['PT2']['SURVEI']['KOA-PAP'])==0)
													-
												@else
													{{count($data['exp']['PT2']['SURVEI']['KOA-PAP'])}}
												@endif
												</a>
											</td>
											<td class='putih'>
												<a href="#" onclick="showInformasi('JMI','exp','next','PT2-SURVEI')">
												@if(count($data['exp']['PT2']['SURVEI']['JMI'])==0)
													-
												@else
													{{count($data['exp']['PT2']['SURVEI']['JMI'])}}
												@endif
												</a>
											</td>
											<td class='putih'>
												<a href="#" onclick="showInformasi('MND-OUTER','exp','next','PT2-SURVEI')">
												@if(count($data['exp']['PT2']['SURVEI']['MND-OUTER'])==0)
													-
												@else
													{{count($data['exp']['PT2']['SURVEI']['MND-OUTER'])}}
												@endif
												</a>
											</td>
											<td class='putih'>
												<a href="#" onclick="showInformasi('MAB','exp','next','PT2-SURVEI')">
												@if(count($data['exp']['PT2']['SURVEI']['MAB'])==0)
													-
												@else
													{{count($data['exp']['PT2']['SURVEI']['MAB'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="red">
												<a>
												{{
													count($data['exp']['PT2']['SURVEI']['JMB'])+
													count($data['exp']['PT2']['SURVEI']['KOA-PAP'])+
													count($data['exp']['PT2']['SURVEI']['JMI'])+
													count($data['exp']['PT2']['SURVEI']['MND-OUTER'])+
													count($data['exp']['PT2']['SURVEI']['MAB'])
												}}
												</a>	
											</td>
										</tr>
										<tr>
											<td class="padding-monitor left">5.2 Push Tenoss</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td>
												<a href="#" onclick="showInformasi('SDI','now','push tenoss','PT2-PUSH-TENOSS')">
												@if(count($data['now']['PT2']['PUSH-TENOSS']['SDI'])==0)
													-
												@else
													{{count($data['now']['PT2']['PUSH-TENOSS']['SDI'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="warna_total">
												<a>
													{{
														count($data['now']['PT2']['PUSH-TENOSS']['SDI'])
													}}
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="putih">
												<a href="#" onclick="showInformasi('SDI','exp','push tenoss','PT2-PUSH-TENOSS')">
												@if(count($data['exp']['PT2']['PUSH-TENOSS']['SDI'])==0)
													-
												@else
													{{count($data['exp']['PT2']['PUSH-TENOSS']['SDI'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="red">
												<a>
													{{
														count($data['exp']['PT2']['PUSH-TENOSS']['SDI'])
													}}
												</a>
											</td>
										</tr>
										<tr>
											<td class="padding-monitor left">5.3 Proses Go Live</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td>
												<a href="#" onclick="showInformasi('SDI','now','go_live','PT2-GO-LIVE')">
												@if(count($data['now']['PT2']['GO-LIVE']['SDI'])==0)
													-
												@else
													{{count($data['now']['PT2']['GO-LIVE']['SDI'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td>
												<a href="#" onclick="showInformasi('CS','now','go_live','PT2-GO-LIVE')">
												@if(count($data['now']['PT2']['GO-LIVE']['CS'])==0)
													-
												@else
													{{count($data['now']['PT2']['GO-LIVE']['CS'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="warna_total">
												<a>
													{{
														count($data['now']['PT2']['GO-LIVE']['SDI'])+
														count($data['now']['PT2']['GO-LIVE']['CS'])
													}}
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="putih">
												<a href="#" onclick="showInformasi('SDI','exp','go_live','PT2-GO-LIVE')">
												@if(count($data['exp']['PT2']['GO-LIVE']['SDI'])==0)
													-
												@else
													{{count($data['exp']['PT2']['GO-LIVE']['SDI'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="putih">
												<a href="#" onclick="showInformasi('CS','exp','go_live','PT2-GO-LIVE')">
												@if(count($data['exp']['PT2']['GO-LIVE']['CS'])==0)
													-
												@else
													{{count($data['exp']['PT2']['GO-LIVE']['CS'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="red">
												<a>
													{{
														count($data['exp']['PT2']['GO-LIVE']['SDI'])+
														count($data['exp']['PT2']['GO-LIVE']['CS'])
													}}
												</a>
											</td>
										</tr>

										<tr>
											<td class="left">6</td>
											<td class="left">ODP Belum Go Live</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td>
												<a href="#" onclick="showInformasi('SDI','now','next','ODP BELUM GO LIVE')">
												@if(count($data['now']['ODP_BLM_GO_LIVE']['SDI'])==0)
													-
												@else
													{{count($data['now']['ODP_BLM_GO_LIVE']['SDI'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td>
												<a href="#" onclick="showInformasi('CS','now','next','ODP BELUM GO LIVE')">
												@if(count($data['now']['ODP_BLM_GO_LIVE']['CS'])==0)
													-
												@else
													{{count($data['now']['ODP_BLM_GO_LIVE']['CS'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="warna_total">
												<a>
													{{
														count($data['now']['ODP_BLM_GO_LIVE']['JMB'])+
														count($data['now']['ODP_BLM_GO_LIVE']['KOA-PAP'])+
														count($data['now']['ODP_BLM_GO_LIVE']['JMI'])+
														count($data['now']['ODP_BLM_GO_LIVE']['MND-OUTER'])+
														count($data['now']['ODP_BLM_GO_LIVE']['MAB']) +
														count($data['now']['ODP_BLM_GO_LIVE']['SDI']) +
														count($data['now']['ODP_BLM_GO_LIVE']['CONS']) +
														count($data['now']['ODP_BLM_GO_LIVE']['MTC']) +
														count($data['now']['ODP_BLM_GO_LIVE']['CS'])
													}}
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="putih">
												<a href="#" onclick="showInformasi('SDI','exp','next','ODP BELUM GO LIVE')">
												@if(count($data['exp']['ODP_BLM_GO_LIVE']['SDI'])==0)
													-
												@else
													{{count($data['exp']['ODP_BLM_GO_LIVE']['SDI'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="putih">
												<a href="#" onclick="showInformasi('CS','exp','next','ODP BELUM GO LIVE')">
												@if(count($data['exp']['ODP_BLM_GO_LIVE']['CS'])==0)
													-
												@else
													{{count($data['exp']['ODP_BLM_GO_LIVE']['CS'])}}
												@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="red">
												<a>
													{{
														count($data['exp']['ODP_BLM_GO_LIVE']['JMB'])+
														count($data['exp']['ODP_BLM_GO_LIVE']['KOA-PAP'])+
														count($data['exp']['ODP_BLM_GO_LIVE']['JMI'])+
														count($data['exp']['ODP_BLM_GO_LIVE']['MND-OUTER'])+
														count($data['exp']['ODP_BLM_GO_LIVE']['MAB']) +
														count($data['exp']['ODP_BLM_GO_LIVE']['SDI']) +
														count($data['exp']['ODP_BLM_GO_LIVE']['CONS']) +
														count($data['exp']['ODP_BLM_GO_LIVE']['MTC'])+
														count($data['exp']['ODP_BLM_GO_LIVE']['CS'])
													}}
												</a>
											</td>
										</tr>
										
										<tr>
											<td class="left">7</td>
											<td class="left">Tanam Tiang</td>
											<td>
												<a href="#" onclick="showInformasi('JMB','now','next','TANAM TIANG')">
												@if(count($data['now']['Tanam']['JMB'])==0)
													-
												@else
													{{count($data['now']['Tanam']['JMB'])}}
												@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('KOA-PAP','now','next','TANAM TIANG')">
												@if(count($data['now']['Tanam']['KOA-PAP'])==0)
													-
												@else
													{{count($data['now']['Tanam']['KOA-PAP'])}}
												@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('JMI','now','next','TANAM TIANG')">
												@if(count($data['now']['Tanam']['JMI'])==0)
													-
												@else
													{{count($data['now']['Tanam']['JMI'])}}
												@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('MND-OUTER','now','next','TANAM TIANG')">
												@if(count($data['now']['Tanam']['MND-OUTER'])==0)
													-
												@else
													{{count($data['now']['Tanam']['MND-OUTER'])}}
												@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('MAB','now','next','TANAM TIANG')">
													@if(count($data['now']['Tanam']['MAB'])==0)
														-
													@else
														{{count($data['now']['Tanam']['MAB'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="warna_total">
												<a>
													{{
														count($data['now']['Tanam']['JMB'])+
														count($data['now']['Tanam']['KOA-PAP'])+
														count($data['now']['Tanam']['JMI'])+
														count($data['now']['Tanam']['MND-OUTER'])+
														count($data['now']['Tanam']['MAB']) +
														count($data['now']['Tanam']['SDI']) +
														count($data['now']['Tanam']['CONS']) +
														count($data['now']['Tanam']['MTC'])
													}}
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('JMB','exp','next','TANAM TIANG')">
												@if(count($data['exp']['Tanam']['JMB'])==0)
														-
												@else
													{{count($data['exp']['Tanam']['JMB'])}}
												@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('KOA-PAP','exp','next','TANAM TIANG')">
													@if(count($data['exp']['Tanam']['KOA-PAP'])==0)
														-
													@else
														{{count($data['exp']['Tanam']['KOA-PAP'])}}
													@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('JMI','exp','next','TANAM TIANG')">
													@if(count($data['exp']['Tanam']['JMI'])==0)
														-
													@else
														{{count($data['exp']['Tanam']['JMI'])}}
													@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('MND-OUTER','exp','next','TANAM TIANG')">
													@if(count($data['exp']['Tanam']['MND-OUTER'])==0)
														-
													@else
														{{count($data['exp']['Tanam']['MND-OUTER'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('MAB','exp','next','TANAM TIANG')">
													@if(count($data['exp']['Tanam']['MAB'])==0)
														-
													@else
														{{count($data['exp']['Tanam']['MAB'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="red">
												<a>
													{{
														count($data['exp']['Tanam']['JMB'])+
														count($data['exp']['Tanam']['KOA-PAP'])+
														count($data['exp']['Tanam']['JMI'])+
														count($data['exp']['Tanam']['MND-OUTER'])+
														count($data['exp']['Tanam']['MAB']) +
														count($data['exp']['Tanam']['SDI']) +
														count($data['exp']['Tanam']['CONS']) +
														count($data['exp']['Tanam']['MTC'])
													}}
												</a>
											</td>
										</tr>
										<tr>
											<td class="left">8</td>
											<td class="left">Unspec/Loss</td>
											<td>
												<a href="#" onclick="showInformasi('JMB','now','next','UNSPEC')">
													@if(count($data['now']['UNSPEC']['JMB'])==0)
														-
													@else
														{{count($data['now']['UNSPEC']['JMB'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('KOA-PAP','now','next','UNSPEC')">
													@if(count($data['now']['UNSPEC']['KOA-PAP'])==0)
														-
													@else
														{{count($data['now']['UNSPEC']['KOA-PAP'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('JMI','now','next','UNSPEC')">
													@if(count($data['now']['UNSPEC']['JMI'])==0)
														-
													@else
														{{count($data['now']['UNSPEC']['JMI'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('MND-OUTER','now','next','UNSPEC')">
													@if(count($data['now']['UNSPEC']['MND-OUTER'])==0)
														-
													@else
														{{count($data['now']['UNSPEC']['MND-OUTER'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('MAB','now','next','UNSPEC')">
													@if(count($data['now']['UNSPEC']['MAB'])==0)
														-
													@else
														{{count($data['now']['UNSPEC']['MAB'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td>
												<a href="#" onclick="showInformasi('MTC','now','next','UNSPEC')">
													@if(count($data['now']['UNSPEC']['MTC'])==0)
														-
													@else
														{{count($data['now']['UNSPEC']['MTC'])}}
													@endif
												</a>
											</td>
											<td>
												<a href="#" onclick="showInformasi('CONS','now','next','UNSPEC')">
													@if(count($data['now']['UNSPEC']['CONS'])==0)
														-
													@else
														{{count($data['now']['UNSPEC']['CONS'])}}
													@endif
												</a>
											</td>
											<td class="warna_total">
												<a>
													{{
														count($data['now']['UNSPEC']['JMB'])+
														count($data['now']['UNSPEC']['KOA-PAP'])+
														count($data['now']['UNSPEC']['JMI'])+
														count($data['now']['UNSPEC']['MND-OUTER'])+
														count($data['now']['UNSPEC']['MAB']) +
														count($data['now']['UNSPEC']['SDI']) +
														count($data['now']['UNSPEC']['CONS']) +
														count($data['now']['UNSPEC']['MTC'])
													}}
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('JMB','exp','next','UNSPEC')">
													@if(count($data['exp']['UNSPEC']['JMB'])==0)
														-
													@else
														{{count($data['exp']['UNSPEC']['JMB'])}}
													@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('KOA-PAP','exp','next','UNSPEC')">
													@if(count($data['exp']['UNSPEC']['KOA-PAP'])==0)
														-
													@else
														{{count($data['exp']['UNSPEC']['KOA-PAP'])}}
													@endif													
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('JMI','exp','next','UNSPEC')">
													@if(count($data['exp']['UNSPEC']['JMI'])==0)
														-
													@else
														{{count($data['exp']['UNSPEC']['JMI'])}}
													@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('MND-OUTER','exp','next','UNSPEC')">
													@if(count($data['exp']['UNSPEC']['MND-OUTER'])==0)
														-
													@else
														{{count($data['exp']['UNSPEC']['MND-OUTER'])}}
													@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('MAB','exp','next','UNSPEC')">
													@if(count($data['exp']['UNSPEC']['MAB'])==0)
														-
													@else
														{{count($data['exp']['UNSPEC']['MAB'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="putih">
												<a href="#" onclick="showInformasi('MTC','exp','next','UNSPEC')">
													@if(count($data['exp']['UNSPEC']['MTC'])==0)
														-
													@else
														{{count($data['exp']['UNSPEC']['MTC'])}}
													@endif
												</a>
											</td>
											<td class="putih">
												<a href="#" onclick="showInformasi('CONS','exp','next','UNSPEC')">
													@if(count($data['exp']['UNSPEC']['CONS'])==0)
														-
													@else
														{{count($data['exp']['UNSPEC']['CONS'])}}
													@endif
												</a>
											</td>
											<td class="red">
												<a>
													{{
														count($data['exp']['UNSPEC']['JMB'])+
														count($data['exp']['UNSPEC']['KOA-PAP'])+
														count($data['exp']['UNSPEC']['JMI'])+
														count($data['exp']['UNSPEC']['MND-OUTER'])+
														count($data['exp']['UNSPEC']['MAB']) +
														count($data['exp']['UNSPEC']['SDI']) +
														count($data['exp']['UNSPEC']['CONS']) +
														count($data['exp']['UNSPEC']['MTC'])
													}}
												</a>
											</td>
										</tr>
										<tr>
											<td class="left">9</td>
											<td class="left">Tidak Ada SC</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td>
												<a href="#" onclick="showInformasi('CS','now','next','TIDAK ADA SC')">
													@if(count($data['now']['NO_SC']['CS'])==0)
														-
													@else
														{{count($data['now']['NO_SC']['CS'])}}
													@endif
												</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="warna_total">
												<a>{{count($data['now']['NO_SC']['CS'])}}</a>
											</td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="arsir"></td>
											<td class="red"></td>
										</tr>
									</tbody>
								</table>
								<hr>
								<div class="row">
									<div class="col-md-12">
										<label>Keterangan</label>
										<ul>
											<li>INV: Unit Inventori</li>
											<li>DEP: Unit Deployer</li>
											<li>CS: Consumer Service</li>
											<li>MTC: Unit Maintenance</li>
											<li>CONS: Unit Construction</li>
										</ul>
										<div style="clear: both;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<!-- new -->
</section>

<!-- Modal -->
<!-- Modal -->
<div class="modal large fade text-left" id="modal-informasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel12"
aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document" style="width:90%">
	  <div class="modal-content">
	    <div class="modal-header bg-primary white">
	      <h4 class="modal-title white" id="myModalLabel12"><i class="la la-lightbulb-o"></i> Detail Order</h4>
	    </div>
	    <div class="modal-body">
	      <table class="table" id="tabel_target" style="width:100%">
				<thead>
					<tr style="width:100%">
						<th>No</th>
						<th>Sektor</th>
						<th>STO</th>
						<th>Status</th>
						<th>Kendala</th>
						<th>SC</th>
						<th>Nama Pelanggan</th>
						<th>No HP</th>
						<th>Tanggal HS</th>
						<th>Umur</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
	    </div>
	    <div class="modal-footer">
	      <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
	    </div>
	  </div>
   </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		//$("#tabel_ini").DataTable({});
	})
	function showInformasi($sektor,$mode,$state,$kendala) {
		$tgl_start = $("#tgl_start").val();
		$tgl_end   = $("#tgl_end").val();
		$("#tabel_target").DataTable().destroy();
		$("#tabel_target").DataTable({
			"ajax": {
				"url" : "{{Route('get_data_monitoring')}}/"+$sektor+"/"+$mode+"?tgl_start="+$tgl_start+"&tgl_end="+$tgl_end+"&state="+$state+"&kendala="+$kendala,
				"method" : "GET",
			},
			"columns": [
                {"data": "sc", render: function (data, type, row, meta) {
                	return meta.row + meta.settings._iDisplayStart + 1;
                },searchable: false},
                {"data": "sektor"},
                {"data": "sto"},
                {"data": "status"},
                {"data": "kendala"},
                {"data": "sc"},
                {"data": "nama_pelanggan"},
                {"data": "no_hp"},
                {"data": "tgl_hs"},
                {"data": "umur", render: function (data, type, row, meta) {
                	if($mode=="exp")
                		return "<span style='color:red'>"+data+" hari</span>";
                	else
                		return "<span style=''>"+data+" hari</span>";
                }, searchable: false},
                {"data": "received_level_one", render: function (data, type, row, meta) {
                	$foto_push = row['foto_push'];
                	$foto_connectivity = row['foto_connectivity'];
                	$need_validate_cs = row['need_validate_cs'];
                	$status = row['status'];
                	if($need_validate_cs==true || $need_validate_cs==1)
                		return "Menunggu Validasi CS";
                	if(data==1 || data==true)  {
                		if($status=="PT2") {
                			if($foto_push == null) return "Proses Konstruksi";
                			if($foto_push != null) return "Proses Go Live";
                		} else
                			return "In progress";
                	} else
                		return "No progress";
                },searchable: false},
			],
		})
		$("#modal-informasi").modal("show");
	}
</script>
<!-- Modal -->

<script type="text/javascript">
	$(document).ready(function(){
	})
</script>



<!-- Model Pengumuman -->
<div class="modal large fade text-left" id="modal-pengumuman" tabindex="-1" role="dialog" aria-labelledby="myModalLabel12"
aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document" style="width:90%;height:90%;">
	  <div class="modal-content">
	    <div class="modal-header bg-primary white">
	      <h4 class="modal-title white" id="myModalLabel12"><i class="la la-lightbulb-o"></i> Informasi</h4>
	    </div>
	    <div class="modal-body">
	    	<p></p>
	      <pre>
<h2 style="margin:0px;text-align:center;font-size:22pt">Selamat Datang di Aplikasi &nbsp<span style="font-family: 'montez';font-size:35pt;color:red">Solusi Order PSB</span></h2>
<div style="background:red; width:25%;margin-left:37%;padding:0px;color:white">
<h2 style="font-size:15pt;padding:0px;margin:0px;text-align:center;font-weight:bold;">Waktu Solusi Order PSB</h2>
    1. Survei PT2/P3: 1 Hari
    2. ODP Belum Go Live: 1 Hari
    3. PT2: 3 Hari
    4. Tanam Tiang: 3 Hari
    5. Unspec/Loss: 3 Hari

</div>
<center>
Order sampai dengan tanggal 19 Juli 2019, masa Current Order sampai dengan 31 Juli 2019 (setelah itu masuk ke masa Expired Order)
Order mulai tanggal 20 Juli 2019 berlaku mengikuti <span style="color:white;background:red">Waktu Solusi Order PSB</span>
</center>
<h2 style="font-size:12pt;margin-top:0px"><center><b>SELAMAT BEKERJA..!</b></center></h2>
<span><center><b><i>"Berikan solusi terbaik Anda untuk order PSB"</i></b></center></span>
<center><button type="button" class="btn btn-md btn-success" data-dismiss="modal">OK</button></center>
	      </pre>
	    </div>
<!-- 	    <div class="modal-footer" style="text-align:center;">
	      <button type="button" class="btn btn-md btn-success" data-dismiss="modal">OK</button>
	    </div> -->
	  </div>
   </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		//$("#modal-pengumuman").modal({backdrop: 'static',keyboard: false});
		@php
			date_default_timezone_set("Asia/Jakarta");
			session_start();
			if(!isset($_SESSION['sdh_liat_pengumuman'])) {
				$now = strtotime(date("Y-m-d"));
				$exp = strtotime("2019-08-01");
				if($now<=$exp) {
					echo ('$("#modal-pengumuman").modal({backdrop: \'static\',keyboard: false});');
				}
				$_SESSION['sdh_liat_pengumuman'] = true;
			}
		@endphp
	})
</script>
<!-- Model Pengumuman -->
@endsection
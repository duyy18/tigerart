@extends('layouts.progress')

@section('addRoute', route('showNalImport'))

@section('title')
Solusi Order PSB
@endsection

@section("style")
<style type="text/css">
	th {
		text-align:center;
	}
	td {
		text-align:center;
	}
</style>
@endsection

@section('content')

<div class="container">
	<div class="col-md-12" style="padding-left: 0">
			<div class="row">
				<div class="col-md-12">
					<div class="box" style="min-height: 370px">
						<div class="with-border">
							<div style="padding:30px;text-align:left;">
								<p style="font-weight:bold">Histori Closing</p>
								<hr>
								<table class="table" id="tabel_target" style="width:100%">
									<thead>
										<tr>
											<th>No</th>
											<th>Sektor</th>
											<th>STO</th>
											<th>Status</th>
											<th>SC</th>
											<th>Nama Pelanggan</th>
											<th>No HP</th>
											<th>Tgl HS</th>
											<th>Tgl Closed</th>
											<th>Umur</th>
										</tr>
									</thead>
									<tbody>
										@foreach($data as $index => $val)
										<tr>
											<td>{{$index+=1}}</td>
											<td>{{$val->sektor}}</td>
											<td>{{$val->sto}}</td>
											<td>{{$val->status}}</td>
											<td>{{$val->sc}}</td>
											<td style="text-align:left;">{{$val->nama_pelanggan}}</td>
											<td>{{$val->no_hp}}</td>
											<td>{{$val->tgl_hs}}</td>
											<td>
												{{date("Y-m-d",strtotime($val->tgl_closed))}}
											</td>
											@if($val->tgl_go_live!=null)
											<td>
												{{$FungsiGlobal->get_umur($val->tgl_hs,$val->tgl_go_live)}} Hari
											</td>
											@else
											<td>
												{{$FungsiGlobal->get_umur($val->tgl_hs,$val->tgl_closed)}} Hari
											</td>
											@endif
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

	<!-- new -->
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$("#tabel_target").DataTable({"scrollX": true});
	})
	function pilihFile() {
		$("#fileCSV").click();
	}
	function upload() {
		$.LoadingOverlay("show");
		$file_data = $("#formCSV input").prop('files')[0];
		$form_data = new FormData();
		$form_data.append('dataxls',$file_data);
        //fd.append("CustomField", "This is some extra data");
        $.ajax({
            url: "{{Route('upload_progress_ajax')}}",
            method: 'POST',
            data: $form_data,
            cache  : false,
			contentType: false,
			processData: false,
            success:function(res){
            	$("#fileCSV").val("");
            	$.LoadingOverlay("hide");
            	if(res.success) {
            		success_upload();
            		setTimeout(function(){
            			window.open("{{Route('upload_progress')}}","_SELF");
            		}, 3000);
            	} else {
            		$.alert({
				    	title: 'Error!',
				    	content: res.pesan,
					});
            	}
            },
            error: function() {
            	$("#fileCSV").val("");
            	$.LoadingOverlay("hide");
            	$.alert({
				    title: 'Error!',
				    content: 'Tidak dapat terhubung ke server!',
				});
            },
            cache: false,
            contentType: false,
            processData: false
        });
	}
	function success_upload() {
		$.alert({
		    title: 'Success!',
		    content: 'File success uploaded!',
		});
	}
</script>
@endsection
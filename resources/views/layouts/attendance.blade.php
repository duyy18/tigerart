<!DOCTYPE html>
<html>
<head>
	<title>Telkom Attendance</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">

	<style type="text/css">
	.att-header {
		height: 60px;
		background: red;
	}

	.att-header-p {
		color: white;
		font-size: 1.5em;
		padding-top: 13px;
	}
	.att-submit-btn {
		width: 100%;
		margin-top: 15px;
	}

	.att-content {
		margin-top: 40px;
		min-height: 600px;
	}

	.att-footer {
		height: 40px;
		background: red;
		text-align: center;
		position: absolute;
		padding-bottom: 0;
	}

	.att-footer-p {
		padding: 0;
		position: relative;
		top: 20px;
		color: white;
		line-height: 0;
	}

	.att-success {
		text-align: center;
	}

	.att-ty {
		font-size: 3em;
		font-weight: bold;
	}
</style>
</head>
<body>

	<div class="att-header col-md-12 col-sm-12 col-xs-12">
		<p class="att-header-p">Attendance</p>
	</div>

	<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 att-content">
		@yield('content')
	</div>

	<div class="clear"></div>
	
	<div class="att-footer col-md-12 col-sm-12 col-xs-12">
		<p class="att-footer-p">Siginjai ART &copy; 2018</p>
	</div>

</body>
</html>
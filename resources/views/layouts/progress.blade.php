<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Solusi Order PSB</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/_all-skins.min.css') }}">
   <link rel="stylesheet" href="{{ asset('css/main.css') }}">

   <link rel="stylesheet" href="{{ asset('adminlte/dist/css/style.css') }}">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/image_zoom/jquery.magnify.css')}}">
  <script src="{{ asset('plugins/image_zoom/jquery.magnify.js') }}"></script>

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style type="text/css">
  .swal2-title{
    font-size: 100px !important;
  }
  .center {text-align: center;}
  .padding-monitor {padding-left:10px}
</style>
@yield("style")
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">

    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">
            <img src="{{ asset('images/logo.png') }}">
          </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="header-title" style="font-family: 'montez'; font-size: 2.5em">
              <a href="#">@yield('title')</a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#" style="color:#dddddd" disabled>Tiger Art</a></li>
            <li><a href="{{Route('monitoring_progress')}}">Home</a></li>
            @if(Auth::user()->group == "UPLOADER")
            <li><a href="{{Route('data_masuk')}}">Upload Data</a></li>
            @else
            <li><a href="{{Route('data_masuk')}}">Histori Upload</a></li>
            @endif
            @if(Auth::user()->group != null && Auth::user()->group!="CS" && Auth::user()->group != "DEPLOYER")
            <li><a href="{{Route('edit_progress')}}/{{Auth::user()->group}}">Edit Data</a></li>
            @endif
            @if(Auth::user()->group == "CS" || Auth::user()->group == "DEPLOYER")
            <li><a href="{{Route('edit_progress_closing')}}/{{Auth::user()->group}}">Edit Data</a></li>
            @endif
            @if(Auth::user()->group=="CS")
            <li><a href="{{Route('edit_progress')}}/{{Auth::user()->group}}">Validasi Data</a></li>
            @endif
            <li><a href="{{Route('histori_progress')}}">Histori Closing</a></li>
            <!-- <li><a href="{{Route('data_close_progress')}}">Data Close</a></li> -->
            <!-- <li><a href="{{Route('apresiasi_progress')}}">Apresiasi dan Denda</a></li> -->
            <li><a href="#" style="color:#dddddd" disabled>Apresiasi dan Denda</a></li>
            <li><a href="{{Route('ganti_password')}}">Ganti Password</a></li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Logout</a></li>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>


    <div class="content-wrapper">
      <div class="container">
        <div class="col-md-12" style="margin:-10px">
            <div class="row">
              <div class="col-md-12" style="padding:-10px">
                <p style="font-family: 'montez';font-size:30px;color: red;font-weight: bold;">
                  ~ {{Auth::user()->call_as}} ~
                </p>
              </div>
            </div>
          </div>
        </div>

      @yield('content')

    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="container">
        <div class="pull-right hidden-xs">

        </div>
        <strong>Copyright &copy; 2018 <a href="https://telkom.co.id">Telkom Indonesia Witel Jambi</a>.</strong> All rights
        reserved.
      </div>
      <!-- /.container -->
    </footer>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <!-- SlimScroll -->
  <script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>

  <!-- ChartJS -->
  <script src="{{ asset('adminlte/dist/js/chart.js/Chart.js') }}"></script>

  <!-- FLOT CHARTS -->
  <script src="{{ asset('adminlte/bower_components/Flot/jquery.flot.js') }}"></script>
  <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
  <script src="{{ asset('adminlte/bower_components/Flot/jquery.flot.resize.js') }}"></script>
  <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
  <script src="{{ asset('adminlte/bower_components/Flot/jquery.flot.pie.js') }}"></script>
  <!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
  <script src="{{ asset('adminlte/bower_components/Flot/jquery.flot.categories.js') }}"></script>

  <!-- AdminLTE for demo purposes -->
  <!-- Ahmad -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $.LoadingOverlaySetup({
        size: 10
      });
    })
  </script>
  @yield('script')
</body>
</html>

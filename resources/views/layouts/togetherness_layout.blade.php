<!DOCTYPE html>
<html>
<head>
	<title>Telkom Togetherness</title>
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.gallery.css') }}">

	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
</head>
<body>

	<div class="t-main-content">
		<nav class="navbar navbar-default t-remove-mb">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">
						<img src="{{ asset('images/logo.png') }}">
					</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="header-title" style="font-family: 'montez'; font-size: 2.5em"><a href="#">Togetherness</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="{{ url('/') }}">Home</a></li>
						<li><a id="search-btn" href="javascript:void(0)">Search</a></li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">Unit 
								<span style="font-size: .7em" class="glyphicon glyphicon-triangle-bottom"></span></a>
								<ul class="dropdown-menu">
									@yield('unit-menu')
								</ul>
							</li>
							
							@if(Auth::user()->role == 1)
							<li><a href="@yield('add-route')">Add New</a></li>
							@endif

							<li><a href="{{ route('logout') }}" onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">Logout</a></li>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>

			<div class="clear"></div>

			<div class="t-search">
				<form action="{{ $search_route }}" method="get" class="t-form" role="search">
					<div class="t-div" style="width: 46%">
						<p>Activity</p>
						<input name="q" type="text" class="form-control" placeholder="Search here..." required>
					</div>
					<div class="t-div" style="width: 24%">
						<p>From</p>
						<input name="from" type="date" class="form-control">
					</div>
					<div class="t-div" style="width: 24%">
						<p>To</p>
						<input name="to" type="date" class="form-control">
					</div>
					<div class="t-div" style="width: 6%;">
						<p>&nbsp;</p>
						<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span></button>
					</div>
					<div class="clear"></div>
				</form>
			</div>



			<div class="clear"></div>

			<div class="container-fluid main-content-container">

				<div class="col-md-10 col-sm-10 col-xs-12 col-sm-offset-1 col-md-offset-1 main-content-grid">

					<div class="card main-content">
						<div class="card-title main-content-title">
							<strong>@yield('section-title')</strong>
						</div>

						@yield('main-content')

					</div>

				</div>

			</div>

			<div class="clear"></div>

			<div class="footer">
				Copyright &copy; <strong>Telkom Indonesia Witel Jambi</strong> - 2018
			</div>
		</div>

		{{-- Overlay --}}
		<div class="overlay">
			<div class="pop-up">
				<div class="pu-close-btn">X</div>
				<div class="pu-content"></div>
			</div>
		</div>


		<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

		<script type="text/javascript"  src="{{ asset('js/jquery.gallery.js') }}"></script>

		<script>

			$("#search-btn").click(function() {
				$(".t-search").toggleClass('show');
			})

		</script>

		@yield('script')

	</body>
	</html>
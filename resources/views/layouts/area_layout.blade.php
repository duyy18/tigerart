<!DOCTYPE html>
<html>
<head>
	<title>Telkom Area</title>
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('css/jquery.gallery.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
</head>
<body>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">
					<img src="{{ asset('images/logo.png') }}">
				</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="header-title" style="font-family: 'montez'; font-size: 2.5em"><a href="{{ url('/area') }}">Area Profiling</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ url('/') }}">Home</a></li>
					{{-- <li><a href="{{ route('addSTO') }}">Add STO</a></li> --}}
					<li><a href="{{ route('logout') }}" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">Logout</a></li>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>

	<div class="clear"></div>

	<div class="container-fluid main-content-container">
		<div class="col-md-3 sto-grid">
			<div class="card sto">
				<div class="card-title sto-title">Telkom Witel Jambi</div>
				<div class="sto-list">
					<a href="{{ route('showWitel') }}">
						<div class="sto-row">
							Kantor Witel Jambi
						</div>
					</a>
					<a href="{{ route('showSTO', 'JMB') }}">
						<div class="sto-row">
							STO Jambi Centrum (JMB)
						</div>
					</a>
					<a href="{{ route('showSTO', 'PAP') }}">
						<div class="sto-row">
							STO Pasir Putih (PAP)
						</div>
					</a>
					<a href="{{ route('showSTO', 'KOA') }}">
						<div class="sto-row">
							STO Kota Baru (KOA)
						</div>
					</a>
					<a href="{{ route('showSTO', 'MND') }}">
						<div class="sto-row">
							STO Mendalo (MND)
						</div>
					</a>
					<a href="{{ route('showSTO', 'KTL') }}">
						<div class="sto-row">
							STO Kuala Tungkal (KTL)
						</div>
					</a>
					<a href="{{ route('underConstruction') }}">
						<div class="sto-row">
							STO Pangkalan Bulian (PKB)
						</div>
					</a>
					<a href="{{ route('showSTO', 'MBN') }}">
						<div class="sto-row">
							STO Muara Bulian (MBN)
						</div>
					</a>
					<a href="{{ route('showSTO', 'MAB') }}">
						<div class="sto-row">
							STO Muara Bungo (MAB)
						</div>
					</a>
					<a href="{{ route('showSTO', 'SRJ') }}">
						<div class="sto-row">
							STO Sarolangun (SRJ)
						</div>
					</a>
					<a href="{{ route('showSTO', 'BKO') }}">
						<div class="sto-row">
							STO Bangko (BKO)
						</div>
					</a>
					<a href="{{ route('showSTO', 'SPN') }}">
						<div class="sto-row">
							STO Sungai Penuh (SPN)
						</div>
					</a>
					<a href="{{ route('showSTO', 'RBB') }}">
						<div class="sto-row">
							STO Rimbo Bujang (RBB)
						</div>
					</a>
					<a href="{{ route('showSTO', 'MRO') }}">
						<div class="sto-row">
							STO Muara Tebo (MRO)
						</div>
					</a>
					<a href="{{ route('showSTO', 'JMI') }}">
						<div class="sto-row">
							STO Telanaippura (JMI)
						</div>
					</a>
					<a href="{{ route('showSTO', 'TTJ') }}">
						<div class="sto-row">
							STO Tebing Tinggi (TTJ)
						</div>
					</a>
					<a href="{{ route('showSTO', 'SKT') }}">
						<div class="sto-row">
							STO Singkut (SKT)
						</div>
					</a>
				</div>
			</div>
		</div>

		@yield('main-content')

	</div>

	<div class="clear"></div>

	<div class="footer">
		Copyright &copy; <strong>Telkom Indonesia Witel Jambi</strong> - 2018
	</div>

	<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

	@yield('script')

</body>
</html>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Siginjai ART | Login</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
    body {
        background-image:    url({{ url('images/login-background.jpg') }});
        background-size:     100% auto;
        background-repeat:   no-repeat;
    }
    .logo-telkom {
        padding: 10px 20px;
        width: 200px;
        position: absolute;
        right: 0;
        top: 0;
    }

    .logo-telkom img {
        width: 100%;
    }

    .card {
        width: 30%;
        height: 67vh;
        background: white;
        margin: 0 auto;
        margin-top: 70px;
        box-shadow: 1px 1px 3px 1px rgba(0, 0, 0, 0.2);
        border-radius: 10px;
        padding: 30px;
    }

    .card img {
        width: 75%;
        display: block;
        margin: 0 auto;
        margin-bottom: 30px;
    }

    .card input {
        margin-bottom: 15px;
    }

    .login-btn {
        width: 100%;
        background: red;
        border: red;
    }

    .login-btn:hover {
        background: #b70b0b;
    }

    .clear {
        clear: both;
    }
</style>
</head>
<body>

    <div class="logo-telkom">
        <img src="{{ url('images/logo.png') }}">
    </div>

    <div class="clear"></div>

    <div class="card">
        <img src="{{ url('images/home/siginjai-readme.png') }}">
        <form action="{{ route('login') }}" method="POST">
            {{ csrf_field() }}

            @if ($errors->has('username'))
            <span class="help-block">
                <strong style="color: red">{{ $errors->first('username') }}</strong>
            </span>
            @endif
            <input class="form-control" type="text" name="username" required autofocus placeholder="Username">

            <input class="form-control" type="password" name="password" required placeholder="Password">
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif

            <button type="submit" class="btn btn-primary login-btn">Login</button>
        </form>
    </div>


</body>
</html>

@php
    session_start();
    session_destroy();
@endphp